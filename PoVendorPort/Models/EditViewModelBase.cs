﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.Models
{
    public abstract class EditViewModelBase
    {
        public bool IsAdding { get; set; }
    }
}