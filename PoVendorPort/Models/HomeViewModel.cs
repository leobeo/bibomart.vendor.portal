﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.Models
{
    using PoVendorPortCommon.Models;
    using System.Threading.Tasks;
    using PoVendorPortCommon;
    using Microsoft.AspNet.Identity;
    public class HomeViewModel
    {
        public List<PurchaseOrder> POs { get; set; } = new List<PurchaseOrder>();
        public async Task Load(IDataAccess da)
        {
            if (HttpContext.Current == null) return;
            var uid = HttpContext.Current.User.Identity.GetUserId();
            LoggedInVendor = await da.GetVendorInfoAsync(uid);
            if (LoggedInVendor.No == null)
            {
                LoggedInVendor.PurchaseOrders = new List<PurchaseOrder>();
            }
            else
            {
                var pos = await da.LoadPurchaseOrdersByVendorId(LoggedInVendor.No);
                LoggedInVendor.PurchaseOrders = pos.ToList();
            }
        }        
        public Vendor LoggedInVendor { get; private set; }     
           
    }
}