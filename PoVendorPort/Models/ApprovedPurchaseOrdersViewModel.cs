﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.Models
{
    using PoVendorPortCommon.Models;
    using System.ComponentModel.DataAnnotations;

    public class ApprovedPurchaseOrdersViewModel
    {
        [Display(Name = "Từ ngày")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime FromDate { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Đến ngày")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ToDate { get; set; }
        //public int ApprovedCount { get; set; }
        public List<PurchaseOrder> ApprovedPurchaseOrders { get; set; }
    }
}