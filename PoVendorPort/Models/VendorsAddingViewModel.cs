﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PoVendorPort.Models
{
    using PoVendorPortCommon.Validation;
    public class VendorsAddingViewModel
    {
        /// <summary>
        /// Gets or sets a comma-separated list of vendors' NO
        /// </summary>
        [Required(ErrorMessage = "Bạn cần nhập mã nhà cung cấp")]
        [Display(Name = "Mã nhà cung cấp: ")]
        public string Vendors { get; set; }
        //This is required only when UsePasswordWithDefaultPattern is false
        [DynamicRequired(nameof(UsePasswordWithDefaultPattern), IsInverseEnabling = true, ErrorMessage = "Bạn cần nhập mật khẩu")]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }
        [Compare(nameof(Password), ErrorMessage = "Xác nhận mật khẩu không khớp")]
        [Display(Name = "Xác nhận mật khẩu")]
        public string ConfirmPassword { get; set; }
        [Display(Name = "Sử dụng mật khẩu mặc định")]
        public bool UsePasswordWithDefaultPattern { get; set; } = true; //VendorNo@123456
        [Display(Name = "Reset mật khẩu cho những mã NCC đã có tài khoản")]
        public bool ResetPasswordForExistingVendors { get; set; }
        public List<string> ExistingVendorCodes { get; set; }
        public string ExistingVendorCodesDisplay
        {
            get
            {
                return ExistingVendorCodes == null ? "" : string.Join(", ", ExistingVendorCodes);
            }
        }
        public bool HasExistingVendorCodes
        {
            get
            {
                return ExistingVendorCodes != null && ExistingVendorCodes.Any();
            }
        }
        public bool ParseNumbersHavingAtLeast5DigitsAsVendorNo { get; set; }
    }
}