﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.Models
{
    using PoVendorPortCommon.Models;
    public class ControlPanelViewModel
    {
        public UserInfo LoggedInUser { get; set; }
        public List<UserInfo> ManagedUsers { get; set; }
        public List<Vendor> ManagedVendors { get; set; }
    }
}