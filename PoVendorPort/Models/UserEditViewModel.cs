﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace PoVendorPort.Models
{
    using PoVendorPortCommon.Models;
    using PoVendorPortCommon.Validation;
    public class UserEditViewModel : EditViewModelBase
    {
        public UserInfo UserInfo { get; set; }
        [DynamicRequired(nameof(IsAdding), ErrorMessage = "Bạn cần nhập mật khẩu")]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }
        [Compare(nameof(Password), ErrorMessage = "Xác nhận mật khẩu không khớp")]
        [Display(Name = "Xác nhận mật khẩu")]
        public string ConfirmPassword { get; set; }
        public List<ManageGroup> ManageGroups { get; set; }
    }
}