﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.Models
{
    using PoVendorPortCommon.Models;
    public class VendorEditViewModel
    {
        public Vendor Vendor { get; set; }
    }
}