﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.SignalR
{
    using Microsoft.AspNet.SignalR;
    public class MainHubContext
    {
        public MainHubContext()
        {
            _hubContext = GlobalHost.ConnectionManager.GetHubContext<MainHub>();
        }
        IHubContext _hubContext;
        public void RefreshClient(string username)
        {
            _hubContext.Clients.User(username).refresh();
        }
    }
}