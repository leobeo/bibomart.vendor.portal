﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace PoVendorPort.SignalR
{
    public class MainHub : Hub
    {
        public void NotifyDataChanged()
        {
            Clients.User(Context.User.Identity.Name).refresh();
        }
    }
}