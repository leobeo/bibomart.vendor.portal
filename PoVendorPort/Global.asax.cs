﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.SqlClient;
using PoVendorPortCommon;
using Microsoft.Practices.Unity;
using System.Threading.Tasks;

namespace PoVendorPort
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {            
            UnityConfig.RegisterComponents();
            ModelBinderProviders.BinderProviders.Insert(0, new PoVendorPortCommon.ModelBinders.VendorsListModelBinderProvider());
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);            
            System.Web.Helpers.AntiForgeryConfig.UniqueClaimTypeIdentifier = System.Security.Claims.ClaimTypes.NameIdentifier;
            //StartListeningForNewVendors();               
        }
        public override void Dispose()
        {
            base.Dispose();            
        }
        public static void StartListeningForNewVendors()
        {
            //var p = PoDependency;
        }
        static SqlDependency _poDependency;
        static bool _autoCreatingUsers;
        public static SqlDependency PoDependency
        {
            get
            {
                if (_poDependency == null)
                {
                    try
                    {
                        var logger = DependencyResolver.Current.GetService<ILogger>();
                        _poDependency = PoVendorPortCommon.SqlCacheDependencyConfig.CreatePoDependency(PoVendorPortCommon.GlobalAppSettings.MainConnectionString, logger: logger);
                        if (_poDependency == null) return null;
                        _poDependency.OnChange += async (s, e) => {
                            if(e.Source == SqlNotificationSource.Data && e.Info == SqlNotificationInfo.Insert)
                            {
                                if (_autoCreatingUsers) return;
                                _autoCreatingUsers = true;
                                try
                                {
                                    await Task.Delay(TimeSpan.FromSeconds(60));
                                    await IoC.UnityContainerConfig.Uc.Resolve<IDataAccess>("dataaccess").CheckAndAutoCreateDefaultUsers();                                    
                                }
                                catch(Exception ex)
                                {
                                    logger.Write($"{DateTime.Now} - Could not create default users for new vendors, Exception: {ex}");
                                }
                                finally {
                                    _autoCreatingUsers = false;
                                }
                            }
                        };
                    }
                    catch(Exception ex)
                    {
                        DependencyResolver.Current.GetService<ILogger>().Write($"{DateTime.Now} - Could not create PO dependency, Exception: {ex}");
                    }
                }
                return _poDependency;
            }
        }
    }
}
