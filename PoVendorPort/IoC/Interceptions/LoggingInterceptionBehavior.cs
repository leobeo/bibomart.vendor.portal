﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.IoC.Interceptions
{
    using Microsoft.Practices.Unity.InterceptionExtension;
    using PoVendorPortCommon;
    using System.Threading.Tasks;
    using System.Reflection;
    using System.Diagnostics;
    public class LoggingInterceptionBehavior : IInterceptionBehavior
    {
        public LoggingInterceptionBehavior(ILogger logger)
        {
            if (logger == null) throw new ArgumentNullException("logger");
            _logger = logger;
        }
        ILogger _logger;
        public bool WillExecute
        {
            get
            {
                return true;
            }
        }

        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return Type.EmptyTypes;
        }

        public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {            
            var result = getNext()(input, getNext);
            var mi = input.MethodBase as MethodInfo;
            Stopwatch sw = null;
            if (GlobalAppSettings.EnabledExecutionTimeLogging)
            {
                sw = Stopwatch.StartNew();
            }
            //NOTE: This does not take care of method returning void (but still awaitable with async)
            if(mi != null && typeof(Task).IsAssignableFrom(mi.ReturnType))
            {
                _handleAsync(result.ReturnValue as Task, mi.Name, sw);
            }
            else 
            {                
                if (result.Exception != null)
                {
                    _logger.Write(string.Format("{0} - MethodName: {1} - Exception: {2}", DateTime.Now, input.MethodBase.Name, result.Exception));
                }
                if (GlobalAppSettings.EnabledExecutionTimeLogging)
                {
                    sw.Stop();
                    _logger.Write(string.Format("{0} - Executing method {1} has completed in {2} ms", DateTime.Now, input.MethodBase.Name, sw.ElapsedMilliseconds));
                }
            }
            return result;
        }
        async Task _handleAsync(Task task, string methodName, Stopwatch sw = null)
        {
            try
            {
                await task;
                if (task.IsFaulted)
                {
                    _logger.Write(string.Format("{0} - MethodName: {1} - Exception: {2}", DateTime.Now, methodName, task.Exception));
                }
                else
                {                    
                    if (sw != null)
                    {
                        sw.Stop();
                        _logger.Write(string.Format("{0} - Executing method {1} has completed in {2} ms", DateTime.Now, methodName, sw.ElapsedMilliseconds));
                    }
                }                
            } catch (Exception ex)
            {
                _logger.Write(string.Format("{0} - MethodName: {1} - Exception: {2}", DateTime.Now, methodName, ex));
                throw;
            }
        }
    }
}