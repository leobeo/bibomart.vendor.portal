﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.IoC
{
    using Microsoft.Practices.Unity;
    using PoVendorPortDAL;
    using Controllers;
    using System.Web.Configuration;
    //using PoVendorPortCommon.Models;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Microsoft.Practices.Unity.InterceptionExtension;
    using PoVendorPort.IoC.Interceptions;

    using System.IO;
    using PoVendorPortCommon;
    using PoVendorPortCommon.Models;
    using SignalR;
    using PoVendorPortCommon.Models.Picking;
    using PoVendorPortCommon.Models.Picking.Invoice;
    using PoVendorPortCommon.Models.Picking.CheckList;
    using PoVendorPortCommon.Models.Picking.Packing;
    using PoVendorPortCommon.Models.Picking.Sales_v2;
    using PoVendorPortCommon.Models.Picking.SalesOut_v2;
    using PoVendorPortCommon.Models.Picking.ReceiveAccept;

    public class UnityContainerConfig
    {
        public static IUnityContainer Uc { get; private set; }
        public static void LoadDependencies(IUnityContainer uc = null)
        {            
            //EnabledExecutionTimeLogging
            GlobalAppSettings.EnabledExecutionTimeLogging = (WebConfigurationManager.AppSettings["LoggingExecutionTime"] ?? "").ToLower() == "true";
            //ClientIPResolvingMethod
            var clientIpResolvingMethod = WebConfigurationManager.AppSettings["ClientIPResolvingMethod"];
            ClientIPResolvingMethod ipResolvingMethod = ClientIPResolvingMethod.WithoutJS;
            Enum.TryParse<ClientIPResolvingMethod>(clientIpResolvingMethod, true, out ipResolvingMethod);
            GlobalAppSettings.ClientIPResolvingMethod = ipResolvingMethod;
            //ClientIPFilterEnabled
            GlobalAppSettings.ClientIPFilterEnabled = (WebConfigurationManager.AppSettings["ClientIPFilterEnabled"] ?? "true").ToLower() == "true";
            //ShowClientSubnet
            GlobalAppSettings.ShowClientSubnet = (WebConfigurationManager.AppSettings["ShowClientSubnet"] ?? "").ToLower() == "true";
            //CacheSlidingDuration
            double sm;
            if(!double.TryParse(WebConfigurationManager.AppSettings["CacheSlidingMinutes"] ?? "", out sm))
            {
                sm = 60;
            }
            GlobalAppSettings.CacheSlidingMinutes = sm;

            GlobalAppSettings.SqlDependencyTables = (WebConfigurationManager.AppSettings["SqlDependencyTables"] ?? "TBL_Purchase_Order").Split(',').Select(e => e.Trim()).ToArray();

            GlobalAppSettings.MainConnectionString = WebConfigurationManager.ConnectionStrings["mainConnection"].ConnectionString;

            GlobalAppSettings.AdminCode = WebConfigurationManager.AppSettings["AdminCode"] ?? "admin";

            GlobalAppSettings.ManagerCodePattern = WebConfigurationManager.AppSettings["ManagerCodePattern"] ?? "GRP.*";

            var logpath = Path.Combine(Directory.GetParent(Path.GetDirectoryName(new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath)).FullName, "Logs");
            if (!Directory.Exists(logpath)) Directory.CreateDirectory(logpath);
            

            var logpath1 = Path.Combine(logpath, @"log.txt");
            GlobalAppSettings.LogPath = logpath1;
            var logpath2 = Path.Combine(logpath, @"UnresolvableClientIPs.txt");
            Uc = uc ?? new UnityContainer();
            Uc.AddNewExtension<Interception>();
            //Uc.RegisterType<IUserInfoCache, UserInfoCache>(new ContainerControlledLifetimeManager());
            Uc.RegisterInstance<ILogger>(new FileLogger(logpath1), new ContainerControlledLifetimeManager());
            Uc.RegisterType<IDataAccess, DataAccess>("dataaccess", new ContainerControlledLifetimeManager(), new Interceptor<InterfaceInterceptor>(), 
                new InjectionConstructor(GlobalAppSettings.MainConnectionString) 
                    , new InterceptionBehavior<LoggingInterceptionBehavior>()
                );
            Uc.RegisterType<MainHubContext>(new ContainerControlledLifetimeManager());
            Uc.RegisterType<IDataAccess, PoVendorPort.DataAccess.DataCache>("datacache", new ContainerControlledLifetimeManager(), 
                                                                                         new InjectionConstructor(new ResolvedParameter<IDataAccess>("dataaccess"), GlobalAppSettings.MainConnectionString, typeof(MainHubContext), typeof(ILogger)), 
                                                                                         new InjectionProperty("CacheSlidingDuration", TimeSpan.FromMinutes(GlobalAppSettings.CacheSlidingMinutes)
                                                                                         ));
            Uc.RegisterType<AccountController>(new InjectionConstructor(new ResolvedParameter<IDataAccess>("datacache")));
            Uc.RegisterType<ManageController>(new InjectionConstructor(new ResolvedParameter<IDataAccess>("datacache")));
            Uc.RegisterType<HomeController>(new InjectionConstructor(new ResolvedParameter<IDataAccess>("datacache"), typeof(ILogger)));
            Uc.RegisterType<Filters.CustomHandleErrorAttribute>(new ContainerControlledLifetimeManager());
            //Uc.RegisterType<HomeController>(new InjectionProperty("Logger", new FileLogger(logpath2)));      

            Uc.RegisterType<SqlCacheDependencyConfig>(new InjectionConstructor(GlobalAppSettings.MainConnectionString, typeof(ILogger)));
            Uc.RegisterType<PoVendorPortCommon.ModelBinders.VendorsListModelBinder>(new InjectionConstructor(new ResolvedParameter<IDataAccess>("datacache")));

            //Picking

            Uc.RegisterType<SalesInController>(new InjectionConstructor(new ResolvedParameter<IDataAccess>("datacache"), typeof(ILogger)));
            Uc.RegisterType<SalesOutController>(new InjectionConstructor(new ResolvedParameter<IDataAccess>("datacache"), typeof(ILogger)));
            Uc.RegisterType<ComperateReportController>(new InjectionConstructor(new ResolvedParameter<IDataAccess>("datacache"), typeof(ILogger)));
            Uc.RegisterType<InvoiceController>(new InjectionConstructor(new ResolvedParameter<IDataAccess>("datacache"), typeof(ILogger)));
        }
    }
    public class TestDataAccess : IDataAccess
    {
        public Task<IEnumerable<string>> AddDefaultVendors(List<string> vendorCodes, bool updatePassForExistingUsers = false, string passwordPattern = "{0}@123456")
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<string>> AddVendors(List<Tuple<string, string>> vendorCredentials, bool updatePassForExistingUsers = false)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ApprovePo(string pono)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ChangePassword(string username, string password, string newPassword)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CheckAndAutoCreateDefaultUsers()
        {
            throw new NotImplementedException();
        }

        public Task<bool> CheckIfPoEditable(string poNo)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CheckLogin(string userName, string password)
        {
            return true;
        }

        public Task<IEnumerable<UserInfo>> GetAllManageableUsers()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ManageGroup>> GetManageGroups()
        {
            throw new NotImplementedException();
        }

        public Task<UserInfo> GetManagerInfo(string username, string password)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PurchaseItem>> GetMorePos(string requestId, int itemsPerPage = 50, int page = -1)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPoNote(string poNo)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PurchaseItem>> GetPosStatusReport(string requestId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PurchaseOrder>> GetPosWithFilter(string vendorNo = null, int status = 0)
        {
            throw new NotImplementedException();
        }

        public Task<UserInfo> GetUserInfo(string username)
        {
            throw new NotImplementedException();
        }

        public Vendor GetVendorInfo(string username)
        {
            throw new NotImplementedException();
        }

        public Task<Vendor> GetVendorInfoAsync(string username)
        {
            throw new NotImplementedException();
        }

        public Vendor GetVendorInfoFromPoTable(string vendorNo)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetVendorPoCount(string vendorNo)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsValidManagerToken(string managerToken)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PurchaseOrder>> LoadPurchaseOrdersByVendorId(string vendorId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> MarkPoAsSent(string poNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<CheckList_Result_Item>> Picking_get_CheckListResult(string userName, string orderNo)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ResetVendorsPass(List<string> vendorNos, string defaultPattern = "{0}@123456")
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Vendor>> SearchVendorsManagedBy(string username, string vendorNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<TO_ItemTotal>> sp_get_TO_ItemTotal(string userName, string datekey, string typeFill, string dataFill,string Router)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_add_CheckListResult(List<CheckList_Result_Item> its, string userName, string orderNo)
        {
            throw new NotImplementedException();
        }

        public Task<string> sp_Picking_add_InvoiceHeader(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_add_InvoiceLine(List<invoice_Detail> it, string userName, int type)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_add_PackingLine(List<packingLine_Item> its, string userName)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_add_ReceiveAcceptOrder(string userName, RA_Item it)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_add_ReportLine(List<report_Detail> it, string userName, int type)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_add_SalesIn(List<salesIn_Detail> it, string userName, int type)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_add_SalesOut(List<salesOut_Detail> it, string userName, int type)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_add_TO_ItemTotal_Line(string userName, TO_ItemTotal it)
        {
            throw new NotImplementedException();
        }

        public Task<List<ItemWait>> sp_Picking_get_AcceptItemWait(string userName, string dateFill, string typeFill, string dataFill)
        {
            throw new NotImplementedException();
        }

        public Task<List<boxType_Item>> sp_Picking_get_BoxType(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<List<CheckList_Item>> sp_Picking_get_Checklist(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<List<Sub_CheckList_Item>> sp_Picking_get_ChecklistSub(string userName, string checkList_codecheck)
        {
            throw new NotImplementedException();
        }

        public Task<List<invoice_Item>> sp_Picking_get_Invoice_Item(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<List<invoice_Detail>> sp_Picking_get_Invoice_ItemDetail(string userName, string orderNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<orderList>> sp_Picking_get_Invoice_lsOrder(string userName, string vendorNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<vendorList>> sp_Picking_get_Invoice_lsVendor(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<List<SalesOut_Line>> sp_Picking_get_ItemForStore_least(string userName, string dateFill, string storeNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<orderList>> sp_Picking_get_lsOrder(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<List<packingLine_Item>> sp_Picking_get_Packing_Line(string userName, string storeNo, string dateFill)
        {
            throw new NotImplementedException();
        }

        public Task<List<RA_Item>> sp_Picking_get_ReceiveAcceptOrder(string userName, string dateFill)
        {
            throw new NotImplementedException();
        }

        public Task<List<report_Detail>> sp_Picking_get_ReportDetail(string userName, string orderNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<report_Item>> sp_Picking_get_ReportItem(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<List<reportVendor_list>> sp_Picking_get_ReportVendor(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<List<RouterItem>> sp_Picking_get_RouterType()
        {
            throw new NotImplementedException();
        }

        public Task<List<saleIn_Item>> sp_Picking_get_SaleIn(string userName, string vendorNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<salesIn_Detail>> sp_Picking_get_SaleInDetail(string userName, string orderNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<saleOut_Item>> sp_Picking_get_SaleOut(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<List<salesOut_Detail>> sp_Picking_get_SaleOutDetail(string userName, string orderNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<SalesOut_Header>> sp_Picking_get_SaleOut_v2(string userName, string dateFill)
        {
            throw new NotImplementedException();
        }

        public Task<List<SalesIn_byDate>> sp_Picking_get_SalesIn_byDate(string userName, string dateFill, string itemNo)
        {
            throw new NotImplementedException();
        }

        public Task<List<SalesOut_byDate>> sp_Picking_get_SalesOut_byDate(string userName, string dateFill, string itemNo, string typeFill)
        {
            throw new NotImplementedException();
        }

        public Task<List<SalesOut_Line>> sp_Picking_get_SalesOut_Detail_v2(string userName, string dateFill, string storeNo)
        {
            throw new NotImplementedException();
        }

        public Task<int> sp_Picking_get_total_NewOrderForStore(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<List<SalesOut_Line>> sp_Picking_get_TO_Detail_Line(string userName, string dateFill, string typeFill, string dataFill, string Router)
        {
            throw new NotImplementedException();
        }

        public Task<List<userRole_Item>> sp_Picking_get_UserRole(string username)
        {
            throw new NotImplementedException();
        }

        public Task<List<vendor_Item>> sp_Picking_get_VendorPOCount(string username)
        {
            throw new NotImplementedException();
        }

        public Task<List<volumeByItem>> sp_Picking_get_VolumeByItem(string userName, string dateFill, string storeNo)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_update_InvoiceHeader(string userName, string invoiceNo, int status)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_update_Report(string userName, string orderNo, int status)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_update_SalesIn(string userName, string orderNo, int status)
        {
            throw new NotImplementedException();
        }

        public Task<bool> sp_Picking_update_SalesOut(string userName, string orderNo, int status)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdatePoNote(PurchaseOrder po)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdatePurchaseOrder(PurchaseOrder po)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserInfo(UserInfo ui, string newPassword = null)
        {
            throw new NotImplementedException();
        }

        Task<PosSet> IDataAccess.GetPosWithFilter(string vendorNo = null, int confirmStatus = -1, int sentStatus = -1, int receivedStatus = -1, DateTime? fromOrderDate = null, DateTime? toOrderDate = null, DateTime? fromConfirmDate = null, DateTime? toConfirmDate = null, string locationCode = null)
        {
            throw new NotImplementedException();
        }
    }
}