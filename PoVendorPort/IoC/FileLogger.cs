﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.IoC
{
    using PoVendorPortCommon;
    using System.IO;
    using System.Threading.Tasks;
    public class FileLogger : ILogger
    {
        public FileLogger(string logfile)
        {            
            _logfile = logfile;
        }
        string _logfile;
        public async Task Write(string message)
        {
            try
            {
                using (var sw = File.AppendText(_logfile))
                {
                    await sw.WriteLineAsync(message);                    
                }
            }
            catch { }
        }
    }
}