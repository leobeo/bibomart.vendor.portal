using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using System.Web.Configuration;
using PoVendorPortCommon;

namespace PoVendorPort
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            IoC.UnityContainerConfig.LoadDependencies(container);
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            var ok = System.Data.SqlClient.SqlDependency.Start(PoVendorPortCommon.GlobalAppSettings.MainConnectionString);
            if (ok)
            {
                DependencyResolver.Current.GetService<PoVendorPortCommon.SqlCacheDependencyConfig>().EnableNotificationsForTables(PoVendorPortCommon.GlobalAppSettings.SqlDependencyTables);
            } else
            {
                var logger = DependencyResolver.Current.GetService<ILogger>();
                logger.Write(string.Format("{0} - Could not start SqlDependency, setting CacheSlidingDuration to just 20 seconds (as a fallback)", System.DateTime.Now));
                PoVendorPortCommon.GlobalAppSettings.CacheSlidingMinutes = 0.333;
            }
        }
    }
}