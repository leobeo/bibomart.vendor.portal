﻿using System.Web;
using System.Web.Optimization;

namespace PoVendorPort
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.unobtrusive-ajax*", "~/Scripts/jquery.signalR-2.2.2.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));
            bundles.Add(new ScriptBundle("~/bundles/userScripts").Include("~/Scripts/jquery.autocomplete.js", "~/Scripts/XdsoftAutoComplete.js", "~/Scripts/jquery.custom-scrollbar.js", "~/Scripts/vex.combined.min.js", "~/Scripts/StringExtensions.js", "~/Scripts/jQueryExtensions.js",  "~/Scripts/Highlighter.js", "~/Scripts/custom.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css", "~/Content/jquery.autocomplete.css", "~/Content/XdsoftAutoComplete.css", "~/Content/vex.css", "~/Content/vex-theme-default.css",
                      "~/Content/site.css", "~/Content/leftnav.css", "~/Content/jquery.custom-scrollbar.css"));
            bundles.Add(new ScriptBundle("~/bundles/polyfill").Include("~/Scripts/number-polyfill.min.js"));
        }
    }
}
