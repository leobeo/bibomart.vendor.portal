USE [BBM_POVENDOR]
GO
/****** Object:  StoredProcedure [dbo].[sp_getPurchaseOrdersByVendorNo]    Script Date: 6/21/2017 6:20:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE proc [dbo].[sp_getPurchaseOrdersByVendorNo](@vendorNo varchar(50))
as
begin
SELECT TOP 1000 [PO_ID]
      ,[Buy-from Vendor No_] as VendorNo
      ,[Buy-from Vendor Name] as VendorName
      ,[No_] as PoNo
      ,[Order Date] as OrderDate
      ,[Expected Receipt Date] as ExpectedReceiptDate
	  ,[modifydate]
	  ,[Status] as PoStatus
	  ,[Approved]
      ,[Location Code] as LocationCode
      ,[Item No_] as ItemNo
      ,[Description]
	  ,[Expected Receipt Date] as ExpectedReceiptDate
      ,[Line No_] as [LineNo]
      ,[Quantity] as Quantity
      ,[Qty_ to Receive] as QuantityToReceive
      ,[Direct Unit Cost] as DirectUnitCost
      ,[Line Amount] as LineAmount      
      ,[Status] as PoStatus
	  ,[Approved]
  FROM [BBM_POVENDOR].[dbo].[TBL_Purchase_Order] where [Buy-from Vendor No_] = @vendorNo
end

--drop proc sp_getPurchaseOrdersByVendorNo
GO
/****** Object:  StoredProcedure [dbo].[sp_getVendorInfo]    Script Date: 6/21/2017 6:20:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_getVendorInfo](@username varchar(80))
as
select MaPhongBan as VendorNo from TSY_USER where username = @username
GO
