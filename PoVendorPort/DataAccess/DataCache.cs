﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.DataAccess
{
    using System.Threading.Tasks;
    using PoVendorPortCommon;
    using PoVendorPortCommon.Models;
    using Microsoft.AspNet.Identity;
    using System.Web.Caching;
    using PoVendorPort.IoC;
    using SignalR;
    using PoVendorPortCommon.Models.Picking;
    using PoVendorPortCommon.Models.Picking.Invoice;
    using PoVendorPortCommon.Models.Picking.CheckList;
    using PoVendorPortCommon.Models.Picking.Packing;
    using PoVendorPortCommon.Models.Picking.Sales_v2;
    using PoVendorPortCommon.Models.Picking.SalesOut_v2;
    using PoVendorPortCommon.Models.Picking.ReceiveAccept;

    public class DataCache : IDataAccess
    {
        public TimeSpan CacheSlidingDuration { get; set; } = TimeSpan.FromMinutes(60);
        public DataCache(IDataAccess da, string connectionString, MainHubContext mainHubContext, ILogger logger)
        {
            _da = da;
            _logger = logger;
            _connectionString = connectionString;
            _mainHubContext = mainHubContext;
        }
        IDataAccess _da;
        ILogger _logger;
        string _connectionString;
        MainHubContext _mainHubContext;
        public Task<bool> CheckLogin(string username, string password)
        {
            return _da.CheckLogin(username, password);
        }

        public async Task<IEnumerable<PurchaseOrder>> LoadPurchaseOrdersByVendorId(string vendorId)
        {
            if (vendorId == null) return new List<PurchaseOrder>();
            var key = string.Format("vendor_{0}", vendorId);
            var uid = HttpContext.Current.User.Identity.GetUserId();
            Vendor vendor = null;
            if(HttpContext.Current != null)
            {
                var cache = HttpContext.Current.Cache;
                vendor = cache[key] as Vendor;
                if (vendor != null)
                {                                        
                    if(vendor.PurchaseOrders != null) return vendor.PurchaseOrders;
                }
            }
            var pos = await _da.LoadPurchaseOrdersByVendorId(vendorId);
            if(HttpContext.Current != null)
            {
                //try caching the vendor ONLY when the vendorId matches with the current vendor (of the current authorized user).
                if (vendor == null)
                {
                    vendor = await GetVendorInfoAsync(HttpContext.Current.User.Identity.GetUserId());
                    if (vendor != null && vendor.No == vendorId)
                    {
                        SqlCacheDependency cd = SqlCacheDependencyConfig.CreateCacheDependency(_connectionString, vendor.No, _logger);

                        HttpContext.Current.Cache.Insert(key, vendor, cd, System.Web.Caching.Cache.NoAbsoluteExpiration, cd == null ? TimeSpan.FromSeconds(20) : CacheSlidingDuration, System.Web.Caching.CacheItemPriority.Default, null);
                        vendor.PurchaseOrders = pos;
                    }
                }                
            }
            return pos;
        }

        public async Task<Vendor> GetVendorInfoAsync(string username)
        {
            var key = string.Format("user_{0}", username);
            if(HttpContext.Current != null)
            {
                if(HttpContext.Current.Cache[key] != null)
                {
                    return HttpContext.Current.Cache[key] as Vendor;
                }
            }
            var vi = await _da.GetVendorInfoAsync(username);
            if(HttpContext.Current != null && vi != null)
            {
                if (vi.No != null)
                {
                    SqlCacheDependency cd = SqlCacheDependencyConfig.CreateCacheDependency(_connectionString, vi.No, _logger);
                    HttpContext.Current.Cache.Insert(key, vi, cd, System.Web.Caching.Cache.NoAbsoluteExpiration, cd == null ? TimeSpan.FromSeconds(20) : CacheSlidingDuration, System.Web.Caching.CacheItemPriority.Default, cacheItemRemovedCallback);
                    
                    cd = SqlCacheDependencyConfig.CreateCacheDependency(_connectionString, vi.No, _logger);
                    HttpContext.Current.Cache.Insert(string.Format("vendor_{0}",vi.No), vi, cd, System.Web.Caching.Cache.NoAbsoluteExpiration, cd == null ? TimeSpan.FromSeconds(20) : CacheSlidingDuration, System.Web.Caching.CacheItemPriority.Default, null);
                    
                }                                
            }
            return vi;
        }

        public Task<bool> UpdatePurchaseOrder(PurchaseOrder po)
        {
            return _da.UpdatePurchaseOrder(po);
        }
        /// <summary>
        /// This checks if the po is still in the cache to update the saved info.
        /// Otherwise we do not need to do anything because the next loading will load a fresh data (which have been saved before).
        /// </summary>
        /// <param name="po"></param>
        public static void UpdateCachedPo(PurchaseOrder po)
        {
            if (HttpContext.Current == null || po == null || po.VendorNo == null) return;
            var cache = HttpContext.Current.Cache;            
            var vendor = cache[string.Format("vendor_{0}", po.VendorNo)] as Vendor;
            if (vendor == null || vendor.PurchaseOrders == null) return;
            var matchedPo = vendor.PurchaseOrders.FirstOrDefault(e => po.PoNo == e.PoNo);
            if (matchedPo == null) return;
            matchedPo.Status = po.Status;
            matchedPo.LastApprovedDate = po.LastApprovedDate;
            var cachedPurItems = matchedPo.PurchaseItems.ToDictionary(e => $"{e.ItemNo}_{e.LineNo}", e => e);
            foreach(var item in po.PurchaseItems)
            {
                PurchaseItem ci;
                if(cachedPurItems.TryGetValue($"{item.ItemNo}_{item.LineNo}", out ci))
                {
                    ci.QtyChanged = item.QuantityToReceive != ci.QuantityToReceive ? 1 : 0;
                    ci.QuantityToReceive = item.QuantityToReceive;
                    //NOTE: Currently we require hours to be submit, so we compare the whole datetimes, not just the date to detect changes
                    ci.DateChanged = item.ExpectedReceiptTime != ci.ExpectedReceiptTime ? 1 : 0;
                    ci.ExpectedReceiptDate = item.ExpectedReceiptDate;
                    ci.ExpectedReceiptTime = item.ExpectedReceiptTime;
                    ci.PoStatus = item.PoStatus = 1;
                }
            }
        }

        public Task<bool> ChangePassword(string username, string password, string newPassword)
        {
            return _da.ChangePassword(username, password, newPassword);
        }
        #region cache dependency changed callback
        void cacheItemRemovedCallback(string key, object item, CacheItemRemovedReason removedReason)
        {
            var actualUsername = key.Split('_').LastOrDefault();//the key here may have format of user_username or vendor_username
            if(removedReason == CacheItemRemovedReason.DependencyChanged)
            {
                _mainHubContext.RefreshClient(actualUsername);
            }
        }

        public async Task<bool> MarkPoAsSent(string poNo)
        {
            return await _da.MarkPoAsSent(poNo);
        }

        public Task<bool> UpdatePoNote(PurchaseOrder po)
        {
            return _da.UpdatePoNote(po);
        }

        public Task<string> GetPoNote(string poNo)
        {
            return _da.GetPoNote(poNo);
        }

        public Task<UserInfo> GetUserInfo(string username)
        {
            return _da.GetUserInfo(username);
        }

        public Task<bool> UpdateUserInfo(UserInfo ui, string newPassword = null)
        {
            return _da.UpdateUserInfo(ui, newPassword);
        }

        public Task<IEnumerable<string>> AddVendors(List<Tuple<string, string>> vendorCredentials, bool updatePassForExistingUsers = false)
        {
            return _da.AddVendors(vendorCredentials, updatePassForExistingUsers);
        }

        public Task<bool> ResetVendorsPass(List<string> vendorNos, string defaultPattern = "{0}@123456")
        {
            return _da.ResetVendorsPass(vendorNos, defaultPattern);
        }

        public Task<IEnumerable<Vendor>> SearchVendorsManagedBy(string username, string vendorNo)
        {
            return _da.SearchVendorsManagedBy(username, vendorNo);
        }

        public Task<IEnumerable<UserInfo>> GetAllManageableUsers()
        {
            return _da.GetAllManageableUsers();
        }

        public Task<bool> CheckAndAutoCreateDefaultUsers()
        {
            return _da.CheckAndAutoCreateDefaultUsers();
        }

        public Vendor GetVendorInfo(string username)
        {
            return _da.GetVendorInfo(username);
        }

        public Vendor GetVendorInfoFromPoTable(string vendorNo)
        {
            return _da.GetVendorInfoFromPoTable(vendorNo);
        }

        public Task<IEnumerable<string>> AddDefaultVendors(List<string> vendorCodes, bool updatePassForExistingUsers = false, string passwordPattern = "{0}@123456")
        {
            return _da.AddDefaultVendors(vendorCodes, updatePassForExistingUsers, passwordPattern);
        }

        public Task<int> GetVendorPoCount(string vendorNo)
        {
            return _da.GetVendorPoCount(vendorNo);
        }

        public Task<IEnumerable<ManageGroup>> GetManageGroups()
        {
            return _da.GetManageGroups();
        }

        public Task<bool> ApprovePo(string pono)
        {
            return _da.ApprovePo(pono);
        }

        public Task<bool> IsValidManagerToken(string managerToken)
        {
            return _da.IsValidManagerToken(managerToken);
        }

        public Task<UserInfo> GetManagerInfo(string username, string password)
        {
            return _da.GetManagerInfo(username, password);
        }

        public Task<PosSet> GetPosWithFilter(string vendorNo = null, int confirmStatus = -1, int sentStatus = -1, int receivedStatus = -1, DateTime? fromOrderDate = null, DateTime? toOrderDate = null, DateTime? fromConfirmDate = null, DateTime? toConfirmDate = null, string locationCode = null)
        {
            return _da.GetPosWithFilter(vendorNo, confirmStatus, sentStatus, receivedStatus, fromOrderDate, toOrderDate, fromConfirmDate, toConfirmDate, locationCode);
        }

        public Task<IEnumerable<PurchaseItem>> GetMorePos(string requestId, int itemsPerPage = 50, int page = -1)
        {
            return _da.GetMorePos(requestId, itemsPerPage, page);
        }

        public Task<IEnumerable<PurchaseItem>> GetPosStatusReport(string requestId)
        {
            return _da.GetPosStatusReport(requestId);
        }

        public Task<bool> CheckIfPoEditable(string poNo)
        {
            return _da.CheckIfPoEditable(poNo);
        }

        public Task<List<userRole_Item>> sp_Picking_get_UserRole(string username)
        {
            return _da.sp_Picking_get_UserRole(username);
        }

        public Task<List<vendor_Item>> sp_Picking_get_VendorPOCount(string username)
        {
            return _da.sp_Picking_get_VendorPOCount(username);
        }

        public Task<List<saleIn_Item>> sp_Picking_get_SaleIn(string userName, string vendorNo)
        {
            return _da.sp_Picking_get_SaleIn(userName, vendorNo);
        }

        public Task<List<salesIn_Detail>> sp_Picking_get_SaleInDetail(string userName, string orderNo)
        {
            return _da.sp_Picking_get_SaleInDetail(userName, orderNo);
        }

        public Task<List<saleOut_Item>> sp_Picking_get_SaleOut(string userName)
        {
            return _da.sp_Picking_get_SaleOut(userName);
        }

        public Task<List<salesOut_Detail>> sp_Picking_get_SaleOutDetail(string userName, string orderNo)
        {
            return _da.sp_Picking_get_SaleOutDetail(userName, orderNo);
        }

        public Task<bool> sp_Picking_update_SalesIn(string userName, string orderNo, int status)
        {
            return _da.sp_Picking_update_SalesIn(userName, orderNo,status);
        }

        public Task<bool> sp_Picking_update_SalesOut(string userName, string orderNo, int status)
        {
            return _da.sp_Picking_update_SalesOut(userName, orderNo,status);
        }

        public Task<bool> sp_Picking_add_SalesIn(List<salesIn_Detail> it, string userName, int type)
        {
            return _da.sp_Picking_add_SalesIn(it, userName,type);
        }

        public Task<bool> sp_Picking_add_SalesOut(List<salesOut_Detail> it, string userName, int type)
        {
            return _da.sp_Picking_add_SalesOut(it, userName,type);
        }

        public Task<List<report_Item>> sp_Picking_get_ReportItem(string userName)
        {
            return _da.sp_Picking_get_ReportItem(userName);
        }

        public Task<List<report_Detail>> sp_Picking_get_ReportDetail(string userName, string orderNo)
        {
            return _da.sp_Picking_get_ReportDetail(userName, orderNo);
        }

        public Task<bool> sp_Picking_update_Report(string userName, string orderNo, int status)
        {
            return _da.sp_Picking_update_Report(userName, orderNo, status);
        }

        public Task<bool> sp_Picking_add_ReportLine(List<report_Detail> it, string userName, int type)
        {
            return _da.sp_Picking_add_ReportLine(it, userName, type);
        }

        public Task<List<reportVendor_list>> sp_Picking_get_ReportVendor(string userName)
        {
            return _da.sp_Picking_get_ReportVendor(userName);
        }

        public Task<List<orderList>> sp_Picking_get_Invoice_lsOrder(string userName, string vendorNo)
        {
            return _da.sp_Picking_get_Invoice_lsOrder(userName, vendorNo);
        }

        public Task<List<vendorList>> sp_Picking_get_Invoice_lsVendor(string userName)
        {
            return _da.sp_Picking_get_Invoice_lsVendor(userName);
        }

        public Task<string> sp_Picking_add_InvoiceHeader(string userName)
        {
            return _da.sp_Picking_add_InvoiceHeader(userName);
        }

        public Task<List<invoice_Item>> sp_Picking_get_Invoice_Item(string userName)
        {
            return _da.sp_Picking_get_Invoice_Item(userName);
        }

        public Task<List<invoice_Detail>> sp_Picking_get_Invoice_ItemDetail(string userName, string orderNo)
        {
            return _da.sp_Picking_get_Invoice_ItemDetail(userName, orderNo);
        }

        public Task<bool> sp_Picking_add_InvoiceLine(List<invoice_Detail> it, string userName, int type)
        {
            return _da.sp_Picking_add_InvoiceLine(it, userName, type);
        }

        public Task<bool> sp_Picking_update_InvoiceHeader(string userName, string invoiceNo, int status)
        {
            return _da.sp_Picking_update_InvoiceHeader(userName, invoiceNo, status);
        }

        public Task<List<CheckList_Item>> sp_Picking_get_Checklist(string userName)
        {
            return _da.sp_Picking_get_Checklist(userName);
        }

        public Task<List<Sub_CheckList_Item>> sp_Picking_get_ChecklistSub(string userName, string checkList_codecheck)
        {
            return _da.sp_Picking_get_ChecklistSub(userName, checkList_codecheck);
        }

        public Task<bool> sp_Picking_add_CheckListResult(List<CheckList_Result_Item> its, string userName, string orderNo)
        {
            return _da.sp_Picking_add_CheckListResult(its, userName, orderNo);
        }

        public Task<bool> sp_Picking_add_PackingLine(List<packingLine_Item> its, string userName)
        {
            return _da.sp_Picking_add_PackingLine(its, userName);
        }

        public Task<List<CheckList_Result_Item>> Picking_get_CheckListResult(string userName, string orderNo)
        {
            return _da.Picking_get_CheckListResult(userName, orderNo);
        }

        public Task<int> sp_Picking_get_total_NewOrderForStore(string userName)
        {
            return _da.sp_Picking_get_total_NewOrderForStore(userName);
        }

        public Task<List<packingLine_Item>> sp_Picking_get_Packing_Line(string userName, string storeNo, string dateFill)
        {
            return _da.sp_Picking_get_Packing_Line(userName, storeNo,dateFill);
        }

        public Task<List<SalesIn_byDate>> sp_Picking_get_SalesIn_byDate(string userName, string dateFill,string itemNo)
        {
            return _da.sp_Picking_get_SalesIn_byDate(userName, dateFill,itemNo);
        }

        public Task<List<SalesOut_byDate>> sp_Picking_get_SalesOut_byDate(string userName, string dateFill,string itemNo, string typeFill)
        {
            return _da.sp_Picking_get_SalesOut_byDate(userName, dateFill, itemNo,typeFill);
        }

        public Task<List<SalesOut_Header>> sp_Picking_get_SaleOut_v2(string userName, string dateFill)
        {
            return _da.sp_Picking_get_SaleOut_v2(userName, dateFill);
        }

        public Task<List<SalesOut_Line>> sp_Picking_get_SalesOut_Detail_v2(string userName, string dateFill, string storeNo)
        {
            return _da.sp_Picking_get_SalesOut_Detail_v2(userName, dateFill, storeNo);
        }

        public Task<List<TO_ItemTotal>> sp_get_TO_ItemTotal(string userName, string datekey, string typeFill, string dataFill,string Router)
        {
            return _da.sp_get_TO_ItemTotal(userName, datekey, typeFill, dataFill, Router);
        }

        public Task<bool> sp_Picking_add_TO_ItemTotal_Line(string userName, TO_ItemTotal it)
        {
            return _da.sp_Picking_add_TO_ItemTotal_Line(userName, it);
        }

        public Task<List<SalesOut_Line>> sp_Picking_get_TO_Detail_Line(string userName, string dateFill, string typeFill, string dataFill, string Router)
        {
            return _da.sp_Picking_get_TO_Detail_Line(userName, dateFill, typeFill, dataFill, Router);
        }

        public Task<List<RouterItem>> sp_Picking_get_RouterType()
        {
            return _da.sp_Picking_get_RouterType();
        }

        public Task<List<SalesOut_Line>> sp_Picking_get_ItemForStore_least(string userName, string dateFill, string storeNo)
        {
            return _da.sp_Picking_get_ItemForStore_least(userName, dateFill, storeNo);
        }

        public Task<List<ItemWait>> sp_Picking_get_AcceptItemWait(string userName, string dateFill, string typeFill, string dataFill)
        {
            return _da.sp_Picking_get_AcceptItemWait(userName, dateFill, typeFill, dataFill);
        }

        public Task<List<boxType_Item>> sp_Picking_get_BoxType(string userName)
        {
            return _da.sp_Picking_get_BoxType(userName);
        }

        public Task<List<volumeByItem>> sp_Picking_get_VolumeByItem(string userName, string dateFill, string storeNo)
        {
            return _da.sp_Picking_get_VolumeByItem(userName, dateFill, storeNo);
        }

        public Task<List<RA_Item>> sp_Picking_get_ReceiveAcceptOrder(string userName, string dateFill)
        {
            return _da.sp_Picking_get_ReceiveAcceptOrder(userName, dateFill);
        }

        public Task<bool> sp_Picking_add_ReceiveAcceptOrder(string userName, RA_Item it)
        {
            return _da.sp_Picking_add_ReceiveAcceptOrder(userName, it);
        }
        #endregion
    }
}