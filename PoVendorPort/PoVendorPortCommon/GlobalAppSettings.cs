﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon
{
    public static class GlobalAppSettings
    {        
        public static bool EnabledExecutionTimeLogging { get; set; }
        public static ClientIPResolvingMethod ClientIPResolvingMethod { get; set; }
        public static bool ClientIPFilterEnabled { get; set; }
        public static bool ShowClientSubnet { get; set; }
        public static double CacheSlidingMinutes { get; set; }
        public static string LogPath { get; set; }
        public static string[] SqlDependencyTables { get; set; }//comma-separated list
        public static string MainConnectionString { get; set; }

        public static string AdminCode { get; set; }
        public static string ManagerCodePattern { get; set; }
        public static string SqlDependencyQueueName = "PoDependencyQueue";
    }
    public enum ClientIPResolvingMethod
    {
        WithoutJS,
        WithJS,
        WithoutJSButWithLog //this is the same as the first option but all IPs resolved by JS which are different from the ones resolved using HttpRequest
        //will be logged (for later examination).
    }
}
