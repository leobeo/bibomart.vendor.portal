﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace PoVendorPortCommon.ModelBinders
{
    using Models;
    public class VendorsListModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(Type modelType)
        {
            if(modelType == typeof(List<Vendor>))
            {
                return DependencyResolver.Current.GetService<VendorsListModelBinder>();
            }
            return null;
        }
    }
}
