﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Text.RegularExpressions;

namespace PoVendorPortCommon.ModelBinders
{
    using Models;
    using System.Web;

    public class VendorsListModelBinder : IModelBinder
    {
        public VendorsListModelBinder(IDataAccess da)
        {
            _da = da;
        }        
        IDataAccess _da;
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelName == null) return null;
            var input = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if(input != null)
            {
                var s = input.AttemptedValue;
                s = HttpUtility.HtmlDecode(s).Replace("&nbsp;", " ");
                var vn = Regex.Matches(s, @"\w+").Cast<Match>().Select(e => e.Value);
                var result = new List<Vendor>();
                foreach(var e in vn)
                {
                    var vi = _da.GetVendorInfo(e);
                    if(vi == null)
                    {
                        vi = _da.GetVendorInfoFromPoTable(e);
                        if (vi == null)
                        {
                            vi = new Vendor
                            {
                                No = e,
                                State = VendorState.NoPoNoUser
                            };
                        } else
                        {
                            vi.State = VendorState.PoNoUser;
                        }
                    }
                    result.Add(vi);
                }
                return result;
            }
            return null;
        }
    }
}
