﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon
{
    public interface ILogger
    {
        Task Write(string message);
    }
}
