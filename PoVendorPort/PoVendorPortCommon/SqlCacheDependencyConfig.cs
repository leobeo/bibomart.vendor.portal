﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon
{
    using System.Web.Caching;
    using System.Data.SqlClient;
    public class SqlCacheDependencyConfig
    {
        public SqlCacheDependencyConfig(string connectionString, ILogger logger)
        {
            ConnectionString = connectionString;
            Logger = logger;
            
        }

        public string ConnectionString { get; private set; }
        public ILogger Logger { get; private set; }
        public bool EnableNotificationsForTables(params string[] tableNames)
        {
            try
            {
                SqlCacheDependencyAdmin.EnableNotifications(ConnectionString);                
            }
            catch(Exception ex)
            {
                Logger.Write(string.Format("{0} - Could not enable notifications for db, Exception: {1}", DateTime.Now, ex));
                return false;
            }
            try
            {
                SqlCacheDependencyAdmin.EnableTableForNotifications(ConnectionString, tableNames);
            } catch(Exception ex)
            {
                Logger.Write(string.Format("{0} - Could not enable notifications for tables: {1}, Exception: {2}", DateTime.Now, string.Join(",", tableNames), ex));
                return false;
            }
            Logger.Write(string.Format("{0} - Table: {1} has notifications enabled OK!", DateTime.Now, string.Join(",", tableNames)));
            return true;
        }
        public bool DisableNotificationsForTables(params string[] tableNames)
        {
            try
            {
                SqlCacheDependencyAdmin.DisableTableForNotifications(ConnectionString, tableNames);
                Logger.Write(string.Format("{0} - Table: {1} has notifications disabled OK!", DateTime.Now, string.Join(",", tableNames)));
                return true;
            } catch(Exception ex)
            {
                //this means that the table notification is already disabled
                if (ex is DatabaseNotEnabledForNotificationException) return true;
                Logger.Write(string.Format("{0} - Cannot disable notifications for table: {1}, Exception: {2}", DateTime.Now, ex));
                return false;
            }
        }
        
        public static SqlCacheDependency CreateCacheDependency(string connectionString, string vendorNo, ILogger logger = null)
        {
            SqlCacheDependency result = null;
            Exception exception = null;
            try
            {
                var ok = true;// SqlDependency.Start(GlobalAppSettings.MainConnectionString);
                if (ok)
                {
                    using (var con = new System.Data.SqlClient.SqlConnection(connectionString))
                    {
                        using (var cmd = con.CreateCommand())
                        {
                            con.Open();
                            cmd.CommandText = "sp_defineDependencyArea";
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("vendorNo", vendorNo);
                            result = new SqlCacheDependency(cmd);
                            cmd.ExecuteNonQuery();                            
                        }
                    }
                }
            } catch(Exception ex)
            {
                exception = ex;
            }
            if(result == null)
            {
                if (logger != null)
                {
                    logger.Write(string.Format("{0} - Could not create SqlCacheDependency to cache vendorNo: {1}. Use a default CacheSlidingDuration of 20 seconds instead. Exception: {2}", DateTime.Now, vendorNo, exception));
                }
            }
            return result;
        }
        public static SqlDependency CreatePoDependency(string connectionString, string defineDependencyProc = "sp_definePoDependency", ILogger logger = null)
        {
            SqlDependency result = null;
            Exception exception = null;
            try
            {
                var ok = SqlDependency.Start(GlobalAppSettings.MainConnectionString);
                if (ok)
                {
                    using (var con = new SqlConnection(connectionString))
                    {
                        using (var cmd = con.CreateCommand())
                        {
                            cmd.CommandText = defineDependencyProc;
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            result = new SqlDependency(cmd);
                            con.Open();
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            } catch(Exception ex)
            {
                exception = ex;
            }
            if (result == null)
            {
                if (logger != null)
                {
                    logger.Write(string.Format("{0} - Could not create SqlDependency to receive change from PO table and auto-create default user. Exception: {1}", DateTime.Now, exception));
                }
            }
            return result;
        }
    }
}
