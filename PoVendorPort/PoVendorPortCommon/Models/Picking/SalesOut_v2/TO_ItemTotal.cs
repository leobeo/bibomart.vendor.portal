﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.SalesOut_v2
{
    public class TO_ItemTotal
    {
        public string dateKey { set; get; }
        public string itemNo { set; get; }
        public string itemName { set; get; }
        public string Router { set; get; }
        public string brandName { set; get; }
        public string divisionCode { set; get; }
        public string categoryCode { set; get; }
        public string groupCode { set; get; }
        public decimal quantity { set; get; }
        public int is_Done { set; get; }
    }
}
