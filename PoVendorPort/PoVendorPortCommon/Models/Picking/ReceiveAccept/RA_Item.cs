﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.ReceiveAccept
{
    public class RA_Item
    {
        public string vendorNo { set; get; }
        public string vendorName { set; get; }
        public string orderNo { set; get; }
        public string orderDate { set; get; }
        public string shipDate { set; get; }
        public string shipDatetime { set; get; }
        public string transTo { set; get; }
        public int totalItem { set; get; }
        public int totalQuantity { set; get; }
        public int status_Code { set; get; }
        public string status_Content { set; get; }
    }
}
