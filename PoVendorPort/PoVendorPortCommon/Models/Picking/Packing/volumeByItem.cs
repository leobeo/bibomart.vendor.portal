﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Packing
{
    public class volumeByItem
    {
        public string orderNo { set; get; }
        public decimal volume { set; get; }
    }
}
