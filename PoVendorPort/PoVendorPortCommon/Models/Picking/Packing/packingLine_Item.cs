﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Packing
{
    public class packingLine_Item
    {
        public string orderNo { set; get; }
        public string boxCode { set; get; }
        public string boxName { set; get; }
        public int packTotal { set; get; }
        public decimal coreVolume { set; get; }
        public decimal volume { set; get; }
        public string createdBy { set; get; }
        public string createdDate { set; get; }
    }
}
