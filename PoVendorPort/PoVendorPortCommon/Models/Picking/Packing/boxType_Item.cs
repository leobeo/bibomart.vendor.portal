﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Packing
{
    public class boxType_Item
    {
        public string boxCode { set; get; }
        public string boxName { set; get; }
        public decimal height { set; get; }
        public decimal width { set; get; }
        public decimal length { set; get; }
        public decimal volume { set; get; }
    }
}
