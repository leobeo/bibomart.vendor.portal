﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Invoice
{
    public class invoice_Item
    {
        public string invoiceNo { set; get; }
        public string vendorNo { set; get; }
        public string vendorName { set; get; }
        public int order_total { set; get; }
        public decimal item_total { set; get; }
        public decimal amount_total { set; get; }
        public string createdDate { set; get; }
        public int status { set; get; }
        public string status_content { set; get; }
    }
}
