﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Invoice
{
    public class invoice_Detail
    {
        public string invoiceNo { set; get; }
        public string vendorNo { set; get; }
        public string orderNo { set; get; }
        public string itemNo { set; get; }
        public string itemName { set; get; }
        public int item_total { set; get; }
        public decimal corePrice { set; get; }
        public decimal discount { set; get; }
        public decimal amount_total { set; get; }
        public string createdDate { set; get; }
        public int status { set; get; }
        public int isEdit { set; get; } = 0;
    }
}
