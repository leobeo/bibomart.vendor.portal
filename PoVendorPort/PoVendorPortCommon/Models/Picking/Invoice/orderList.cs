﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Invoice
{
    public class orderList
    {
        public string orderNo { set; get; }
        public string vendorNo { set; get; }
        public string vendorName { set; get; }
        public int item_total { set; get; }
        public decimal amount_total { set; get; }
        public string createdDAte { set; get; }
        public string lastModifyDate { set; get; }
    }
}
