﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking
{
    public class saleIn_Item
    {
        public string vendorNo { set; get; }
        public string vendorName { set; get; }
        public string orderNo { set; get; }
        public string storeNo { set; get; }
        public string storeName { set; get; }
        public int quantityItem { set; get; }
        public int status_code { set; get; }
        public string status_Content { set; get; }
        public string status_color { set; get; }
        public string createdDate { set; get; }
        public int is_checkList { set; get; }
        public int totalPack{set;get;}
    }
}
