﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking
{
    public class vendor_Item
    {
        public string vendorNo { set; get; }
        public string vendorName { set; get; }
        public int quantityOrder { set; get; }
    }
}
