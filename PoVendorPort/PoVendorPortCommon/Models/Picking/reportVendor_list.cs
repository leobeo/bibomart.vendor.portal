﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking
{
    public class reportVendor_list
    {
        public string vendorNo { set; get; }
        public string vendorName { set; get; }
        public int order_total { set; get; }
    }
}
