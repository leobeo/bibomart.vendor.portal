﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking
{
    public class salesIn_Detail
    {
        public string orderNo { set; get; }
        public string storeNo { set; get; }
        public string itemNo { set; get; }
        public string itemName { set; get; }
        public decimal orderQuantity { set; get; }
        public decimal realQuantity { set; get; }
        public decimal corePrice { set; get; }
        public string createdDate { set; get; }
        public string modifyDate { set; get; }
        public int status { set; get; }
        public int isEdit { set; get; } = 0;
    }
}
