﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking
{
    public class userRole_Item
    {
        public string userName { set; get; }
        public string rolesCode { set; get; }
        public string controlNo { set; get; }
        public string controlName { set; get; }
        public string moduleName { set; get; }
        public string moduleController { set; get; }
        public string moduleAction { set; get; }
        public int createAction { set; get; }
        public int editAction { set; get; }
        public int acceptAction { set; get; }
        public int typeGroup { set; get; }
        public int level_m { set; get; }
        public int webView { set; get; }
    }
}
