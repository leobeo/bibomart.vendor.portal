﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.CheckList
{
    public class Sub_CheckList_Item
    {
        public string checkList_codecheck { set; get; }
        public string title { set; get; }
        public string code { set; get; }
        public string name { set; get; }
        public int status { set; get; }
        public int inputType { set; get; }
    }
}
