﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.CheckList
{
    public class CheckList_Result_Item
    {
        public string orderNo { set;get;}
        public string codeCheck { set; get; }
        public int is_codeCheck { set; get; }
        public string nameCheck { set; get; }
        public int inputType { set; get; }
        public string subCheck { set; get; }
        public int is_subCheck { set; get; }
        public string sub_nameCheck { set; get; }
        public string descriptions { set; get; }
    }
}
