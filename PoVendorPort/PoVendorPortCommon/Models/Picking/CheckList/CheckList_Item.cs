﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.CheckList
{
    public class CheckList_Item
    {
        public string code { set; get; }
        public string codeCheck { set; get; }
        public string nameCheck { set; get; }
        public int inputType { set; get; }
        public int subcheck { set; get; }
        public int groupCode { set; get; }
        public int status { set; get; }
        public int required { set; get; }
    }
}
