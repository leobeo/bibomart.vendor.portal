﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking
{
    public class saleOut_Item
    {
        public string storeNo { set; get; }
        public string storeName { set; get; }
        public string orderNo { set; get; }
        public string orderType { set; get; }
        public int itemQuantity { set; get; }
        public int status_code { set; get; }
        public string status_Content { set; get; }
        public string createdDate { set; get; }
        public string modifyDate { set; get; }
        public int packTotal { set; get; }
        public int is_Packing { set; get; }
    }
}
