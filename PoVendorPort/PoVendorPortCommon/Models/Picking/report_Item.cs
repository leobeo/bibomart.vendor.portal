﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking
{
    public class report_Item
    {
        public string vendorNo { set; get; }
        public string vendorName { set; get; }
        public string orderNo { set; get; }
        public int status_code { set; get; }
        public string status_content { set; get; }
        public string createdDate { set; get; }
        public string lastModify { set; get; }
        public string lastModifyBy { set; get; }
    }
}
