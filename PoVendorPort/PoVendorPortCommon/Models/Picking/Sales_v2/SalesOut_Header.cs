﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Sales_v2
{
     public class SalesOut_Header
    {
        public string storeNo { set; get; }
        public string storeName { set; get; }
        public string createdDate { set; get; }
        public int is_Packing { set; get; }
        public int total_Packing { set; get; }
        public int total_orderNo { set; get; }
        public int itemQuantity { set; get; }
        public int totalQuantity { set; get; }
    }
}
