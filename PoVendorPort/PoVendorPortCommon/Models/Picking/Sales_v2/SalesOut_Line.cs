﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Sales_v2
{
    public class SalesOut_Line
    {
        public string orderNo { set; get; }
        public string storeNo { set; get; }
        public string itemNo { set; get; }
        public string itemName { set; get; }
        public string brand { set; get; }
        public string division { set; get; }
        public string category { set; get; }
        public string group { set; get; }
        public decimal corePrice { set; get; }
        public decimal orderQuantity { set; get; }
        public decimal realQuantity{set; get;}
        public int status { set; get; }
        public string status_Content { set; get; }
        public string createdDate { set; get; }
        public string modifyDate { set; get; }
        public int storeLayout { set; get; }
    }
}
