﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Sales_v2
{
    public class RouterItem
    {
        public string Router { set; get; }
        public string Calenders { set; get; }
        public string Note { set; get; }
    }
}
