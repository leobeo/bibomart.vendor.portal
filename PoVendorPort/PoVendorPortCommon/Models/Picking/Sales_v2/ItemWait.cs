﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking.Sales_v2
{
    public class ItemWait
    {
        public string storeNo { set; get; }
        public string storeName { set; get; }
        public string orderNo { set; get; }
        public string itemNo { set; get; }
        public string itemNam { set; get; }
        public decimal quantity { set; get; }
        public string userCreated { set; get; }
    }
}
