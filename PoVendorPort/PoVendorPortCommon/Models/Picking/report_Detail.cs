﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models.Picking
{
    public class report_Detail
    {
        public string orderNo { set; get; }
        public string itemNo { set; get; }
        public string itemName { set; get; }
        public string locationCode { set; get; }
        public decimal corePrice { set; get; }
        public decimal discountPercent { set; get; }
        public int orderQuantity { set; get; }
        public int realQuantity { set; get; }
        public string createdDate { set; get; }
        public string lastModify { set; get; }
        public int status { set; get; }
        public int isEdit { set; get; }=0;
    }
}
