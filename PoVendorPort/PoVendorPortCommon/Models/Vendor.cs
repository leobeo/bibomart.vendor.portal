﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("TBL_Vendor")]
    public class Vendor
    {
        [Column("VendorNo")]
        public string No { get; set; }//Currently only this is used
        public string Name { get; set; }
        [Column("Search Name")]
        public string SearchName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Contact { get; set; }
        public int Blocked { get; set; }
        [Column("Vietnamese Name")]
        public string VietnameseName { get; set; }
        [Column("Receiving Litmit Date")]
        public int MaxDaysToReceive { get; set; }
        public string Note { get; set; }
        public string UserName { get; set; }//and this
        [System.Web.Script.Serialization.ScriptIgnore]
        public IEnumerable<PurchaseOrder> PurchaseOrders { get; set; }
        #region non-entity properties
        public VendorState State { get; set; }
        public bool IsManaged { get; set; }
        public int PoCount { get; set; }
        #endregion
    }
    public enum VendorState
    {
        //the vendor has no account as well as no po (imported in po table)
        NoPoNoUser,
        //the vendor has po (imported in po table) and account (created in tsy_user)
        PoUser,
        //the vendor has po (imported in po table) but has no account
        PoNoUser,
        //the vendor has account but does not have any po
        NoPoUser
    }
}
