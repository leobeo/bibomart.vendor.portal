﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models
{
    public class ManageGroup
    {
        string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if(_name != value)
                {
                    _name = value;
                    if(value != null && value.StartsWith(Prefix))
                    {
                        _suffix = value.Substring(3);
                    } else
                    {
                        _suffix = null;
                    }
                }
            }
        }
        string _suffix;
        public string Suffix
        {
            get
            {
                return _suffix;
            }
            set
            {
                if(_suffix != value)
                {
                    _suffix = value;
                    if (value != null)
                    {
                        _name = $"{Prefix}{value}";
                    }
                    else _name = null;
                }
            }
        }
        public string ManagedVendorCodes { get; set; }//a comma-separated list of vendor codes managed by the group
        public const string Prefix = "GRP";
    }
}
