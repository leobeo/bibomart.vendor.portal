﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models
{
    public class SalesIn_byDate
    {
        public string orderNo{set; get;}
        public string ItemNo { set; get; }
        public string itemName { set; get; }
        public int status_code { set; get; }
        public string status_Content { set; get; }
        public string status_color { set; get; }
        public string createdDate { set; get; }
        public decimal quantity { set; get; }
        public decimal realQuantity { set; get; }
        public int totalPack { set; get; }
    }
}
