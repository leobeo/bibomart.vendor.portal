﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations.Schema;

namespace PoVendorPortCommon.Models
{    
    using System.ComponentModel.DataAnnotations;
    //[Table("")]
    public class UserInfo
    {        
        [Display(Name = "Tên đăng nhập")]
        [Required(ErrorMessage = "Bạn cần nhập tên đăng nhập")]
        public string UserName { get; set; }
        [Display(Name = "Tên đầy đủ")]
        //[TrackChange]
        public string FullName { get; set; }
        
        [Display(Name = "Mã nhóm")]
        [Required(ErrorMessage = "Bạn cần nhập mã nhóm")]
        //[TrackChange]        
        public string MaPB { get; set; }//ma phong ban
        public string UserGroupId { get; set; }   
        [Display(Name = "Mã NCC được quản lý: ")]
        public List<Vendor> ManagedVendors { get; set; }
        public List<UserInfo> ManagedUsers { get; set; }
        public bool IsAdmin
        {
            get
            {
                return MaPB?.ToLower() == GlobalAppSettings.AdminCode.ToLower();
            }
        }
        public bool IsManager
        {
            get
            {
                return Regex.IsMatch(MaPB ?? "", GlobalAppSettings.ManagerCodePattern, RegexOptions.IgnoreCase);
            }
        }
        public string AccessToken { get; set; }
    }
   
}
