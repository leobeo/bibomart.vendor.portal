﻿using DotNet.TabularModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models
{
    public class PoStatusReportItem
    {
        [Cell(Header = "Ngày đặt hàng")] //, NPOIFormatString = "dd/MM/yyyy")]
        public DateTime OrderDate { get; set; }
        [Cell(Header = "Mã đơn hàng")]
        public string PoNo { get; set; }
        [Cell(Header = "Mã NCC")]
        public string VendorNo { get; set; }
        [Cell(Header = "Tên NCC")]
        public string VendorName { get; set; }
        [Cell(Header = "Mã kho")]
        public string LocationCode { get; set; }
        [Cell(Header = "Mã sản phẩm")]
        public string ItemNo { get; set; }
        [Cell(Header = "Tên sản phẩm")]
        public string ItemName { get; set; }        
        [Cell(Header = "Số lượng")]
        public double Quantity { get; set; }
        [Cell(Header = "Số lượng nhận")]
        public int QuantityToReceive { get; set; }
        [Cell(Header = "Trạng thái xác nhận số lượng giao")]
        public string ConfirmedPoStatus { get; set; }
        [Cell(Header = "Thời gian giao hàng dự kiến")] //, NPOIFormatString = "dd/MM/yyyy HH:mm:ss")]
        public DateTime ExpectedReceiptTime { get; set; }
        [Cell(Header = "Trạng thái xác nhận đã gửi hàng")]
        public string POSentStatus { get; set; }
        [Cell(Header = "Ngày giờ xác nhận đã giao")] //, NPOIFormatString = "dd/MM/yyyy HH:mm:ss")]
        public DateTime POSentDate { get; set; }
        [Cell(Header = "Trạng thái đơn hàng post")]
        public string Status { get; set; }
    }
}
