﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Validation;
    using DotNet.TabularModel;
    [Table("PurchaseItem")]
    public class PurchaseItem
    {
        [Cell(Order = 1, Header = "Mã SP")]
        public string ItemNo { get; set; }
        public string PoNo { get; set; }
        [Cell(Order = 2, Header = "Mã kho")]
        public string LocationCode { get; set; }
        [Cell(Order = 3, Header = "Tên SP")]
        public string Description { get; set; }
        public string UnitOfMeasure { get; set; }
        [Cell(Order = 5, Header = "Số lượng đặt")]
        public double Quantity { get; set; }
        public string LineNo { get; set; }
        [DataType(DataType.Date)]
        //[PurchaseItemReceiptDateValidation(ErrorMessage = "Ngày giao phải tối thiểu từ ngày hiện tại.")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]        
        public DateTime ExpectedReceiptDate { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thời gian giao")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH-mm}", ApplyFormatInEditMode = true)]
        public DateTime ExpectedReceiptTime { get; set; }
        [DynamicRange(0, "Quantity", ErrorMessage = "Số lượng nhập phải lớn hơn hoặc bằng {0} và nhỏ hơn hoặc bằng số lượng đặt.")]
        [Required(ErrorMessage = "Bạn cần nhập số lượng giao.")]
        [Cell(Order = 6, Header = "Số lượng giao")]
        public int QuantityToReceive { get; set; }
        [Column("LineAmount")]
        public decimal Amount { get; set; }
        //public decimal AmountVAT { get; set; }
        [Column("DirectUnitCost")]
        [Cell(Order = 4, Header = "Đơn giá")] //, NPOIFormatString = "#,### [$VND]")]
        public decimal UnitPrice { get; set; }
        public int PoStatus { get; set; }
        public int DateChanged { get; set; }
        public int QtyChanged { get; set; }
        public PurchaseOrder PO { get; set; }
    }
}
