﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Script.Serialization;
    using DotNet.TabularModel;
    [Table("PurchaseOrder")]
    public class PurchaseOrder
    {
        [Column("No_")]
        [Cell(Header = "Mã đơn hàng")]
        public string PoNo { get; set; }

        public string VendorNo { get; set; }
        [Column("VendorName")]
        public string VendorNameVn { get; set; }
        [ScriptIgnore]
        //[Cell(Header = "Ngày đặt hàng", NPOIFormatString = "dd/MM/yyyy")]
        [Cell(Header = "Ngày đặt hàng")] //, NPOIFormatString = "dd/MM/yyyy")]
        public DateTime OrderDate { get; set; }
        [Cell(Header = "Ngày giao hàng")] //, NPOIFormatString = "dd/MM/yyyy")]
        public DateTime ExpectedReceiptDate { get; set; }
        public string StoreAddress { get; set; }
        public string StorePhoneNo { get; set; }
        public string StoreName { get; set; }        
        public string StoreInfo
        {
            get
            {
                return $"{StoreName} <b> Địa chỉ </b>: {StoreAddress}, <b>số ĐT</b>: {(string.IsNullOrEmpty(StorePhoneNo) ? "(không có)" : StorePhoneNo)}";
            }
        }
        public string BuyerId { get; set; }//currently not used
        public int Status { get; set; }//indicates if the order was confirmed or not
        public int PoStatus { get; set; }
        public int Approved { get; set; }//indicates if the order was modified and 
        public string Desc { get; set; }//currently not used
        public string Note { get; set; }
        public int Sended { get; set; }//should be Sent
        public DateTime SendDate { get; set; }
        public int ViewStatus { get; set; }//1 means ReadOnly, 0 means Editable
        public bool IsEditable
        {
            get
            {
                return ViewStatus == 0;
            }
        }
        [Column("modifydate")]
        [ScriptIgnore]
        public DateTime LastApprovedDate { get; set; }
        public string LastApprovedDateString
        {
            get
            {
                return LastApprovedDate.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        public string LastApprovedValidJsDateString
        {
            get
            {
                return LastApprovedDate.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }
        public string OrderDateDisplay
        {
            get
            {
                return OrderDate.ToString("dd/MM/yyyy");
            }
        }
        [ScriptIgnore]
        [Cells(Header = "Sản phẩm trong đơn")]
        public List<PurchaseItem> PurchaseItems { get; set; } = new List<PurchaseItem>();
    }
}
