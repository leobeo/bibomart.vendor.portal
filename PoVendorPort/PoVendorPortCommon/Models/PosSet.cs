﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models
{
    public class PosSet
    {
        public PosSet(string id)
        {
            RequestId = id;
        }
        public string RequestId { get; private set; }
        IEnumerable<PurchaseOrder> _pos;
        public IEnumerable<PurchaseOrder> Pos
        {
            get
            {
                return _pos;
            }
            set
            {
                if (_pos != value)
                {
                    _pos = value;
                    if (value != null)
                    {
                        Pis = value.SelectMany(e => e.PurchaseItems ?? Enumerable.Empty<PurchaseItem>()).ToList();
                    }
                }
            }
        }
        public IEnumerable<PurchaseItem> Pis { get; set; }
        public int ItemsPerPage { get; set; }
        public int Page { get; set; } = -1;
        public int ReadCount { get; private set; }
        public bool ReachedEnd
        {
            get
            {
                return ReadCount == Pis.Count();
            }
        }
        public IEnumerable<PurchaseItem> NextPage
        {
            get
            {
                if (Pis == null || ReachedEnd) return null;
                var cp = ++Page;
                var skippedCount = ItemsPerPage * cp;
                var items = Pis.Skip(skippedCount).Take(ItemsPerPage).ToList();
                ReadCount = skippedCount + items.Count;                
                return items;
            }
        }
        public void RewindSet()
        {
            Page = -1;            
        }
    }
}
