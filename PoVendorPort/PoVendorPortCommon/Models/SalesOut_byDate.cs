﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Models
{
    public class SalesOut_byDate
    {
        public string itemNo { set; get; }
        public string itemName { set; get; }
        public string orderNo { set; get; }
        public decimal orderQuantity { set; get; }
        public decimal realQuantity { set; get; }
        public int status { set; get; }
        public string status_Content{set; get;}
        public string createdDate { set; get; }
        public string modifyDate { set; get; }
        public string storeNo { set; get; }
        public string storeName { set; get; }
    }
}
