﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon
{
    using Models;
    using System.IO;
    using DotNet.Document.ExcelGenerators;
    public static class Export
    {
        public static Stream ExportPurchaseOrdersToExcel(IEnumerable<PurchaseOrder> pos, string sheetname = null)
        {
            var exgen = new NPOIExcelGenerator();
            //exgen.GenerateExcel(pos, sheetname, true, p => null, true);
            exgen.GenerateExcel(pos, sheetname, true, true);           
            return exgen.SaveAsStream();
        }
        public static Stream ExportPosStatusReport(IEnumerable<PoStatusReportItem> items, string sheetname = null)
        {
            var exgen = new NPOIExcelGenerator();
            //exgen.GenerateExcel(items, sheetname, true, p => null, true);
            exgen.GenerateExcel(items, sheetname, true, true);
            return exgen.SaveAsStream();
        }
    }
}
