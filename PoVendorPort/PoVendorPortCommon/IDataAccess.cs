﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon
{
    using Models;
    using PoVendorPortCommon.Models.Picking;
    using PoVendorPortCommon.Models.Picking.CheckList;
    using PoVendorPortCommon.Models.Picking.Invoice;
    using PoVendorPortCommon.Models.Picking.Packing;
    using PoVendorPortCommon.Models.Picking.ReceiveAccept;
    using PoVendorPortCommon.Models.Picking.Sales_v2;
    using PoVendorPortCommon.Models.Picking.SalesOut_v2;

    public interface IDataAccess
    {
        Task<bool> CheckLogin(string username, string password);
        Task<IEnumerable<PurchaseOrder>> LoadPurchaseOrdersByVendorId(string vendorId);
        Task<Vendor> GetVendorInfoAsync(string username);
        Task<bool> UpdatePurchaseOrder(PurchaseOrder po);
        Task<bool> ChangePassword(string username, string password, string newPassword);

        Task<bool> MarkPoAsSent(string poNo);

        Task<bool> UpdatePoNote(PurchaseOrder po);
        Task<string> GetPoNote(string poNo);

        #region for user management
        Task<UserInfo> GetUserInfo(string username);
        Task<bool> UpdateUserInfo(UserInfo ui, string newPassword = null);
        Task<IEnumerable<string>> AddVendors(List<Tuple<string, string>> vendorCredentials, bool updatePassForExistingUsers = false);
        Task<IEnumerable<string>> AddDefaultVendors(List<string> vendorCodes, bool updatePassForExistingUsers = false, string passwordPattern = "{0}@123456");
        Task<bool> ResetVendorsPass(List<string> vendorNos, string defaultPattern = "{0}@123456");
        Task<IEnumerable<Vendor>> SearchVendorsManagedBy(string username, string vendorNo);
        Task<IEnumerable<UserInfo>> GetAllManageableUsers();
        Task<bool> CheckAndAutoCreateDefaultUsers();
        Vendor GetVendorInfo(string username);
        Vendor GetVendorInfoFromPoTable(string vendorNo);
        Task<int> GetVendorPoCount(string vendorNo);
        Task<IEnumerable<ManageGroup>> GetManageGroups();
        Task<bool> ApprovePo(string pono);
        Task<bool> IsValidManagerToken(string managerToken);
        Task<UserInfo> GetManagerInfo(string username, string password);
        Task<PosSet> GetPosWithFilter(string vendorNo = null, int confirmStatus = -1, int sentStatus = -1, int receivedStatus = -1, DateTime? fromOrderDate = null, DateTime? toOrderDate = null, DateTime? fromConfirmDate = null, DateTime? toConfirmDate = null, string locationCode = null);
        Task<IEnumerable<PurchaseItem>> GetMorePos(string requestId, int itemsPerPage = 50, int page = -1);
        Task<IEnumerable<PurchaseItem>> GetPosStatusReport(string requestId);
        Task<bool> CheckIfPoEditable(string poNo);
        #endregion

        #region Picking
        Task<List<userRole_Item>> sp_Picking_get_UserRole(string username);

        Task<List<vendor_Item>> sp_Picking_get_VendorPOCount(string username);

        Task<List<saleIn_Item>> sp_Picking_get_SaleIn(string userName, string vendorNo);

        Task<List<salesIn_Detail>> sp_Picking_get_SaleInDetail(string userName, string orderNo);

        Task<List<saleOut_Item>> sp_Picking_get_SaleOut(string userName);

        Task<List<salesOut_Detail>> sp_Picking_get_SaleOutDetail(string userName, string orderNo);

        Task<List<report_Item>> sp_Picking_get_ReportItem(string userName);

        Task<List<report_Detail>> sp_Picking_get_ReportDetail(string userName, string orderNo);

        Task<List<reportVendor_list>> sp_Picking_get_ReportVendor(string userName);

        Task<List<orderList>> sp_Picking_get_Invoice_lsOrder(string userName, string vendorNo);

        Task<List<vendorList>> sp_Picking_get_Invoice_lsVendor(string userName);

        Task<List<invoice_Item>> sp_Picking_get_Invoice_Item(string userName);

        Task<List<invoice_Detail>> sp_Picking_get_Invoice_ItemDetail(string userName, string orderNo);

        Task<List<CheckList_Item>> sp_Picking_get_Checklist(string userName);

        Task<List<Sub_CheckList_Item>> sp_Picking_get_ChecklistSub(string userName, string checkList_codecheck);

        Task<List<CheckList_Result_Item>> Picking_get_CheckListResult(string userName, string orderNo);

        Task<int> sp_Picking_get_total_NewOrderForStore(string userName);

        Task<List<packingLine_Item>> sp_Picking_get_Packing_Line(string userName, string storeNo, string dateFill);

        Task<List<SalesIn_byDate>> sp_Picking_get_SalesIn_byDate(string userName, string dateFill,string itemNo);

        Task<List<SalesOut_byDate>> sp_Picking_get_SalesOut_byDate(string userName, string dateFill, string itemNo, string typeFill);

        Task<List<SalesOut_Header>> sp_Picking_get_SaleOut_v2(string userName, string dateFill);

        Task<List<SalesOut_Line>> sp_Picking_get_SalesOut_Detail_v2(string userName, string dateFill, string storeNo);

        Task<List<TO_ItemTotal>> sp_get_TO_ItemTotal(string userName, string datekey, string typeFill, string dataFill,string Router);

        Task<List<SalesOut_Line>> sp_Picking_get_TO_Detail_Line(string userName, string dateFill, string typeFill, string dataFill, string Router);

        Task<List<RouterItem>> sp_Picking_get_RouterType();

        Task<List<SalesOut_Line>> sp_Picking_get_ItemForStore_least(string userName, string dateFill, string storeNo);

        Task<List<ItemWait>> sp_Picking_get_AcceptItemWait(string userName, string dateFill, string typeFill, string dataFill);

        Task<List<boxType_Item>> sp_Picking_get_BoxType(string userName);

        Task<List<volumeByItem>> sp_Picking_get_VolumeByItem(string userName, string dateFill, string storeNo);

        Task<List<RA_Item>> sp_Picking_get_ReceiveAcceptOrder(string userName, string dateFill);

        #region dataAction

        #region updateData
        Task<bool> sp_Picking_update_SalesIn(string userName, string orderNo, int status);

        Task<bool> sp_Picking_update_SalesOut(string userName, string orderNo, int status);

        Task<bool> sp_Picking_update_Report(string userName, string orderNo, int status);

        Task<bool> sp_Picking_update_InvoiceHeader(string userName, string invoiceNo, int status);

        #endregion

        #region addData

        Task<bool> sp_Picking_add_SalesIn(List<salesIn_Detail> it, string userName,int type);

        Task<bool> sp_Picking_add_SalesOut(List<salesOut_Detail> it, string userName,int type);

        Task<bool> sp_Picking_add_ReportLine(List<report_Detail> it, string userName, int type);

        Task<string> sp_Picking_add_InvoiceHeader(string userName);

        Task<bool> sp_Picking_add_InvoiceLine(List<invoice_Detail> it, string userName, int type);

        Task<bool> sp_Picking_add_CheckListResult(List<CheckList_Result_Item> its, string userName, string orderNo);

        Task<bool> sp_Picking_add_PackingLine(List<packingLine_Item> its, string userName);

        Task<bool> sp_Picking_add_TO_ItemTotal_Line(string userName, TO_ItemTotal it);

        Task<bool> sp_Picking_add_ReceiveAcceptOrder(string userName, RA_Item it);
        #endregion

        #endregion
        #endregion
    }
}
