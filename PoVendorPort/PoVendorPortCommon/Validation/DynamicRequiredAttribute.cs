﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Validation
{
    using System.ComponentModel.DataAnnotations;
    public class DynamicRequiredAttribute : RequiredAttribute
    {
        public DynamicRequiredAttribute(string boolProperty)
        {
            BoolProperty = boolProperty;
        }
        /// <summary>
        /// Gets the property whose value should be a boolean and its value can be used to enable/disable the RequiredAttribute.
        /// It depends on the IsInverseEnabling as well. By Default the RequiredAttribute is enabled when the bool property has value of true.
        /// </summary>
        public string BoolProperty { get; private set; }
        public bool IsInverseEnabling { get; set; }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var p = validationContext.ObjectType.GetProperty(BoolProperty);
            if(p == null || p.PropertyType != typeof(bool))
            {
                throw new InvalidOperationException("Invalid BoolProperty, it should be a public bool property of the current model class");
            }
            //if the dynamic condition means RequiredAttribute should be disabled, we just return null to indicate the validation is valid anyway.
            var pv = (bool)p.GetValue(validationContext.ObjectInstance);
            var shouldIgnore = pv ^ !IsInverseEnabling;
            if (shouldIgnore)
            {
                return null;
            }
            return base.IsValid(value, validationContext);
        }
    }
}
