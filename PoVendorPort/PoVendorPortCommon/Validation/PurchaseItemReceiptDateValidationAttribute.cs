﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoVendorPortCommon.Validation
{
    using Models;
    public class PurchaseItemReceiptDateValidationAttribute : DynamicRangeAttribute
    {
        public PurchaseItemReceiptDateValidationAttribute() : base(SpecialRangeValues.DateTime_Now, null)
        {

        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var pi = validationContext.ObjectInstance as PurchaseItem;
            //when QuantityToReceive is 0, we ignore the date validation
            if(pi != null && pi.QuantityToReceive == 0)
            {
                return null;
            }
            return base.IsValid(value, validationContext);
        }
    }
}
