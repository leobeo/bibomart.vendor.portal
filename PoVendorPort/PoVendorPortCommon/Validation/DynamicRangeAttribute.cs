﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPortCommon.Validation
{
    using System.Web.Mvc;
    using System.ComponentModel.DataAnnotations;
    [AttributeUsage(AttributeTargets.Property)]
    public class DynamicRangeAttribute : ValidationAttribute, IClientValidatable
    {
        public DynamicRangeAttribute(object min, object max)
        {
            if(object.Equals(min, SpecialRangeValues.DateTime_Now))
            {
                min = DateTime.Now;
            }
            if(object.Equals(max, SpecialRangeValues.DateTime_Now))
            {
                max = DateTime.Now;
            }
            var mi = min as IComparable;
            var ma = max as IComparable;
            if (mi == null && min != null || ma == null && max != null) throw new InvalidOperationException("min and max should be IComparable");
            if (mi != null && ma != null)
            {
                if (mi.CompareTo(Convert.ChangeType(ma, mi.GetType())) > 0) throw new InvalidOperationException("min cannot be greater than max");
            }
            MinValue = mi;
            MaxValue = ma;
        }
        public DynamicRangeAttribute(object min, string maxProp)
        {
            if (object.Equals(min, SpecialRangeValues.DateTime_Now))
            {
                min = DateTime.Now;
            }
            var mi = min as IComparable;
            if (mi == null) throw new InvalidOperationException("min should be IComparable");
            MinValue = mi;
            MaxValuePropertyName = maxProp;
        }
        public DynamicRangeAttribute(string minProp, object max)
        {
            if (object.Equals(max, SpecialRangeValues.DateTime_Now))
            {
                max = DateTime.Now;
            }
            var ma = max as IComparable;
            if (ma == null) throw new InvalidOperationException("max should be IComparable");
            MinValuePropertyName = minProp;
            MaxValue = ma;
        }
        public DynamicRangeAttribute(string minProp, string maxProp)
        {
            MinValuePropertyName = minProp;
            MaxValuePropertyName = maxProp;
        }
        public IComparable MinValue { get; set; }
        public IComparable MaxValue { get; set; }
        public string MinValuePropertyName { get; set; }
        public string MaxValuePropertyName { get; set; }
        
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {                        
            object d = null;
            var mm = _resolveDynamicMinMax(validationContext.ObjectInstance, validationContext.ObjectType);
            var t = mm.Item1 != null ? mm.Item1.GetType() : mm.Item2 != null ? mm.Item2.GetType() : null;
            var e1 = mm.Item1 == null;
            var e2 = mm.Item2 == null;
            if (value != null)
            {
                if (!e1)
                {
                    d = Convert.ChangeType(value, mm.Item1.GetType());
                    e1 = mm.Item1.CompareTo(d) <= 0;
                }
                if (!e2)
                {
                    d = Convert.ChangeType(value, mm.Item2.GetType());
                    e2 = mm.Item2.CompareTo(d) >= 0;
                }                
            }
            if (e1 && e2) return null;
            return new ValidationResult(string.Format(string.Format(ErrorMessage ?? "Value should be in range of [{0},{1}]", mm.Item1, mm.Item2)));
        }
        Tuple<IComparable,IComparable> _resolveDynamicMinMax(object model, Type modelType)
        {
            var minValue = MinValue;
            var maxValue = MaxValue;
            var minProp = modelType.GetProperty(MinValuePropertyName ?? "");            
            if (minProp != null)
            {
                minValue = minProp.GetValue(model) as IComparable;
            }
            var maxProp = modelType.GetProperty(MaxValuePropertyName ?? "");
            if (maxProp != null)
            {
                maxValue = maxProp.GetValue(model) as IComparable;
            }
            return new Tuple<IComparable, IComparable>(minValue, maxValue);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            var mm = _resolveDynamicMinMax(metadata.Container, metadata.ContainerType);
            if (mm.Item1 != null)
            {
                rule.ValidationParameters["minvalue"] = mm.Item1;
            }
            if (mm.Item2 != null)
            {
                rule.ValidationParameters["maxvalue"] = mm.Item2;
            }
            
            rule.ValidationType = "range";
            yield return rule;
        }
    }
    public enum SpecialRangeValues
    {
        DateTime_Now
    }
}