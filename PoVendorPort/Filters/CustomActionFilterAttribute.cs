﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Microsoft.AspNet.Identity;
using System.Reflection;

namespace PoVendorPort.Filters
{
    using PoVendorPortCommon;
    public class CustomActionFilterAttribute : ActionFilterAttribute
    {
        public override async void OnActionExecuting(ActionExecutingContext filterContext)
        {            
            var da = IoC.UnityContainerConfig.Uc.Resolve<IDataAccess>("datacache");
            var un = filterContext.HttpContext.User.Identity.GetUserId();
            //this almost happens when logging out.
            if (un == null)
            {
                base.OnActionExecuting(filterContext);
                return;
            }
            var ui = await da.GetUserInfo(un);
            var isVendor = !(ui.IsAdmin || ui.IsManager);
            if (isVendor)
            {
                var requireManagerRight = filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(ManagerRightRequiredAttribute), true).Any();
                if (requireManagerRight)
                {
                    filterContext.Result = new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }
            } else //should be a manager
            {
                var requireAdminRight = filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(AdminRightRequiredAttribute), true).Any();
                if (!requireAdminRight)
                {
                    requireAdminRight = filterContext.ActionDescriptor.GetCustomAttributes(typeof(AdminRightRequiredAttribute), true).Any();
                }
                if(!ui.IsAdmin && requireAdminRight)
                {
                    filterContext.Result = new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }
            }
            filterContext.Controller.ViewData["IsVendor"] = isVendor;
            base.OnActionExecuting(filterContext);
        }
    }
}