﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ManagerRightRequiredAttribute : Attribute
    {
    }
}