﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoVendorPort.Filters
{
    using System.Web.Mvc;
    using PoVendorPortCommon;
    /// <summary>
    /// The benefit of using this filter over Unity Interception is we can capture all unhandled Exceptions.
    /// The Unity Interception can do that of course but requires you to scan all possible calls to catch the Exceptions.
    /// The benefit of using Unity Interception for logging error is it can provide more info on the method where the exception is raised.
    /// We can use both of these to help logging the Exceptions better.
    /// </summary>
    public class CustomHandleErrorAttribute : HandleErrorAttribute
    {
        public CustomHandleErrorAttribute(ILogger logger)
        {
            _logger = logger;
        }
        ILogger _logger;
        public override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                _logger.Write(string.Format("{0} - Exception: {1}", DateTime.Now, filterContext.Exception));
            }
            base.OnException(filterContext);
        }
    }
}