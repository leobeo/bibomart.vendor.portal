﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PoVendorPort.Startup))]
namespace PoVendorPort
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
