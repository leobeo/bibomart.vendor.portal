﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace PoVendorPortDAL
{
    using PoVendorPortCommon;
    using Dapper;
    using System.Data.SqlClient;
    using sc = System.ComponentModel.DataAnnotations.Schema;
    using System.Reflection;
    using PoVendorPortCommon.Models;
    using System.Web;
    using PoVendorPortCommon.Models.Picking;
    using PoVendorPortCommon.Models.Picking.Invoice;
    using PoVendorPortCommon.Models.Picking.CheckList;
    using PoVendorPortCommon.Models.Picking.Packing;
    using PoVendorPortCommon.Models.Picking.Sales_v2;
    using PoVendorPortCommon.Models.Picking.SalesOut_v2;
    using PoVendorPortCommon.Models.Picking.ReceiveAccept;

    public class DataAccess : IDataAccess
    {
        static DataAccess()
        {
            var modelTypes = typeof(PurchaseOrder).Assembly.DefinedTypes.Where(t => t.Namespace == "PoVendorPortCommon.Models" && t.IsDefined(typeof(sc.TableAttribute)));

            foreach (var type in modelTypes)
            {
                SqlMapper.SetTypeMap(type, new CustomPropertyTypeMap(type, (t, col) =>
                {
                    return t.GetProperties().FirstOrDefault(p => p.GetCustomAttributes<sc.ColumnAttribute>().Any(e => e.Name.ToLower() == col.ToLower()) || p.Name.ToLower() == col.ToLower());
                }));
            }
        }
        public DataAccess(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public string ConnectionString { get; private set; }
        public async Task<IEnumerable<string>> AddDefaultVendors(List<string> vendorCodes, bool updatePassForExistingUsers = false, string passwordPattern = "{0}@123456")
        {
            if (passwordPattern == null) passwordPattern = "{0}@123456";
            return await AddVendors(vendorCodes.Select(e => new Tuple<string, string>(e, string.Format(passwordPattern, e))).ToList(), updatePassForExistingUsers);
        }
        public async Task<IEnumerable<string>> AddVendors(List<Tuple<string, string>> vendorCredentials, bool updatePassForExistingUsers = false)
        {
            var dt = new DataTable();
            dt.Columns.Add("username");
            dt.Columns.Add("password");
            if (!vendorCredentials.Any()) return Enumerable.Empty<string>();

            dt = vendorCredentials.Select(e =>
            {
                var dr = dt.NewRow();
                dr[0] = e.Item1;
                dr[1] = Common.modXuLyChuoiKyTu.EncodeString(e.Item2);
                return dr;
            }).CopyToDataTable();

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (var t = con.BeginTransaction())
                {
                    try
                    {
                        var existingCodes = con.Query<string>("sp_createDefaultUsers", new { credentials = dt, updatePassForExistingUsers = updatePassForExistingUsers }, t, commandType: CommandType.StoredProcedure);
                        t.Commit();
                        return existingCodes.ToList();
                    }
                    catch
                    {
                        t.Rollback();
                        throw;
                    }
                }
            }
        }

        public async Task<bool> ApprovePo(string pono)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("sp_approvePo", new { pono = pono }, commandType: CommandType.StoredProcedure);
                return true;
            }
        }

        public async Task<bool> ChangePassword(string username, string password, string newPassword)
        {
            password = Common.modXuLyChuoiKyTu.EncodeString(password);
            newPassword = Common.modXuLyChuoiKyTu.EncodeString(newPassword);
            using (var con = new SqlConnection(ConnectionString))
            {
                var i = con.Execute("sp_changePassword", new { username = username, password = password, newPassword = newPassword }, commandType: System.Data.CommandType.StoredProcedure);
                return i > 0;
            }
        }

        public async Task<bool> CheckAndAutoCreateDefaultUsers()
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var nonExistingUsers = con.Query<string>("sp_getNonExistingUsers", commandType: CommandType.StoredProcedure);
                if (!nonExistingUsers.Any()) return true;
                await this.AddVendors(nonExistingUsers.Select(e => new Tuple<string, string>(e, $"{e}@123456")).ToList());
                return true;
            }
        }

        public async Task<bool> CheckIfPoEditable(string poNo)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var i = con.ExecuteScalar<int>("sp_checkIfPoEditable", new { poNo = poNo }, commandType: CommandType.StoredProcedure);
                return i > 0;
            }
        }

        public async Task<bool> CheckLogin(string username, string password)
        {
            password = Common.modXuLyChuoiKyTu.EncodeString(password);
            using (var con = new SqlConnection(ConnectionString))
            {
                var c = con.ExecuteScalar<int>("select count(1) from TSY_USER where username=@u and password=@p", new { u = username, p = password });
                if (c > 0)
                {
                    con.Execute("sp_updateUserLastLoginTime", new { username = username }, commandType: System.Data.CommandType.StoredProcedure);
                }
                return c > 0;
            }
        }

        public async Task<IEnumerable<UserInfo>> GetAllManageableUsers()
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var items = con.Query<UserInfo>("sp_getAllManagers", commandType: CommandType.StoredProcedure);
                return items.ToList();
            }
        }

        public async Task<IEnumerable<ManageGroup>> GetManageGroups()
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var groups = con.Query<ManageGroup>("sp_getManageGroups", commandType: CommandType.StoredProcedure);
                return groups.ToList();
            }
        }

        public async Task<UserInfo> GetManagerInfo(string username, string password)
        {
            var loginOk = await CheckLogin(username, password);
            if (loginOk)
            {
                var ui = await GetUserInfo(username);
                if (ui != null && (ui.IsManager || ui.IsAdmin))
                {
                    return ui;
                }
            }
            return null;
        }

        public async Task<IEnumerable<PurchaseItem>> GetMorePos(string requestId, int itemsPerPage = 50, int page = -1)
        {
            if (HttpContext.Current == null) return Enumerable.Empty<PurchaseItem>();
            var cache = HttpContext.Current.Cache;
            var ps = cache[requestId] as PosSet;
            if (ps == null)
            {
                return Enumerable.Empty<PurchaseItem>();
            }
            ps.ItemsPerPage = itemsPerPage;
            if (page != -1)
            {
                ps.Page = page;
            }
            return ps.NextPage;
        }

        public async Task<string> GetPoNote(string poNo)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                return con.ExecuteScalar<string>("sp_getPoNote", new { poNo = poNo }, commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<PurchaseItem>> GetPosStatusReport(string requestId)
        {
            if (HttpContext.Current == null) return null;
            var cache = HttpContext.Current.Cache;
            var ps = cache[requestId] as PosSet;
            if (ps == null) return null;
            return ps.Pis;
        }

        public async Task<PosSet> GetPosWithFilter(string vendorNo = null, int confirmStatus = -1, int sentStatus = -1, int receivedStatus = -1, DateTime? fromOrderDate = null, DateTime? toOrderDate = null, DateTime? fromConfirmDate = null, DateTime? toConfirmDate = null, string locationCode = null)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var ps = new PosSet(Guid.NewGuid().ToString("N"));
                var items = con.Query<PurchaseOrder, PurchaseItem, PurchaseOrder>("sp_getPosWithFilter2", (po, pi) =>
                {
                    pi.ExpectedReceiptTime = pi.ExpectedReceiptDate;
                    po.PurchaseItems.Add(pi);
                    pi.PO = po;
                    pi.PoNo = po.PoNo;
                    return po;
                },
                    new
                    {
                        vendorNo = vendorNo,
                        confirmStatus = confirmStatus,
                        sentStatus = sentStatus,
                        receivedStatus = receivedStatus,
                        fromOrderDate = fromOrderDate,
                        toOrderDate = toOrderDate,
                        fromConfirmDate = fromConfirmDate,
                        toConfirmDate = toConfirmDate,
                        locationCode = locationCode
                    }, splitOn: "LocationCode", commandType: CommandType.StoredProcedure).ToList();
                var groups = items.GroupBy(e => e.PoNo).Select(g =>
                {
                    var first = g.First();
                    var pis = g.SelectMany(e => e.PurchaseItems).ToList();
                    first.PurchaseItems = pis;
                    return first;
                });
                ps.Pos = groups.ToList();
                ps.ItemsPerPage = 50;
                return ps;
            }
        }

        public async Task<UserInfo> GetUserInfo(string username)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var ui = con.Query<UserInfo>("sp_getUserInfo", new { username = username }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                return ui;
            }
        }

        public Vendor GetVendorInfo(string username)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var vi = con.Query<Vendor>("sp_getVendorInfo", new { username = username }, commandType: System.Data.CommandType.StoredProcedure).SingleOrDefault();
                if (vi != null)
                {
                    vi.UserName = username;
                    vi.State = VendorState.PoUser;
                }
                else
                {
                    //create a default instance, this vendor has login account but does not have any PO
                    vi = new Vendor();
                    vi.State = VendorState.NoPoUser;
                    vi.UserName = username; //NOTE: don't set No, let it be null.
                }
                return vi;
            }
        }

        public async Task<Vendor> GetVendorInfoAsync(string username)
        {
            return GetVendorInfo(username);
        }

        public Vendor GetVendorInfoFromPoTable(string vendorNo)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var vi = con.Query<Vendor>("sp_getVendorInfoFromPoTable", new { vendorNo = vendorNo }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return vi;
            }
        }

        public async Task<int> GetVendorPoCount(string vendorNo)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var c = con.ExecuteScalar<int>("sp_getVendorPoCount", new { vendorNo = vendorNo }, commandType: CommandType.StoredProcedure);
                return c;
            }
        }

        public async Task<bool> IsValidManagerToken(string managerToken)
        {
            if (HttpContext.Current == null) return false;
            var cache = HttpContext.Current.Cache;
            return cache[managerToken] != null;
        }

        public async Task<IEnumerable<PurchaseOrder>> LoadPurchaseOrdersByVendorId(string vendorId)
        {

            using (var con = new SqlConnection(ConnectionString))
            {
                var items = con.Query<PurchaseOrder, PurchaseItem, PurchaseOrder>("sp_getPurchaseOrdersByVendorNo", (po, pi) =>
                {
                    pi.ExpectedReceiptTime = pi.ExpectedReceiptDate;
                    po.PurchaseItems.Add(pi);
                    pi.PO = po;
                    pi.PoNo = po.PoNo;
                    return po;
                }, new { vendorNo = vendorId }, splitOn: "LocationCode", commandType: System.Data.CommandType.StoredProcedure);
                var groups = items.GroupBy(e => e.PoNo).Select(g =>
                {
                    var first = g.First();
                    var pis = g.SelectMany(e => e.PurchaseItems).ToList();
                    first.PurchaseItems = pis;
                    return first;
                });
                return groups.ToList();
            }
            /*
            return Enumerable.Range(0, 50).Select((e, i) => new PurchaseOrder() {
                Desc = string.Format("PurchaseOrder {0}", i),
                OrderDate = DateTime.Now,
                ExpectedReceiptDate = DateTime.Now.AddDays(5),
                PoNo = i.ToString(),
                VendorNo = "Vendor " + i,
                VendorNameVn = "Vendor " + i,
                PurchaseItems = Enumerable.Range(0, 30).Select((o,k) => new PurchaseItem() {
                     Description = "PurchaseItem " + k,
                     PoNo = i.ToString(),
                     LocationCode = "Location " + i,
                     ItemNo = i + "_" + k,
                    ExpectedReceiptDate = DateTime.Now.AddDays(5),
                    Quantity = 15,
                     QuantityToReceive = 10
                }).ToList() 
            }); */
        }

        public async Task<bool> MarkPoAsSent(string poNo)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("sp_markPOAsSent", new { poNo = poNo }, commandType: System.Data.CommandType.StoredProcedure);
                return true;
            }
        }

        public async Task<bool> ResetVendorsPass(List<string> vendorNos, string defaultPattern = "{0}@123456")
        {
            if (string.IsNullOrEmpty(defaultPattern) || defaultPattern.Length < 6) throw new InvalidOperationException("Password cannot be null, empty or have length less than 6");
            var dt = new DataTable();
            dt.Columns.Add("username");
            dt.Columns.Add("password");
            dt = vendorNos.Select(e =>
            {
                var dr = dt.NewRow();
                dr[0] = e;
                dr[1] = Common.modXuLyChuoiKyTu.EncodeString(string.Format(defaultPattern, e));
                return dr;
            }).CopyToDataTable();
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (var t = con.BeginTransaction())
                {
                    try
                    {
                        con.Execute("sp_resetPasswordsToDefault", new { credentials = dt }, t, commandType: CommandType.StoredProcedure);
                        t.Commit();
                        return true;
                    }
                    catch
                    {
                        t.Rollback();
                        throw;
                    }
                }
            }
        }

        public async Task<IEnumerable<Vendor>> SearchVendorsManagedBy(string username, string vendorNo)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                return con.Query<Vendor>("sp_searchVendorsManagedBy", new { username = username, vendorNo = vendorNo }, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<bool> UpdatePoNote(PurchaseOrder po)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("sp_updatePoNote", new { poNo = po.PoNo, note = po.Note }, commandType: System.Data.CommandType.StoredProcedure);
                return true;
            }
        }

        public async Task<bool> UpdatePurchaseOrder(PurchaseOrder po)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (var t = con.BeginTransaction())
                {
                    try
                    {
                        con.Execute("sp_UpdatePurchaseOrder", po.PurchaseItems.Select(e =>
                                                new
                                                {
                                                    VendorID = po.VendorNo,
                                                    DocumentNo = po.PoNo,
                                                    LocationCode = e.LocationCode,
                                                    ItemNo = e.ItemNo,
                                                    LineNo = e.LineNo,
                                                    ExpectedReceiptDate = e.ExpectedReceiptDate.Date.Add(e.ExpectedReceiptTime.TimeOfDay),
                                                    QtytoReceive = e.QuantityToReceive
                                                }), t, commandType: System.Data.CommandType.StoredProcedure);
                        //update note using separate stored proc
                        con.Execute("sp_updatePoNote", new { poNo = po.PoNo, note = po.Note }, t, commandType: System.Data.CommandType.StoredProcedure);
                        t.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        t.Rollback();
                        throw;
                    }
                }
            }
        }

        public async Task<bool> UpdateUserInfo(UserInfo ui, string newPassword = null)
        {
            if (newPassword != null)
            {
                newPassword = Common.modXuLyChuoiKyTu.EncodeString(newPassword);
            }
            var dt = new DataTable();
            dt.Columns.Add("StringItem");
            if (ui.ManagedVendors != null)
            {
                var nonExistingVendorCodes = new List<string>();
                foreach (var v in ui.ManagedVendors)
                {
                    if (v.State != VendorState.PoUser)
                    {
                        //create default user for the vendor
                        nonExistingVendorCodes.Add(v.No);
                    }
                    var dr = dt.NewRow();
                    dr[0] = v.No;
                    dt.Rows.Add(dr);
                }
                if (nonExistingVendorCodes.Any())
                {
                    await AddDefaultVendors(nonExistingVendorCodes);
                }
            }
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (var t = con.BeginTransaction())
                {
                    try
                    {
                        con.Execute("sp_updateUser", new { username = ui.UserName, fullname = ui.FullName, password = newPassword, mapb = ui.MaPB, vendorCodes = dt }, t,
                                    commandType: CommandType.StoredProcedure);
                        t.Commit();
                        return true;
                    }
                    catch
                    {
                        t.Rollback();
                        throw;
                    }
                }
            }
        }

        #region Picking

        public async Task<List<userRole_Item>> sp_Picking_get_UserRole(string username)
        {
            List<userRole_Item> its_s = new List<userRole_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_UserRole", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", username));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        userRole_Item it = new userRole_Item
                        {
                            userName = reader["userName"].ToString(),
                            rolesCode = reader["rolesCode"].ToString(),
                            controlNo = reader["controlNo"].ToString(),
                            controlName = reader["controlName"].ToString(),
                            moduleName = reader["moduleName"].ToString(),
                            moduleController = reader["moduleController"].ToString(),
                            moduleAction = reader["moduleAction"].ToString(),
                            createAction = int.Parse(reader["createAction"].ToString()),
                            editAction = int.Parse(reader["editAction"].ToString()),
                            acceptAction = int.Parse(reader["acceptAction"].ToString()),
                            typeGroup = int.Parse(reader["typeGroup"].ToString()),
                            level_m = int.Parse(reader["level_m"].ToString()),
                            webView = int.Parse(reader["webView"].ToString())

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<vendor_Item>> sp_Picking_get_VendorPOCount(string username)
        {
            List<vendor_Item> its_s = new List<vendor_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_VendorPOCount", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", username));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        vendor_Item it = new vendor_Item
                        {
                            vendorNo = reader["vendorNo"].ToString(),
                            vendorName = reader["vendorName"].ToString(),
                            quantityOrder = int.Parse(reader["quantityOrder"].ToString())

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<saleIn_Item>> sp_Picking_get_SaleIn(string userName, string vendorNo)
        {
            List<saleIn_Item> its_s = new List<saleIn_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_SaleIn", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("vendorNo", vendorNo));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        saleIn_Item it = new saleIn_Item
                        {
                            vendorNo = reader["vendorNo"].ToString(),
                            vendorName = reader["vendorName"].ToString(),
                            orderNo = reader["orderNo"].ToString(),
                            status_code = int.Parse(reader["status_code"].ToString()),
                            status_Content = reader["status_Content"].ToString(),
                            status_color = reader["status_color"].ToString(),
                            createdDate = reader["createdDate"].ToString(),
                            quantityItem = int.Parse(reader["quantityItem"].ToString()),
                            is_checkList = int.Parse(reader["is_checkList"].ToString()),
                            totalPack = int.Parse(reader["totalPack"].ToString())

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<salesIn_Detail>> sp_Picking_get_SaleInDetail(string userName, string orderNo)
        {
            List<salesIn_Detail> its_s = new List<salesIn_Detail>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_SaleInDetail", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("orderNo", orderNo));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        salesIn_Detail it = new salesIn_Detail
                        {
                            orderNo = reader["orderNo"].ToString(),
                            storeNo = reader["storeNo"].ToString(),
                            itemNo = reader["itemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            orderQuantity = int.Parse(reader["orderQuantity"].ToString()),
                            realQuantity = int.Parse(reader["realQuantity"].ToString()),
                            corePrice = decimal.Parse(reader["corePrice"].ToString()),
                            createdDate = reader["createdDate"].ToString(),
                            modifyDate = reader["modifyDate"].ToString(),
                            status = int.Parse(reader["status"].ToString())

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<saleOut_Item>> sp_Picking_get_SaleOut(string userName)
        {
            List<saleOut_Item> its_s = new List<saleOut_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_SaleOut", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        saleOut_Item it = new saleOut_Item
                        {
                            storeNo = reader["storeNo"].ToString(),
                            storeName = reader["storeName"].ToString(),
                            orderNo = reader["orderNo"].ToString(),
                            orderType = reader["orderType"].ToString(),
                            itemQuantity = int.Parse(reader["itemQuantity"].ToString()),
                            status_code = int.Parse(reader["status_code"].ToString()),
                            status_Content = reader["status_Content"].ToString(),
                            createdDate = reader["createdDate"].ToString(),
                            modifyDate = reader["modifyDate"].ToString(),
                            packTotal = int.Parse(reader["packTotal"].ToString()),
                            is_Packing = int.Parse(reader["is_Packing"].ToString())
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<salesOut_Detail>> sp_Picking_get_SaleOutDetail(string userName, string orderNo)
        {
            List<salesOut_Detail> its_s = new List<salesOut_Detail>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_SaleOutDetail", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("orderNo", orderNo));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        salesOut_Detail it = new salesOut_Detail
                        {
                            orderNo = reader["orderNo"].ToString(),
                            storeNo = reader["storeNo"].ToString(),
                            itemNo = reader["itemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            corePrice = decimal.Parse(reader["corePrice"].ToString()),
                            orderQuantity = int.Parse(reader["orderQuantity"].ToString()),
                            realQuantity = int.Parse(reader["realQuantity"].ToString()),
                            status = int.Parse(reader["status"].ToString()),
                            createdDate = reader["createdDate"].ToString(),
                            modifyDate = reader["modifyDate"].ToString()

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<report_Item>> sp_Picking_get_ReportItem(string userName)
        {
            List<report_Item> its_s = new List<report_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_ReportItem", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        report_Item it = new report_Item
                        {
                            vendorNo = reader["vendorNo"].ToString(),
                            vendorName = reader["vendorName"].ToString(),
                            orderNo = reader["orderNo"].ToString(),
                            status_code = int.Parse(reader["status_code"].ToString()),
                            status_content = reader["status_content"].ToString(),
                            createdDate = reader["createdDate"].ToString(),
                            lastModify = reader["lastModify"].ToString(),
                            lastModifyBy = reader["lastModifyBy"].ToString()
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<report_Detail>> sp_Picking_get_ReportDetail(string userName, string orderNo)
        {
            List<report_Detail> its_s = new List<report_Detail>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_ReportDetail", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("orderNo", orderNo));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        report_Detail it = new report_Detail
                        {
                            orderNo = reader["orderNo"].ToString(),
                            itemNo = reader["itemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            locationCode = reader["locationCode"].ToString(),
                            corePrice = decimal.Parse(reader["corePrice"].ToString()),
                            discountPercent = decimal.Parse(reader["discountPercent"].ToString()),
                            orderQuantity = int.Parse(reader["orderQuantity"].ToString()),
                            realQuantity = int.Parse(reader["realQuantity"].ToString()),
                            createdDate = reader["createdDate"].ToString(),
                            lastModify = reader["lastModify"].ToString(),
                            status = int.Parse(reader["status"].ToString())
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<reportVendor_list>> sp_Picking_get_ReportVendor(string userName)
        {
            List<reportVendor_list> its_s = new List<reportVendor_list>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_ReportVendor", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        reportVendor_list it = new reportVendor_list
                        {
                            vendorNo = reader["vendorNo"].ToString(),
                            vendorName = reader["vendorName"].ToString(),
                            order_total = int.Parse(reader["order_total"].ToString())
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<orderList>> sp_Picking_get_Invoice_lsOrder(string userName, string vendorNo)
        {
            List<orderList> its_s = new List<orderList>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_Invoice_lsOrder", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("vendorNo", vendorNo));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        orderList it = new orderList
                        {
                            vendorNo = reader["vendorNo"].ToString(),
                            vendorName = reader["vendorName"].ToString(),
                            orderNo = reader["orderNo"].ToString(),
                            item_total = int.Parse(reader["item_total"].ToString()),
                            amount_total = decimal.Parse(reader["amount_total"].ToString()),
                            createdDAte = reader["createdDate"].ToString(),
                            lastModifyDate = reader["lastModifyDate"].ToString()
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<vendorList>> sp_Picking_get_Invoice_lsVendor(string userName)
        {
            List<vendorList> its_s = new List<vendorList>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_Invoice_lsVendor", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        vendorList it = new vendorList
                        {
                            vendorNo = reader["vendorNo"].ToString(),
                            vendorName = reader["vendorName"].ToString(),
                            order_total = int.Parse(reader["order_total"].ToString())
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<invoice_Item>> sp_Picking_get_Invoice_Item(string userName)
        {
            List<invoice_Item> its_s = new List<invoice_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_Invoice_Item", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        invoice_Item it = new invoice_Item
                        {
                            invoiceNo = reader["invoiceNo"].ToString(),
                            vendorNo = reader["vendorNo"].ToString(),
                            vendorName = reader["vendorName"].ToString(),
                            order_total = int.Parse(reader["order_total"].ToString()),
                            item_total = int.Parse(reader["item_total"].ToString()),
                            amount_total = decimal.Parse(reader["amount_total"].ToString()),
                            createdDate = reader["createdDate"].ToString(),
                            status = int.Parse(reader["status"].ToString()),
                            status_content = reader["status_content"].ToString()
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<invoice_Detail>> sp_Picking_get_Invoice_ItemDetail(string userName, string orderNo)
        {
            List<invoice_Detail> its_s = new List<invoice_Detail>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_Invoice_ItemDetail", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("orderNo", orderNo));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        invoice_Detail it = new invoice_Detail
                        {
                            invoiceNo = reader["invoiceNo"].ToString(),
                            vendorNo = reader["vendorNo"].ToString(),
                            orderNo = reader["orderNo"].ToString(),
                            itemNo = reader["itemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            item_total = int.Parse(reader["item_total"].ToString()),
                            corePrice = decimal.Parse(reader["corePrice"].ToString()),
                            discount = decimal.Parse(reader["discount"].ToString()),
                            amount_total = decimal.Parse(reader["amount_total"].ToString()),
                            createdDate = reader["createdDate"].ToString(),
                            status = int.Parse(reader["status"].ToString())
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<CheckList_Item>> sp_Picking_get_Checklist(string userName)
        {
            List<CheckList_Item> its_s = new List<CheckList_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_Checklist", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        CheckList_Item it = new CheckList_Item
                        {
                            code = reader["code"].ToString(),
                            codeCheck = reader["codeCheck"].ToString(),
                            nameCheck = reader["nameCheck"].ToString(),
                            inputType = int.Parse(reader["inputType"].ToString()),
                            subcheck = int.Parse(reader["subcheck"].ToString()),
                            groupCode = int.Parse(reader["groupCode"].ToString()),
                            status = int.Parse(reader["status"].ToString()),
                            required = int.Parse(reader["required"].ToString())
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<Sub_CheckList_Item>> sp_Picking_get_ChecklistSub(string userName, string checkList_codecheck)
        {
            List<Sub_CheckList_Item> its_s = new List<Sub_CheckList_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_ChecklistSub", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("checkList_codecheck", checkList_codecheck));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Sub_CheckList_Item it = new Sub_CheckList_Item
                        {
                            checkList_codecheck = reader["checkList_codecheck"].ToString(),
                            title = reader["title"].ToString(),
                            code = reader["code"].ToString(),
                            name = reader["name"].ToString(),
                            status = int.Parse(reader["status"].ToString()),
                            inputType = int.Parse(reader["inputType"].ToString())

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<CheckList_Result_Item>> Picking_get_CheckListResult(string userName, string orderNo)
        {
            List<CheckList_Result_Item> its_s = new List<CheckList_Result_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_CheckListResult", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("orderNo", orderNo));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        CheckList_Result_Item it = new CheckList_Result_Item
                        {
                            orderNo = reader["orderNo"].ToString(),
                            codeCheck = reader["codeCheck"].ToString(),
                            is_codeCheck = int.Parse(reader["is_codeCheck"].ToString()),
                            nameCheck = reader["nameCheck"].ToString(),
                            inputType = int.Parse(reader["inputType"].ToString()),
                            subCheck = reader["subCheck"].ToString(),
                            is_subCheck = int.Parse(reader["is_subCheck"].ToString()),
                            sub_nameCheck = reader["sub_nameCheck"].ToString(),
                            descriptions = reader["descriptions"].ToString()

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<int> sp_Picking_get_total_NewOrderForStore(string userName)
        {
            int rt_i = 0;

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_total_NewOrderForStore", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        rt_i = int.Parse(reader["total"].ToString());
                    }
                    con.Close();

                    return rt_i;
                }
            }
            catch (Exception ex)
            {
                return rt_i;
            }
        }

        public async Task<List<packingLine_Item>> sp_Picking_get_Packing_Line(string userName, string storeNo, string dateFill)
        {
            List<packingLine_Item> its_s = new List<packingLine_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_Packing_Line_v2", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("storeNo", storeNo));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        packingLine_Item it = new packingLine_Item
                        {
                            orderNo = reader["orderNo"].ToString(),
                            boxCode = reader["boxCode"].ToString(),
                            boxName = reader["boxName"].ToString(),
                            packTotal = int.Parse(reader["packTotal"].ToString()),
                            coreVolume = decimal.Parse(reader["coreVolume"].ToString()),
                            volume = decimal.Parse(reader["volume"].ToString()),
                            createdBy = reader["createdBy"].ToString(),
                            createdDate = reader["createdDate"].ToString()


                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<SalesIn_byDate>> sp_Picking_get_SalesIn_byDate(string userName, string dateFill, string itemNo)
        {
            List<SalesIn_byDate> its_s = new List<SalesIn_byDate>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_SalesIn_byDate", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        SalesIn_byDate it = new SalesIn_byDate
                        {
                            orderNo = reader["orderNo"].ToString(),
                            ItemNo = reader["ItemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            status_code = int.Parse(reader["status_code"].ToString()),
                            status_Content = reader["status_Content"].ToString(),
                            status_color = reader["status_color"].ToString(),
                            createdDate = reader["createdDate"].ToString(),
                            quantity = decimal.Parse(reader["quantity"].ToString()),
                            realQuantity = decimal.Parse(reader["realQuantity"].ToString()),
                            totalPack = int.Parse(reader["totalPack"].ToString())

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<SalesOut_byDate>> sp_Picking_get_SalesOut_byDate(string userName, string dateFill, string itemNo, string typeFill)
        {
            List<SalesOut_byDate> its_s = new List<SalesOut_byDate>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_SalesOut_byDate", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));
                    cmd.Parameters.Add(new SqlParameter("itemNo", itemNo));
                    cmd.Parameters.Add(new SqlParameter("typeFill", typeFill));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        SalesOut_byDate it = new SalesOut_byDate
                        {
                            itemNo = reader["itemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            orderNo = reader["orderNo"].ToString(),
                            orderQuantity = decimal.Parse(reader["orderQuantity"].ToString()),
                            realQuantity = decimal.Parse(reader["realQuantity"].ToString()),
                            status = int.Parse(reader["status"].ToString()),
                            status_Content = reader["status_Content"].ToString(),
                            createdDate = reader["createdDate"].ToString(),
                            modifyDate = reader["modifyDate"].ToString(),
                            storeNo = reader["storeNo"].ToString(),
                            storeName = reader["storeName"].ToString()

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<SalesOut_Header>> sp_Picking_get_SaleOut_v2(string userName, string dateFill)
        {
            List<SalesOut_Header> its_s = new List<SalesOut_Header>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_SaleOut_v2", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        SalesOut_Header it = new SalesOut_Header
                        {
                            storeNo = reader["storeNo"].ToString(),
                            storeName = reader["storeName"].ToString(),
                            createdDate = reader["createdDate"].ToString(),
                            is_Packing=int.Parse(reader["is_Packing"].ToString()),
                            total_Packing=int.Parse(reader["total_Packing"].ToString()),
                            total_orderNo = int.Parse(reader["total_orderNo"].ToString()),
                            itemQuantity = int.Parse(reader["itemQuantity"].ToString()),
                            totalQuantity = int.Parse(reader["totalQuantity"].ToString())

                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<SalesOut_Line>> sp_Picking_get_SalesOut_Detail_v2(string userName, string dateFill, string storeNo)
        {
            List<SalesOut_Line> its_s = new List<SalesOut_Line>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_SalesOut_Detail_v2", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));
                    cmd.Parameters.Add(new SqlParameter("storeNo", storeNo));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        SalesOut_Line it = new SalesOut_Line
                        {
                            orderNo = reader["orderNo"].ToString(),
                            storeNo = reader["storeNo"].ToString(),
                            itemNo = reader["itemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            brand = reader["brand"].ToString(),
                            division = reader["division"].ToString(),
                            category = reader["category"].ToString(),
                            group = reader["group"].ToString(),
                            corePrice=decimal.Parse(reader["corePrice"].ToString()),
                            orderQuantity=decimal.Parse(reader["orderQuantity"].ToString()),
                            realQuantity=decimal.Parse(reader["realQuantity"].ToString()),
                            status = int.Parse(reader["status"].ToString()),
                            status_Content = reader["status_Content"].ToString(),
                            createdDate = reader["createdDate"].ToString(),
                            modifyDate = reader["modifyDate"].ToString()
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<TO_ItemTotal>> sp_get_TO_ItemTotal(string userName, string datekey, string typeFill, string dataFill, string Router)
        {
            List<TO_ItemTotal> its_s = new List<TO_ItemTotal>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_get_TO_ItemTotal", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("datekey", datekey));
                    cmd.Parameters.Add(new SqlParameter("typeFill", typeFill));
                    cmd.Parameters.Add(new SqlParameter("dataFill", dataFill));
                    cmd.Parameters.Add(new SqlParameter("Router", Router));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TO_ItemTotal it = new TO_ItemTotal
                        {
                            dateKey = reader["dateKey"].ToString(),
                            itemNo = reader["itemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            Router=reader["Router"].ToString(),
                            brandName = reader["brandName"].ToString(),
                            divisionCode = reader["divisionCode"].ToString(),
                            categoryCode = reader["categoryCode"].ToString(),
                            groupCode = reader["groupCode"].ToString(),
                            quantity = decimal.Parse(reader["quantity"].ToString()),
                            is_Done = int.Parse(reader["is_Done"].ToString())
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<SalesOut_Line>> sp_Picking_get_TO_Detail_Line(string userName, string dateFill, string typeFill, string dataFill, string Router)
        {
            List<SalesOut_Line> its_s = new List<SalesOut_Line>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_TO_Detail_Line", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));
                    cmd.Parameters.Add(new SqlParameter("typeFill", typeFill));
                    cmd.Parameters.Add(new SqlParameter("dataFill", dataFill));
                    cmd.Parameters.Add(new SqlParameter("Router", Router));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        SalesOut_Line it = new SalesOut_Line
                        {
                            orderNo = reader["orderNo"].ToString(),
                            storeNo = reader["storeNo"].ToString(),
                            itemNo = reader["itemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            brand = reader["brand"].ToString(),
                            division = reader["division"].ToString(),
                            category = reader["category"].ToString(),
                            group = reader["group"].ToString(),
                            corePrice=decimal.Parse(reader["corePrice"].ToString()),
                            orderQuantity=decimal.Parse(reader["orderQuantity"].ToString()),
                            realQuantity=decimal.Parse(reader["realQuantity"].ToString()),
                            status = int.Parse(reader["status"].ToString()),
                            status_Content = reader["status_Content"].ToString(),
                            createdDate = reader["createdDate"].ToString(),
                            modifyDate = reader["modifyDate"].ToString(),
                            storeLayout=int.Parse(reader["storeLayout"].ToString())
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<RouterItem>> sp_Picking_get_RouterType()
        {
            List<RouterItem> its_s = new List<RouterItem>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_RouterType", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        RouterItem it = new RouterItem
                        {
                            Router=reader["Router"].ToString(),
                            Calenders=reader["Calenders"].ToString(),
                            Note=reader["Note"].ToString()
                        };

                        its_s.Add(it);
                    }
                    con.Close();

                    return its_s;
                }
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<SalesOut_Line>> sp_Picking_get_ItemForStore_least(string userName, string dateFill, string storeNo)
        {
            List<SalesOut_Line> its_s = new List<SalesOut_Line>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_ItemForStore_least", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));
                    cmd.Parameters.Add(new SqlParameter("storeNo", storeNo));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        SalesOut_Line it = new SalesOut_Line
                        {
                            orderNo = reader["orderNo"].ToString(),
                            storeNo = reader["storeNo"].ToString(),
                            itemNo = reader["itemNo"].ToString(),
                            itemName = reader["itemName"].ToString(),
                            status = int.Parse(reader["status"].ToString()),
                            status_Content = reader["status_Content"].ToString()
                        };

                        its_s.Add(it);
                    }
                    con.Close();
                }

                return its_s;
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<ItemWait>> sp_Picking_get_AcceptItemWait(string userName, string dateFill, string typeFill, string dataFill)
        {
            List<ItemWait> its_s = new List<ItemWait>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_AcceptItemWait", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));
                    cmd.Parameters.Add(new SqlParameter("typeFill", typeFill));
                    cmd.Parameters.Add(new SqlParameter("dataFill", dataFill));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ItemWait it = new ItemWait
                        {
                            storeNo = reader["storeNo"].ToString(),
                            storeName = reader["storeName"].ToString(),
                            orderNo = reader["orderNo"].ToString(),
                            itemNo = reader["itemNo"].ToString(),
                            itemNam = reader["itemNam"].ToString(),
                            quantity = decimal.Parse(reader["quantity"].ToString()),
                            userCreated=reader["userCreated"].ToString()
                            
                        };

                        its_s.Add(it);
                    }
                    con.Close();
                }

                return its_s;
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<boxType_Item>> sp_Picking_get_BoxType(string userName)
        {
            List<boxType_Item> its_s = new List<boxType_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_BoxType", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        boxType_Item it = new boxType_Item
                        {
                            boxCode=reader["boxCode"].ToString(),
                            boxName=reader["boxName"].ToString(),
                            height=decimal.Parse(reader["height"].ToString()),
                            width=decimal.Parse(reader["width"].ToString()),
                            length=decimal.Parse(reader["length"].ToString()),
                            volume=decimal.Parse(reader["volume"].ToString())

                        };

                        its_s.Add(it);
                    }
                    con.Close();
                }

                return its_s;
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<volumeByItem>> sp_Picking_get_VolumeByItem(string userName, string dateFill, string storeNo)
        {
            List<volumeByItem> its_s = new List<volumeByItem>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_VolumeByItem", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));
                    cmd.Parameters.Add(new SqlParameter("storeNo", storeNo));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        volumeByItem it = new volumeByItem
                        {
                            orderNo=reader["orderNo"].ToString(),
                            volume=decimal.Parse(reader["volume"].ToString())
                        };

                        its_s.Add(it);
                    }
                    con.Close();
                }

                return its_s;
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        public async Task<List<RA_Item>> sp_Picking_get_ReceiveAcceptOrder(string userName, string dateFill)
        {
            List<RA_Item> its_s = new List<RA_Item>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_get_ReceiveAcceptOrder", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateFill", dateFill));


                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        RA_Item it = new RA_Item
                        {
                            vendorNo=reader["vendorNo"].ToString(),
                            vendorName=reader["vendorName"].ToString(),
                            orderNo=reader["orderNo"].ToString(),
                            orderDate=reader["orderDate"].ToString(),
                            shipDate=reader["shipDate"].ToString(),
                            shipDatetime=reader["shipDatetime"].ToString(),
                            transTo=reader["transTo"].ToString(),
                            totalItem=int.Parse(reader["totalItem"].ToString()),
                            totalQuantity=int.Parse(reader["totalQuantity"].ToString()),
                            status_Code=int.Parse(reader["status_Code"].ToString()),
                            status_Content=reader["status_Content"].ToString()
                        };

                        its_s.Add(it);
                    }
                    con.Close();
                }

                return its_s;
            }
            catch (Exception ex)
            {
                return its_s;
            }
        }

        #region dataAction

        #region updateData
        public async Task<bool> sp_Picking_update_SalesIn(string userName, string orderNo, int status)
        {
            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_update_SalesIn", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("orderNo", orderNo));


                    var reader = cmd.ExecuteNonQuery();

                    con.Close();


                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> sp_Picking_update_SalesOut(string userName, string orderNo, int status)
        {
            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_update_SalesOut", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("orderNo", orderNo));


                    var reader = cmd.ExecuteNonQuery();

                    con.Close();


                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> sp_Picking_update_Report(string userName, string orderNo, int status)
        {
            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_update_Report", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("orderNo", orderNo));


                    var reader = cmd.ExecuteNonQuery();

                    con.Close();


                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> sp_Picking_update_InvoiceHeader(string userName, string invoiceNo, int status)
        {
            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_update_InvoiceHeader", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("invoiceNo", invoiceNo));
                    cmd.Parameters.Add(new SqlParameter("status", status));


                    var reader = cmd.ExecuteNonQuery();

                    con.Close();

                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region addData

        public async Task<bool> sp_Picking_add_SalesIn(List<salesIn_Detail> it, string userName, int type)
        {
            try
            {
                foreach (var sub_i in it)
                {
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("sp_Picking_add_SalesIn", con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("orderNo", sub_i.orderNo));
                        cmd.Parameters.Add(new SqlParameter("itemNo", sub_i.itemNo));
                        cmd.Parameters.Add(new SqlParameter("orderQuantity", sub_i.orderQuantity));
                        cmd.Parameters.Add(new SqlParameter("realQuantity", sub_i.realQuantity));
                        cmd.Parameters.Add(new SqlParameter("corePrice", decimal.Parse(sub_i.corePrice.ToString())));
                        cmd.Parameters.Add(new SqlParameter("createdBy", userName));
                        cmd.Parameters.Add(new SqlParameter("status", type));

                        var reader = cmd.ExecuteNonQuery();

                        con.Close();
                    }

                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> sp_Picking_add_SalesOut(List<salesOut_Detail> it, string userName, int type)
        {
            try
            {
                foreach (var sub_i in it)
                {
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("sp_Picking_add_SalesOut", con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("orderNo", sub_i.orderNo));
                        cmd.Parameters.Add(new SqlParameter("itemNo", sub_i.itemNo));
                        cmd.Parameters.Add(new SqlParameter("orderQuantity", sub_i.orderQuantity));
                        cmd.Parameters.Add(new SqlParameter("realQuantity", sub_i.realQuantity));
                        cmd.Parameters.Add(new SqlParameter("corePrice", sub_i.corePrice));
                        cmd.Parameters.Add(new SqlParameter("createdBy", userName));
                        cmd.Parameters.Add(new SqlParameter("status", type));

                        var reader = cmd.ExecuteNonQuery();

                        con.Close();
                    }

                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> sp_Picking_add_ReportLine(List<report_Detail> it, string userName, int type)
        {
            try
            {
                foreach (var sub_i in it)
                {
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("sp_Picking_add_ReportLine", con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("orderNo", sub_i.orderNo));
                        cmd.Parameters.Add(new SqlParameter("itemNo", sub_i.itemNo));
                        cmd.Parameters.Add(new SqlParameter("discountPercent", sub_i.discountPercent));
                        cmd.Parameters.Add(new SqlParameter("orderQuantity", sub_i.orderQuantity));
                        cmd.Parameters.Add(new SqlParameter("realQuantity", sub_i.realQuantity));
                        cmd.Parameters.Add(new SqlParameter("userName", userName));
                        cmd.Parameters.Add(new SqlParameter("status", type));

                        var reader = cmd.ExecuteNonQuery();

                        con.Close();
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<string> sp_Picking_add_InvoiceHeader(string userName)
        {
            string str_r = "";

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_add_InvoiceHeader", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        str_r = reader["invoiceNo"].ToString();
                    }
                    con.Close();

                    return str_r;
                }
            }
            catch (Exception ex)
            {
                return str_r;
            }
        }
        public async Task<bool> sp_Picking_add_InvoiceLine(List<invoice_Detail> it, string userName, int type)
        {
            try
            {
                foreach (var sub_i in it)
                {
                    var s_od = sub_i;

                    if (sub_i.isEdit == 1)
                    {
                        using (var con = new SqlConnection(ConnectionString))
                        {
                            con.Open();
                            SqlCommand cmd = new SqlCommand("sp_Picking_add_InvoiceLine", con);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add(new SqlParameter("invoiceNo", sub_i.invoiceNo));
                            cmd.Parameters.Add(new SqlParameter("orderNo", sub_i.orderNo));
                            if (type == 0)
                            {
                                cmd.Parameters.Add(new SqlParameter("itemNo", ""));
                                cmd.Parameters.Add(new SqlParameter("discountPercent", "0.0"));
                            }
                            else
                            {
                                cmd.Parameters.Add(new SqlParameter("itemNo", sub_i.itemNo));

                                cmd.Parameters.Add(new SqlParameter("discountPercent", decimal.Parse(sub_i.discount.ToString())));
                            }
                            cmd.Parameters.Add(new SqlParameter("userName", userName));
                            cmd.Parameters.Add(new SqlParameter("type", type));

                            var reader = cmd.ExecuteNonQuery();

                            con.Close();
                        }
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> sp_Picking_add_CheckListResult(List<CheckList_Result_Item> its, string userName, string orderNo)
        {
            try
            {
                foreach (var sub_i in its)
                {
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("sp_Picking_add_CheckListResult", con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("userName", userName));
                        cmd.Parameters.Add(new SqlParameter("orderNo", sub_i.orderNo));
                        cmd.Parameters.Add(new SqlParameter("codeCheck", sub_i.codeCheck));
                        cmd.Parameters.Add(new SqlParameter("is_codeCheck", sub_i.is_codeCheck));
                        cmd.Parameters.Add(new SqlParameter("subCheck", sub_i.subCheck));
                        cmd.Parameters.Add(new SqlParameter("is_subCheck", sub_i.is_subCheck));
                        cmd.Parameters.Add(new SqlParameter("descriptions", sub_i.descriptions));


                        var reader = cmd.ExecuteNonQuery();

                        con.Close();
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> sp_Picking_add_PackingLine(List<packingLine_Item> its, string userName)
        {
            try
            {
                foreach (var sub_i in its)
                {
                    if (sub_i.packTotal > 0)
                    {
                        using (var con = new SqlConnection(ConnectionString))
                        {
                            con.Open();
                            SqlCommand cmd = new SqlCommand("sp_Picking_add_PackingLine", con);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add(new SqlParameter("userName", userName));
                            cmd.Parameters.Add(new SqlParameter("orderNo", sub_i.orderNo));
                            cmd.Parameters.Add(new SqlParameter("boxCode",sub_i.boxCode.Trim()));
                            cmd.Parameters.Add(new SqlParameter("packTotal", sub_i.packTotal));


                            var reader = cmd.ExecuteNonQuery();

                            con.Close();
                        }
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> sp_Picking_add_TO_ItemTotal_Line(string userName, TO_ItemTotal it)
        {
            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_add_TO_ItemTotal_Line", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("dateKey", it.dateKey));
                    cmd.Parameters.Add(new SqlParameter("itemNo", it.itemNo));
                    cmd.Parameters.Add(new SqlParameter("isDone", it.is_Done));
                    cmd.Parameters.Add(new SqlParameter("Router", it.Router));



                    var reader = cmd.ExecuteNonQuery();

                    con.Close();
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> sp_Picking_add_ReceiveAcceptOrder(string userName, RA_Item it)
        {
            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_Picking_add_ReceiveAcceptOrder", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("userName", userName));
                    cmd.Parameters.Add(new SqlParameter("vendorNo", it.vendorNo));
                    cmd.Parameters.Add(new SqlParameter("orderNo", it.orderNo));
                    cmd.Parameters.Add(new SqlParameter("status", it.status_Code));


                    var reader = cmd.ExecuteNonQuery();

                    con.Close();
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;

            }
        }
            #endregion

            #endregion

            #endregion


        }
}
