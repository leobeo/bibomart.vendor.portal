$.fn.extend({
	getAllTextNodes : function(noSpace){
    		var nodes = [];
      		var textNodes = [];
      		if(this[0].nodeType == 3) return [this[0]];
      		nodes.push(this[0]);
      		while(nodes.length > 0){
      			var node = nodes.pop();        
        		$(node).contents().each(function(){
          			if(this.nodeType == 3){
                			if(!noSpace || /\S+/.test(this.nodeValue)) {
              					textNodes.push(this);
                			}
              			} else {
              				nodes.push(this);
              			}
        		});
      		}
      		return textNodes;
    	}
});