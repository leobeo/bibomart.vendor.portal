//Author: dotnet.pro.no1@gmail.com
//Date: 08/18/2017
//
// This is an extension method on jQuery to turn a div into an editor (replacing text input, textarea ,,,)
// which can highlight keywords, you can provide a lookup to resolve color for each keyword or even a resolveColorCallback can be used.
// You can even provide class for each wrapped keyword - so that it can be targeted later (either using script or CSS) to such as support popping up some 
// extended info on hovering the keyword...

// USAGE: 
// Just call initHighlighter() on any jQuery object, it however should be a block element such as div.
//  NOTE: all the content of the element on which this method is called will be cleared. So be sure that you used an empty element.
//  Arguments:
//	- matchPattern : should be a RegExp instance, this is required and will be used to match all keywords which should be highlighted.
//	- options: this is optional, but without any provided, the highlighting simply won't work.
//		   this should be an object which has the following properties:
//		+ colorLookup : should be an object with properties being all possible keywords so that the keywords can be looked-up 
//				and result in the corresponding color (so the properties' value should be a CSS color).
//		+ classLookup : similar to colorLookup but instead of looking up for a color, it should return a class name (which will be applied to the 
//				wrapper span element of the corresponding keyword).
//		+ replacingCallback : this is a callback which can be used to completely take control over the replacement for the keywords.
//				So by using this callback you should provide your own rendering rule, the colorLookup and classLookup won't be usable.
//				To continue using the default replacement, you just need to return null in that callback.
//		+ resolveClassCallback: this is a callback which can be used instead of classLookup, it provides a dynamic way to resolve for class name 
//		  instead of using the (usually) one-time fixedly init classLookup.
$.fn.extend({
	jsHighlighter_init : function(matchPattern, options){    
			options = options || {};
	    		var colorLookup = options.colorLookup;
    			var replacingCallback = options.replacingCallback;
    			var classLookup = options.classLookup;
    			var resolveClassCallback = options.resolveClassCallback;
  			this[0].style.position = "relative";
    			this.html(function(i,ohtml) { return "<div class='renderer' style='position:absolute;left:0px;right:0px;bottom:0px;top:0px;overflow-y:auto;overflow-x:hidden'>" + ohtml + "</div><div contenteditable='true' class='editor' style='position:absolute;left:0px;right:0px;bottom:0px;top:0px;overflow-y:auto;overflow-x:hidden;color:transparent;background:transparent;caret-color:black'>" + ohtml + "</div>";});
    			var editor = this.find(".editor");
    			var renderer = this.find(".renderer");
			var _this = this;
    			editor.on("input",function(){      
  				renderer.html(editor.html().replace(matchPattern, function(m){      
          				if(replacingCallback) {
          					var v = replacingCallback(m);
            					if(v != null) return v;
          				} 
          				var c = "";
          				if(resolveClassCallback){
          					c = resolveClassCallback(m);
          				} else if(classLookup) {
            					c = classLookup[m];
          				}
          				if(c) c = " class='" + c + "'"; 
          				var style = "";
          				if(colorLookup) {
            					var color = colorLookup[m];
          					if(color) style = "color:" + color;
          				}
          				if(style) style = " style='" + style + "'";
      					return "<span" + c + style + ">" + m + "</span>"; 
  				}));
				//propagate the input event so that the original editor element can notify about the text change
				_this[0].inputText = $(this).text();
				_this[0].inputHtml = $(this).html();
				_this.trigger("input");     
		
			}).scroll(function(){
				renderer.scrollTop($(this).scrollTop());
			}).triggerHandler("input");
			return _this;
  	      	},
	jsHighlighter_setHtml : function(html, forRendererOnly){
					if(forRendererOnly){
						var renderer = this.find(".renderer");
						if(renderer){
							renderer.html(html);
						}
					} else {
					    var editor = this.find(".editor");
						if(editor) {
							editor.html(html);
						}
					}
					return this;
	},
	jsHighlighter_getHtml : function(ofRenderer){
	    if(ofRenderer){
	        var renderer = this.find(".renderer");
	        if(renderer){
	            return renderer.html();
	        }
	    } else {
	        var editor = this.find(".editor");
	        if(editor){
	            return editor.html();
	        }
	    }
	},
	jsHighlighter_setInteractiveOn : function(interactive){
						if(interactive === undefined){
							interactive = true;
						}
						var editor = this.find(".editor");
						var renderer = this.find(".renderer");
						if(!editor || !renderer) return;
						if(interactive){
							editor[0].style.pointerEvents = "none";
							renderer.on("click.highlighter", function(e){
								//temporarily set this to make the editor interactive to capture its ranges and selection
								editor[0].style.pointerEvents = "auto";	
								var range = null;
								if(document.caretPositionFromPoint){
				    					range = document.caretPositionFromPoint(e.clientX, e.clientY);
								} else if(document.caretRangeFromPoint) {
				    					range = document.caretRangeFromPoint(e.clientX, e.clientY);
								}
								if(range){
				    					if(window.getSelection){
				       						var sel = window.getSelection();
				       						sel.removeAllRanges();
			               						sel.addRange(range);
				    					} else { //fall back to a simple handling (almost happens to prior IE 9)
				       						editor.focus();//this of course won't place the caret at the desired position (right under the mouse pointer).
						   			//it just focuses the editor to provide a possibility to type text in, user then need to use keyboard 
						   			//to navigate the caret to the desired position before start typing. (cannot use mouse)
				    					}	
								}
								//re-disabled the pointer interative on editor so that the actually renderer can be interactive.
								editor[0].style.pointerEvents = "none";
							});
						} else {
							editor[0].style.pointerEvents = "auto";
							renderer.off("click.highlighter");
						}
						return this;
					}	    		     
});