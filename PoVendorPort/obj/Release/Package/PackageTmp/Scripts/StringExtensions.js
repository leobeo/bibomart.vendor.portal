/* Extensions on String type, using prototype
Author: dotnet.pro.no1@gmail.com

*/
(function(){ 
    //strip Vietnamese accents 
    var charLookup = {}; 
    var source = ["aáàãạảăắằẵặẳâấầẫậẩ","eéèẽẹẻêếềễệể","iíìĩịỉ", "oóòõọỏôốồỗộổơớờỡợở", "uúùũụủưứừữựử", "yýỳỹỵỷ", "dđ"];
    source.forEach(function(e){
    	var vl = e[0];
    	var uvl = e[0].toUpperCase();
  	for(var i = 0; i < e.length; i++){
    		charLookup[e[i]] = vl;
      		charLookup[e[i].toUpperCase()] = uvl;
    	}
    });  
    String.prototype.stripAccents = function(){
  	return this.split("").reduce(function(c,e){
  	    var ch = e;
  	    if (e in charLookup) ch = charLookup[e];
  	    else {
  	        var cc = e.charCodeAt(0);
  	        //check if the current char is just a combining diacritical char (a type of nonspacing mark)
  	        //this kind of char should be ignored (because it is rendered as accent on the char before it).
  	        if (cc >= 0x300 && cc <= 0x36f) {
  	            ch = "";
  	        }
  	    }
  	    return c + ch;
                             },"");
    };
})();