﻿var selectedLi = null;
var lastSortedColumnIndex = -1;
var sortedHeaderCell = null;
var poNote = null;
var popupElem = null;
var hideVendorPopup = null;

var approvingStarted = function () {
    $("#approve-errors").toggleClass("noerror", true);
    $("#approve-button").toggleClass("disabled", true).toggleClass("approving", true)
                        .find("span")[0].className = "glyphicon glyphicon-refresh";
    $("#purchase-items-container").toggleClass("approving", true);
}
var approvingDone = function () {
    $("#approve-button").toggleClass("disabled", false).toggleClass("approving", false)
                        .find("span")[0].className = "glyphicon glyphicon-ok";
    $("#purchase-items-container").toggleClass("approving", false);
}
var showErrorPopup = function (onElement, title, message, duration) {
    if (undefined === duration) duration = 3000;
    $(onElement).popover({
        title: title,        
        content: message,
        container: "body",
        placement: "auto",
        trigger: "manual"
    }).popover("show");
    setTimeout(function () {
        $(onElement).popover("destroy");
    }, duration);
}
//this mainly updates the expected receipt date when qty-to-receive changes (from 0 to other and back to 0)
//If it's 0, the expected receipt date is meaningless (and should be disabled).
var updateInputStatusOnQtyToReceiveChange = function (qtyToReceiveCell) {    
    var ecd = qtyToReceiveCell.closest(".cell").nextAll(".cell").has("input.expected-receipt-date").first().find("input.expected-receipt-date");
    if (ecd.length) {
        ecd[0].readOnly = qtyToReceiveCell.val() == 0;
    }
}
//update the display showing about the current po info.
var updatePoInfoDisplay = function (selectedPo) {
    //update some po info currently shown on the page
    if (selectedPo) {
        $("#approve-button").prop("disabled", !selectedPo.IsEditable);
        $("#po-expected-receipt-time").prop("readonly", !selectedPo.IsEditable);
        $("#storeInfo").html(selectedPo.StoreInfo);
    }
    $("#pono").html(selectedPo ? selectedPo.PoNo : "");
    $("#order-date").html(selectedPo ? selectedPo.OrderDateDisplay : "");

    $("#status > span:first-child").html(selectedPo ? (selectedPo.Status ? "Đã duyệt" : "Chưa duyệt") : "");
    $("#status").toggleClass("approved", selectedPo && selectedPo.Status ? true : false).toggleClass("text-success", selectedPo && selectedPo.Status ? true : false);

    $("#status").attr("data-original-title", selectedPo && selectedPo.Status ? "Duyệt lần cuối vào " + selectedPo && selectedPo.LastApprovedDateString : "");
    $("#po-sent-state").toggleClass("not-sent", selectedPo.Sended == 0)
                       .toggleClass("approved", selectedPo.Status != 0);
    $("#po-detail").toggleClass("approved", selectedPo && selectedPo.Status != 0)
                   .toggleClass("goods-received", selectedPo && selectedPo.Approved != 0);
}

//this must be called using call() with the instance of sorted header cell passed in.
var sortColumn = function (colIndex, toggle) {
    var i = colIndex || $(this).index();
    if (toggle) {
        if (this.desc == undefined) {
            this.desc = false;
        } else {
            this.desc = !this.desc;
        }
    }
    //remove sorted from last sorted column
    if (i != lastSortedColumnIndex && lastSortedColumnIndex != -1) {
        $(".csstable > .table-header > .table-row > .cell").eq(lastSortedColumnIndex).toggleClass("sorted", false)[0].desc = undefined;
    }

    var rowsContainer = $("#purchase-items-container");
    var isDesc = this.desc;
    var groupedNumberRegex = /\d+(,\d{3})*/;
    //sort according to the new column             
    rowsContainer.html(rowsContainer.children(".table-row").map(function (k, e) { return $(e).children(".cell").eq(i).map(function (j, el) { var input = $(this).find("input"); return input.length ? input[0] : el; }); })
                                               .sort(function (a, b) {
                                                   var s1 = $(a).val() || $(a).html().trim().stripAccents();
                                                   var s2 = $(b).val() || $(b).html().trim().stripAccents();
                                                   if (isDesc) {
                                                       var s = s1; s1 = s2; s2 = s;
                                                   }

                                                   if (groupedNumberRegex.test(s1) && groupedNumberRegex.test(s2)) {
                                                       s1 = s1.replace(",", "");
                                                       s2 = s2.replace(",", "");
                                                   }
                                                   //check for number first
                                                   var d1 = new Number(s1);
                                                   var d2 = new Number(s2);
                                                   if (isNaN(d1) || isNaN(d2)) {
                                                       //check next for datetime
                                                       d1 = new Date(s1).valueOf();
                                                       d2 = new Date(s2).valueOf();                                                                                                              
                                                   }
                                                   if (isNaN(d1) || isNaN(d2)) {
                                                       return s1.localeCompare(s2);
                                                   } else {                                                       
                                                       return d1 - d2;
                                                   }
                                               })
                                               .map(function (k, e) { return $(e).closest(".table-row")[0]; }));
    lastSortedColumnIndex = i;
    sortedHeaderCell = this;
    $(this).closest(".cell").toggleClass("sorted", true).find("span.glyphicon").toggleClass("glyphicon-chevron-down", isDesc)
                                                              .toggleClass("glyphicon-chevron-up", !isDesc);
}

//this callback will be called on success (connection) by the AjaxHelper (BeginForm)
function submitSuccess(data) {
    approvingDone();
    if (data == "ok") {        
        $(selectedLi).toggleClass("approved", true);
        //update the number of approved purchase orders
        $("#approved-po-count").html($("#pos .body ul > li.approved").length);
        var d = new Date();
        d = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        $(selectedLi).data("poinfo").LastApprovedDateString = d;
        //auto next
        if (selectedLi) {
            var poinfo = $(selectedLi).data("poinfo");
            poinfo.Status = 1;
            var li = $(selectedLi).next("li");
            if (li.length) li.trigger("click");
            else { //no more po
                updatePoInfoDisplay(poinfo);
                //turn modified to last-modified for the current po
                $("#purchase-items-container .table-row.modified").toggleClass("modified", false)
                                                                  .toggleClass("last-modified", true)
                                                                  .find(".cell input.modified").toggleClass("modified").each(function () {
                                                                      this.title = "";
                                                                  });
                //because the last (current) po has been confirmed, the modified cells (as before) should be considered normal again 
                //so the number of edited/modified row should be reset back to 0 (for the current po).
                $("#edited-items-count").html("0 / " + $("#purchase-items-container").find(".table-row").length);
            }
        }
    } else {
        $("#approve-errors").html(data).toggleClass("noerror", false);
    }
}
function submitComplete(xhr, status) {
    if (status != "success") {
        approvingDone();
        $("#approve-errors").html("Có lỗi kết nối, xin thử lại sau.").toggleClass("noerror", false);
    }
}
function loadPurchaseItemsForSelectedPO() {
    if (selectedLi) $(selectedLi).toggleClass("selected", false);
    $(this).toggleClass("selected", true);
    selectedLi = this;
    var disabled = $(this).is(":first-child");
    $("#prev-po-button")[0].initDisabled = disabled;
    disabled = $(this).is(":last-child");
    $("#next-po-button")[0].initDisabled = disabled;

    //load the corresponding items        
    $(this).trigger("loadingPurchaseItems");
}

function initVendorTagsPopup(scope) {       
    $(scope).find(".vendor-tag").mouseover(function () {        
        clearTimeout(hideVendorPopup);
        hideVendorPopup = null;
        var info = $(this).data("vendorinfo");        
        var popup = "";
        var tagRect = this.getBoundingClientRect();
        var x = tagRect.left + tagRect.width/2;
        var y = tagRect.top + tagRect.height + 10;        
        //PoUser
        if (info.State == 1) {
            var note = info.IsManaged ? "<div><span style='font-weight:bold;color:red'>Chú ý:</span><span style='font-style:italic'>Nhà cung cấp đã được quản lý bởi 1 người dùng khác.</span></div>" : "";
            popup = "<div class='vendor-info-popup'><table class='vender-info-table'><tr><td style='font-weight:bold'>Mã NCC: </td><td>" + info.No + "</td></tr><tr><td style='font-weight:bold'>Tên NCC: </td><td>" + info.VietnameseName + "</td></tr></table>" + note + "</div>";
        }
        else if(info.State == 0 || info.State == 2) {
            popup = "<div class='vendor-info-popup'><div>Nhà cung cấp chưa có tài khoản và đơn hàng</div><a href='#' class='remove-vendor-tag'>Xóa khỏi danh sách?</a></div>";
        } else if (info.State == 3) {
            popup = "<div class='vendor-info-popup'><div>Nhà cung cấp đã có tài khoản nhưng không có đơn hàng nào</div><a href='#' class='remove-vendor-tag'>Xóa khỏi danh sách?</a></div>";
        }
        if (popup != "") {
            popupElem[0].vendorTag = this;
            showPopup(popup, x, y);
        }
    }).mouseout(function () {        
        if (popupElem.is(".mover")) return;
        clearTimeout(hideVendorPopup);
        hideVendorPopup = setTimeout(function () {
            if (popupElem.is(".mover")) return;
            hidePopup();
        }, 1000);
    });
}
var shownOnce = false;
function showPopup(html, x, y) {    
    popupElem.css({ left: x, top: y })
             .html(html).toggleClass("shown", true);    
    popupElem.find(".remove-vendor-tag").click(function () {
        var vendorTag = popupElem[0].vendorTag;
        if (vendorTag) {
            var hl = $(vendorTag).closest(".highlighter");
            var editor = hl.find(".editor");
            var vendorNo = $(vendorTag).html().trim();            
            $(vendorTag).remove();            
            var normalizedHtml = $(editor.getAllTextNodes(true)).map(function (i, e) {
                                                                    return e.nodeValue.trim().match(/\w+/g)
                                                                 })
                                                                .filter(function (i, e) { return e != vendorNo; }).toArray()
                                                                .join(', ');            
            editor.html(normalizedHtml).triggerHandler("input");            
            //hl.jsHighlighter_setHtml(normalizedHtml);
        }
        hidePopup();
    });
    if (!shownOnce) {
        shownOnce = true;
        setTimeout(function () {
            popupElem.css("transition", "all 1s");
        }, 500);        
    }
}
function hidePopup() {
    popupElem.toggleClass("shown", false);
}
function showVendorDetail(vendorLink) {
    var vd = $(".vendor-detail");
    vendorLink = $(vendorLink);
    var vendorNo = vendorLink.data("vendor");
    var vendorName = vendorLink.data("vendor-name");
    var vendorPoCount = vendorLink.data("vendor-pocount");
    var vnoElement = $("#detail-vendor-no");
    var vnameElement = $("#detail-vendor-name");
    var poCountElement = $("#detail-vendor-pocount");
    
    vnoElement.html(vendorNo);
    vnameElement.html(vendorName);
    poCountElement.html(vendorPoCount);
    
    vd.css("z-index", 1)                        
      .toggleClass("shown", true);
}
function hideVendorDetail() {
    $(".vendor-detail").on("transitionend", function () {
                            $(this).css("z-index", -1).off("transitionend");
                     }).toggleClass("shown", false);
}
var showPoNote = function (show, focus) {
    if (undefined === show) show = true;
    $(".po-note-link").toggleClass("note-shown", show);
    if (show && focus) {
        $("#po-note-edit").focus();
    }
};
//for left nav
$(function () {
    popupElem = $("<div>", { class: "popup" }).appendTo(document.body);
    popupElem.mouseover(function () {        
        $(this).toggleClass("mover", true);
        clearTimeout(hideVendorPopup);
        hideVendorPopup = null;
    }).mouseout(function () {
        $(this).toggleClass("mover", false);
        clearTimeout(hideVendorPopup);
        hideVendorPopup = setTimeout(function () {
            hidePopup();
        }, 1000);
    });
    $(".vendor-detail").css("z-index", -1);
    $("#status, #next-po-button, #prev-po-button").tooltip();

    $(".po-list .header .nav-button").on("click", function (e) {
        var polist = $(this).closest(".po-list");
        var expanded = polist.hasClass("expanded");
        polist.toggleClass("expanded");                
        if (expanded) $(this).html("<span class='glyphicon glyphicon-chevron-right'></span>");
        else {
            $(this).html("<span class='glyphicon glyphicon-chevron-left'></span>");
            $("#po-search").focus();
        }
        e.preventDefault();                
        //alert("???");
    });    
    $(".po-list ul li").click(function (e) {        
        if (this == selectedLi) return;        
        loadPurchaseItemsForSelectedPO.call(this);
    });
    $([document, window]).click(function (e) {
        //console.log(e);
        var l = $(e.target).closest(".po-list").length;
        if (l) return;        
        var shouldReturn = false;
        if (e.originalEvent && e.originalEvent.path) {
            e.originalEvent.path.forEach(function (o) {
                if ($(o).hasClass("po-list")) {
                    shouldReturn = true;
                    return;
                }
            });
        }
        if (shouldReturn) return;
        /*
        if($(e.originalEvent.path.some(function (o) {
            return true;//$(o).hasClass("po-list");
        }))) return; */        
        $(".po-list.expanded .header .nav-button").trigger("click");
    });
    $("#submit-form").submit(function (e) {
        var reasonsGroup = $("#reasons-not-to-provide-items");
        var hasZero = $("#purchase-items-container .cell .quantity-to-receive").is(function (i, o) {
            return parseInt($(this).val()) == 0;
        });
        if (hasZero) {
            var selectedReason = parseInt(reasonsGroup.val());
            if (selectedReason == -2) {
                showErrorPopup(reasonsGroup[0], "", "Bạn cần chọn lý do không cung cấp được hàng");
                return false;
            }
            else {
                var note = $("#po-note-content").val().trim();
                if (!note) {

                    reasonsGroup.popover({
                        content: "Bạn cần nhập lý do không cung cấp được hàng",
                        title: "",
                        container: "body",
                        trigger: "manual",
                        placement: "auto"
                    }).popover("show");
                    if (reasonsGroup.val() == "-1") {
                        showPoNote(true, true);
                    }
                    setTimeout(function () {
                        reasonsGroup.popover("destroy");
                    }, 3000);
                    //e.preventDefault();
                    return false;
                } else {
                    approvingStarted();
                }
            }
        } else {
            approvingStarted();
        }
    });

    $(".panel-heading.header").on("affix.bs.affix", function () {
        var tb = $(this).nextAll(".csstable.panel-body").first();
        if (tb.length) {
            var tbh = tb.find(".table-header").first();
            tbh.find(".cell").each(function () {                
                this.fixedWidth = $(this).width();
            });
        }
    });
    $(".panel-heading.header").on("affixed.bs.affix", function () {        
        //find the table body
        var tb = $(this).nextAll(".csstable.panel-body").first();
        if (tb.length) {
            var rect = tb[0].getBoundingClientRect();            
            $(this).css("left", rect.left).css("width", rect.width);
            var tbh = tb.find(".table-header").first();
            var thisRect = this.getBoundingClientRect();
            tbh.css("position", "fixed").css("left", rect.left).css("width", rect.width).css("top", thisRect.bottom);
            tbh.find(".cell").width(function () {
                return this.fixedWidth;
            });
            $(this).closest(".purchase-items-list").css("margin-top", $(this).height() + tbh.height());
        }
    }).on("affixed-top.bs.affix", function () {
        $(this).css("left", 0).css("width", "auto");
        var tb = $(this).nextAll(".csstable.panel-body").first();
        if (tb.length) {
            var tbh = tb.find(".table-header").first();
            tbh.css("position", "static").css("left", 0).css("width", "auto").css("top", 0);
            $(this).closest(".purchase-items-list").css("margin-top", "auto");
        }
    });
    $("#next-po-button, #prev-po-button").click(function (e) {        
        if (!selectedLi) return;
        var _this = this;
        var goon = function () {
            var next = _this.id == "next-po-button";
            var li = next ? $(selectedLi).next("li") : $(selectedLi).prev("li");
            if (li.length) {
                li.trigger("click");
            }
        }
        //check if there is any change to ask for user to confirm before going to the next/prev page
        if ($("#purchase-items-container .table-row.modified").length) {
            vex.dialog.confirm({
                message: "Bạn đã có sửa đổi nhưng chưa duyệt.\nBạn có đồng ý tiếp tục chuyển sang đơn hàng mới không? Những sửa đổi chưa duyệt sẽ không được lưu.",
                callback: function (ok) {
                    if (ok) {
                        goon();
                    }
                }
            });
        } else {
            goon();
        }
    });
    
    //use custom scrollbar for the po list
    $(".po-list > .body, .po-note > .edit").customScrollbar();
    //handle searching for the purchase orders
    var resizingTimeout = null;
    $("#po-search").on("input", function () {
        var vl = $(this).val().trim().toLowerCase();
        $("#pos .body ul > li").each(function () {
            $(this).toggleClass("hidden", vl != "" && $(this).html().toLowerCase().indexOf(vl) == -1);
            if(resizingTimeout) clearTimeout(resizingTimeout);
            resizingTimeout = setTimeout(function () {
                $(".po-list > .body").customScrollbar("resize", false);
            }, 500);
        });
    });
    //handle searching for the purchase items
    $("#item-search").on("input", function () {
        var vl = $(this).val().trim().toLowerCase().stripAccents();
        //console.log(vl);
        $("#purchase-items-container .table-row").each(function () {
            $(this).toggleClass("hidden", $(this).find(".item-no").html().toLowerCase().stripAccents().indexOf(vl) == -1 &&
                                          $(this).find(".item-desc").html().toLowerCase().stripAccents().indexOf(vl) == -1);
        });
    });
    $("#toggle-modified-items").change(function () {
        $("#purchase-items-container").toggleClass("hide-modified-items", this.checked);
    });
    
    
    //for sortable columns
    $(".csstable > .table-header > .table-row > .cell.sortable, .csstable > .table-header > .table-row > .cell > .sortable").click(function () {
        sortColumn.call(this, undefined, true);
    });
    //$(".po-receipt-date > span").tooltip();
    //for a bit dainty looking dialog using vex.js
    vex.defaultOptions.className = 'vex-theme-default';
    vex.dialog.buttons.YES.text = "Chấp nhận";
    vex.dialog.buttons.NO.text = "Hủy";    
});