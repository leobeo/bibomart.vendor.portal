﻿$(function () {
    if ("undefined" == typeof mgroups) return;
    $("#manage-group").autocomplete({
        source: [mgroups],
        valueKey: "Name", //key to match with the typed query
        showHint: false,
        autoselect: true,
        openOnFocus: true,
        itemStyle: { paddingLeft: "12px", pointer: "cursor" },
        getTitle: function (item) {
            return item.Name; //prop to be shown on dropdown list
        },
        getValue: function (item) {
            return item.Name; //prop to be shown on the text input
        }
    }).on("selected.xdsoft", function (e, item) {
        $("#manage-group").triggerHandler("input");
        this.title = this.value;
    }).on("input change propertychange paste",function () {
        this.title = this.value;
    });
    //after about 1 second, just reupdate the autocomplete render to ensure that if the document size is changed (such as by some animation effect on validation error...)
    //it won't be rendered in wrong position and with wrong size.
    setTimeout(function () {
        $("#manage-group").autocomplete("update");        
    }, 1500);
    $(".validation-summary-errors > ul > li").on("animationend", function () {
        $("#manage-group").autocomplete("update");
    });
});