﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoVendorPort.Controllers
{
    using PoVendorPortCommon;
    using Models;
    using System.Threading.Tasks;
    using System.Web.Caching;
    using Microsoft.AspNet.Identity;
    using PoVendorPortCommon.Models;
    using DataAccess;
    using PoVendorPortCommon.Models.Picking;
    using PagedList;
    using Newtonsoft.Json;
    using PoVendorPortCommon.Models.Picking.CheckList;
    using PoVendorPortCommon.Models.Picking.ReceiveAccept;

    [Authorize]
    public class SalesInController : Controller
    {
        public SalesInController(IDataAccess da, ILogger logger)
        {
            DataAccess = da;
            Logger = logger;
            
        }

        public IDataAccess DataAccess { get; set; }
        public ILogger Logger { get; set; }

        // GET: SalesIn
        public async Task<ActionResult> Index(int? page, int? Size, string vendor, int? sort)
        {

            var total_wait = await DataAccess.sp_Picking_get_total_NewOrderForStore(User.Identity.GetUserId());

            /*if (Session["userRole"] == null)
            {*/
                List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                Session["userRole"] = bn;
            /*}*/

            List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];

            userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "SalesIn");

            if (role_Item == null)
            {
                return RedirectToAction("Error");
            }

             Session["role_Item"] = role_Item;

            if (role_Item.typeGroup == 1)
            {
                return RedirectToAction("ItemInput");
            }

            if (page == null) page = 1;

            if (sort == null) sort = 0;

            //List<vendor_Item> its = await DataAccess.sp_Picking_get_VendorPOCount(User.Identity.GetUserName());
            List<vendor_Item> its = await DataAccess.sp_Picking_get_VendorPOCount(User.Identity.GetUserId());

            Session["lsVendor"] = its;

            var listV = its.OrderBy(s => s.vendorNo);

            if (sort == 1)
            {

                if (vendor != null && vendor.Length > 0)
                {
                    listV = its.Where(s => s.vendorNo.ToLower().Contains(vendor.ToLower()) || s.vendorName.ToLower().Contains(vendor.ToLower())).OrderByDescending(s => s.vendorNo);
                }
                else
                {
                    listV = its.OrderByDescending(s => s.vendorNo);
                }
            }
            else
            {
                if (vendor != null && vendor.Length > 0)
                {
                    listV = its.Where(s => s.vendorNo.ToLower().Contains(vendor.ToLower()) || s.vendorName.ToLower().Contains(vendor.ToLower())).OrderBy(s => s.vendorNo);
                }
            }



            int pageSize = (Size ?? 5);

            // 4.1 Toán tử ?? trong C# mô tả nếu page khác null thì lấy giá trị page, còn
            // nếu page = null thì lấy giá trị 1 cho biến pageNumber.
            int pageNumber = (page ?? 1);

            int checkTotal = (int)(listV.Count() / pageSize) + 1;
            if (pageNumber > checkTotal) pageNumber = 1;

            ViewBag.pageSize = pageSize;
            ViewBag.vendor = vendor;
            ViewBag.sort = sort;

            return View(listV.ToPagedList(pageNumber, pageSize));
        }

        public async Task<ActionResult> Orders(string vendorNo, int? page, int? Size, string order, int? sort)
        {
            try
            {
                if (vendorNo == null || vendorNo.Length == 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    Session["vendorNo"] = vendorNo;
                    //getVendor

                    List<vendor_Item> lsVendor = (List<vendor_Item>)Session["lsVendor"];

                    var vd_info = lsVendor.FirstOrDefault(s => s.vendorNo == vendorNo);

                    ViewBag.vd_info = vd_info;

                    //get list Order


                    if (page == null) page = 1;

                    if (sort == null) sort = 0;

                    List<saleIn_Item> its = await DataAccess.sp_Picking_get_SaleIn(User.Identity.GetUserId(),vendorNo);

                    Session["lsPurchase"] = its;

                    IEnumerable<saleIn_Item> listV;

                    List<int> orderByStatus = new List<int> {2,1,3};

                    if (sort == 1)
                    {

                        if (order != null && order.Length > 0)
                        {
                            listV = its.Where(s => s.orderNo.ToLower().Contains(order.ToLower())).OrderByDescending(s=>s.orderNo).OrderBy(s => orderByStatus.IndexOf(s.status_code));
                        }
                        else
                        {
                            listV = its.OrderByDescending(s => s.orderNo).OrderBy(s => s.status_code).OrderBy(s => orderByStatus.IndexOf(s.status_code));
                        }
                    }
                    else
                    {
                        if (order != null && order.Length > 0)
                        {
                             listV = its.Where(s => s.orderNo.ToLower().Contains(order.ToLower())).OrderBy(s=>s.orderNo).OrderBy(s => orderByStatus.IndexOf(s.status_code));
                        }
                        else
                        {
                            listV = its.OrderBy(s=>s.orderNo).OrderBy(s => orderByStatus.IndexOf(s.status_code));
                        }
                    }


                    int pageSize = (Size ?? 5);


                    int pageNumber = (page ?? 1);

                    int checkTotal = (int)(listV.Count() / pageSize) + 1;
                    if (pageNumber > checkTotal) pageNumber = checkTotal;

                    ViewBag.pageSize = pageSize;
                    ViewBag.order = order;
                    ViewBag.vendorNo = vendorNo;
                    ViewBag.sort = sort;

                    return View(listV.ToPagedList(pageNumber, pageSize));
                }

            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }

        }

        public async Task<ActionResult> Detail(string orderNo, int? page, int? Size, string item, int? sort)
        {
            try
            {
                if (orderNo == null || orderNo.Length == 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    if(Session["vendorNo"] != null)
                    {
                        ViewBag.vendorNo = Session["vendorNo"].ToString();
                    }


                    List<salesIn_Detail> its = new List<salesIn_Detail>();
                    List<CheckList_Result_Item> ck_R = await DataAccess.Picking_get_CheckListResult(User.Identity.GetUserId(), orderNo);

                    if (Session["onEdit"] != null && Session["orderNo"]!=null && Session["orderNo"].ToString() == orderNo)
                    {
                        its = (List<salesIn_Detail>)Session["onEdit"];
                    }
                    else
                    {
                        its = await DataAccess.sp_Picking_get_SaleInDetail(User.Identity.GetUserId(), orderNo);
                        Session["orderNo"] = orderNo;
                        Session["onEdit"] = its;
                    }


                    ViewBag.ck_R = ck_R;

                    //get purchase info

                    List<saleIn_Item> lsPurchase = (List<saleIn_Item>)Session["lsPurchase"];

                    var pc_info = lsPurchase.FirstOrDefault(s => s.orderNo == orderNo);

                    ViewBag.pc_info = pc_info;

                    //get purchase detail


                    if (page == null) page = 1;

                    if (sort == null) sort = 0;

                    

                    var listV = its.OrderBy(s => s.itemNo);


                    if (sort == 1)
                    {

                        if (item != null && item.Length > 0)
                        {
                            listV = its.Where(s => s.itemNo.Substring(0,item.Length) == item).OrderByDescending(s => s.itemNo);
                        }
                        else
                        {
                            listV = its.OrderByDescending(s => s.itemNo);
                        }
                    }
                    else
                    {
                        if (item != null && item.Length > 0)
                        {
                            listV = its.Where(s => s.itemNo.Substring(0, item.Length) == item).OrderBy(s => s.itemNo);
                        }
                    }


                    int pageSize = (Size ?? 5);


                    int pageNumber = (page ?? 1);

                    int checkTotal = (int)(listV.Count() / pageSize) + 1;
                    if (pageNumber > checkTotal) pageNumber = checkTotal;

                    ViewBag.pageSize = pageSize;
                    ViewBag.item = item;
                    ViewBag.orderNo = orderNo;
                    ViewBag.sort = sort;

                    

                    return View(listV.ToPagedList(pageNumber, pageSize));
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> Checklist(string orderNo)
        {
            try
            {
                if (orderNo == null || orderNo.Length == 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    if (Session["vendorNo"] != null)
                    {
                        ViewBag.vendorNo = Session["vendorNo"].ToString();
                    }

                    List<salesIn_Detail> its = new List<salesIn_Detail>();

                    if (Session["onEdit"] != null && Session["orderNo"] != null && Session["orderNo"].ToString() == orderNo)
                    {
                        its = (List<salesIn_Detail>)Session["onEdit"];
                    }
                    else
                    {
                        its = await DataAccess.sp_Picking_get_SaleInDetail(User.Identity.GetUserId(), orderNo);
                        Session["orderNo"] = orderNo;
                        Session["onEdit"] = its;
                    }

                    //get purchase info

                    List<saleIn_Item> lsPurchase = (List<saleIn_Item>)Session["lsPurchase"];

                    var pc_info = lsPurchase.FirstOrDefault(s => s.orderNo == orderNo);

                    //order Header
                    ViewBag.pc_info = pc_info;

                    //orderNoo
                    ViewBag.orderNo = orderNo;

                    //Checklist
                    var lsCK = await DataAccess.sp_Picking_get_Checklist(User.Identity.GetUserId());
                    List<CheckList_Result_Item> ls_checkList_Result_s = new List<CheckList_Result_Item>();
                    foreach(var i in lsCK)
                    {
                        CheckList_Result_Item it = new CheckList_Result_Item
                        {
                            orderNo = orderNo,
                            codeCheck = i.codeCheck,
                            inputType=i.inputType
                        };

                        ls_checkList_Result_s.Add(it);
                    }

                    Session["ls_checkList_Result_s"] = ls_checkList_Result_s;
                    Session["lsCK"] = lsCK;

                    ViewBag.lsCK = lsCK;


                    //list is_SubCheck

                    var ls_ck_sub = lsCK.Where(s => s.subcheck == 1);

                    ViewBag.ls_ck_sub = ls_ck_sub;

                    //list required
                    var ls_ck_required = lsCK.Where(s => s.required == 1);

                    ViewBag.ls_ck_required = ls_ck_required;


                    return View();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult ItemInput()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        public async Task<ActionResult> ReceiveAcceptOrder(string dateFill, string dataFill)
        {
            try
            {

                if (Session["userRole"] == null)
                {
                    List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                    Session["userRole"] = bn;
                }

                List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];

                userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "SalesIn");

                Session["role_Item"] = role_Item;


                //get List
                if (dateFill == null) dateFill = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                if (dataFill == null) dataFill = "";


                var fr_lsRA = await DataAccess.sp_Picking_get_ReceiveAcceptOrder(User.Identity.GetUserId(), dateFill);

                Session["fr_lsRA"] = fr_lsRA;

                var lsRA = fr_lsRA.Where(s => s.vendorNo.Contains(dataFill) || s.vendorName.Contains(dataFill) || s.orderNo.Contains(dataFill)).OrderBy(s => s.status_Code).ThenBy(s=>s.vendorNo).ThenBy(s => s.orderNo);

                ViewBag.dateFill = dateFill;
                ViewBag.dataFill = dataFill;

                return View(lsRA);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        #region using for destop
        [HttpPost]
        public async Task<ActionResult> getListOrder(string orderNo)
        {
            try
            {
                List<saleIn_Item> its = (List<saleIn_Item>)Session["lsPurchase"];

                if(orderNo !=null && orderNo.Length > 0)
                {
                    var lis = its.Where(s => s.orderNo.ToLower().Contains(orderNo.ToLower()));

                    return Json(JsonConvert.SerializeObject(lis));
                }
                else
                {
                    return Json(JsonConvert.SerializeObject(its));
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getOrder_Header(string orderNo)
        {
            try
            {
                //List<saleIn_Item> its = (List<saleIn_Item>)Session["lsPurchase"];
                List<saleIn_Item> its = await DataAccess.sp_Picking_get_SaleIn(User.Identity.GetUserId(), Session["vendorNo"].ToString());
                var lis = its.FirstOrDefault(s => s.orderNo==orderNo);
                return Json(JsonConvert.SerializeObject(lis));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        public async Task<ActionResult> getOrder_Detail(string orderNo)
        {
            try
            {
                List<salesIn_Detail> its = await DataAccess.sp_Picking_get_SaleInDetail(User.Identity.GetUserId(), orderNo);
                Session["onEdit"] = its;

                return Json(JsonConvert.SerializeObject(its));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        #endregion

        [HttpPost]
        public async Task<ActionResult> getSubCheck(string code)
        {
            try
            {
                var lsCK = (List<CheckList_Item>)Session["lsCK"];

                var fnd = lsCK.FirstOrDefault(s => s.code == code);

                var bn = await DataAccess.sp_Picking_get_ChecklistSub(User.Identity.GetUserId(), fnd.codeCheck);

                return Json(JsonConvert.SerializeObject(bn));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getCheckResult(string orderNo)
        {
            try
            {
                var bn = await DataAccess.Picking_get_CheckListResult(User.Identity.GetUserId(), orderNo);
                return Json(JsonConvert.SerializeObject(bn));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> addSubCheck(string orderNo, string codeCheck, string subCodeCheck, string desc)
        {
            try
            {
                List<CheckList_Result_Item> ls_checkList_Result_s = (List<CheckList_Result_Item>)Session["ls_checkList_Result_s"];

                var found = ls_checkList_Result_s.FirstOrDefault(s => s.orderNo == orderNo && s.codeCheck == codeCheck);
                if(found.orderNo != null)
                {
                    found.subCheck = subCodeCheck;
                    found.is_subCheck = 1;
                    found.descriptions = desc;
                }

                Session["ls_checkList_Result_s"] = ls_checkList_Result_s;

                return Json("Done");
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> changedQuantity(string orderNo,string itemNo, int quantity)
        {
            try
            {
               List<salesIn_Detail> its = (List<salesIn_Detail>)Session["onEdit"];

                var found = its.FirstOrDefault(s => s.itemNo== itemNo);

                if (found.realQuantity != quantity)
                {
                    found.realQuantity = quantity;
                    found.isEdit = 1;

                    Session["onEdit"] = its;
                }
               
                return Json("Done");
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> saveCheckList(List<CheckList_Result_Item> its)
        {
            try
            {
                List<CheckList_Result_Item> ls_checkList_Result_s = (List<CheckList_Result_Item>)Session["ls_checkList_Result_s"];

                foreach(var i in its)
                {
                    if (i.is_codeCheck >0 )
                    {
                        var found = ls_checkList_Result_s.FirstOrDefault(s => s.orderNo == i.orderNo && s.codeCheck == i.codeCheck);
                        if(found.orderNo != null)
                        {
                            if (found.inputType == 1)
                            {
                                found.is_codeCheck = 1;
                            }
                            else
                            {
                                found.is_codeCheck = i.is_codeCheck;
                            }
                            
                        }
                    }
                }

                var bn = await DataAccess.sp_Picking_add_CheckListResult(ls_checkList_Result_s,User.Identity.GetUserId(),"");

                if (bn == true)
                {
                    return Json("Done");
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> rawSave()
        {
            try
            {
                List<salesIn_Detail> its = (List<salesIn_Detail>)Session["onEdit"];

                var dis = await DataAccess.sp_Picking_update_SalesIn(User.Identity.GetUserId(), its[0].orderNo, 1);
                if (dis == true)
                {
                    var bn = await DataAccess.sp_Picking_add_SalesIn(its, User.Identity.GetUserId(), 1);
                    return Json("Done");
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> AcceptOrder()
        {
            try
            {
                List<salesIn_Detail> its = (List<salesIn_Detail>)Session["onEdit"];

                var dis = await DataAccess.sp_Picking_update_SalesIn(User.Identity.GetUserId(), its[0].orderNo, 1);
                if (dis == true)
                {
                    var bn = await DataAccess.sp_Picking_add_SalesIn(its, User.Identity.GetUserId(), 2);
                    return Json("Done");
                }
                else
                {
                    return Json("");
                }

                
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getSalesInByDate(string dateFill)
        {
            try
            {
                var bn = await DataAccess.sp_Picking_get_SalesIn_byDate(User.Identity.GetUserId(), dateFill,"");
                Session["SalesIn_byDate"] = bn;
                return Json(JsonConvert.SerializeObject(bn.OrderBy(s=>s.ItemNo)));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> addSaleInByDate(string ordeNo, string item, int realquantity)
        {
            try
            {
                List<SalesIn_byDate> ls = (List<SalesIn_byDate>)Session["SalesIn_byDate"];
                var foud = ls.First(s => s.orderNo == ordeNo && s.ItemNo == item);
                if (foud != null)
                {
                    var bn = await DataAccess.sp_Picking_add_SalesIn(
                        new List<salesIn_Detail>
                        {
                            new salesIn_Detail{orderNo=foud.orderNo, itemNo=foud.ItemNo, orderQuantity=foud.quantity, realQuantity=realquantity}
                        },User.Identity.GetUserId(),1
                        );

                    if (bn == true)
                    {
                        return Json(bn.ToString());
                    }
                    else
                    {
                        return Json("");
                    }
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> acceptSaleInByDate()
        {
            try
            {
                List<SalesIn_byDate> ls = (List<SalesIn_byDate>)Session["SalesIn_byDate"];

                List<salesIn_Detail> its = new List<salesIn_Detail>();
                foreach (var i in ls)
                {
                    salesIn_Detail it = new salesIn_Detail
                    {
                        orderNo = i.orderNo,
                        itemNo = i.ItemNo,
                        orderQuantity = i.quantity,
                        realQuantity = i.realQuantity
                    };

                    its.Add(it);
                }

                var bn = await DataAccess.sp_Picking_add_SalesIn(its, User.Identity.GetUserId(), 2);
                return Json("Done");
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> addAcceptReceive(string str_v, string str_o, int str_s)
        {
            try
            {
                List <RA_Item> fr_lsRA = (List<RA_Item>)Session["fr_lsRA"];

                //Check Time
                int time = DateTime.Now.Hour;
                if(time<15 && time > 16)
                {
                    return Json(@"{""code"":""0"",""mess"":""Ngoài giờ xác nhận""}");
                }

                //get item
                var found = fr_lsRA.First(s => s.vendorNo == str_v & s.orderNo == str_o && s.status_Code != str_s);
                if (found != null)
                {
                    var dn = DateTime.Now.AddDays(1).ToString("yyyyMMdd");
                    if (found.shipDate == dn)
                    {
                        found.status_Code = str_s;
                        var rt = await DataAccess.sp_Picking_add_ReceiveAcceptOrder(User.Identity.GetUserId(), found);

                        return Json(@"{""code"":""1"",""mess"":""Done""}");
                    }
                    else
                    {
                        return Json(@"{""code"":""0"",""mess"":""Không thể duyệt đơn ngày "+found.shipDatetime+@" !""}");
                    }
                    
                }
                else
                {
                    return Json(@"{""code"":""0"",""mess"":""Không tìm thấy dữ liệu thỏa mãn ""}");
                }

            }
            catch (Exception ex)
            {
                return Json("");
            }
        }
    }
}