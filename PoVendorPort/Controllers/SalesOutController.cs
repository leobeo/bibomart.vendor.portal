﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoVendorPort.Controllers
{
    using PoVendorPortCommon;
    using Models;
    using System.Threading.Tasks;
    using System.Web.Caching;
    using Microsoft.AspNet.Identity;
    using PoVendorPortCommon.Models;
    using DataAccess;
    using PoVendorPortCommon.Models.Picking;
    using PagedList;
    using Newtonsoft.Json;
    using PoVendorPortCommon.Models.Picking.Packing;
    using PoVendorPortCommon.Models.Picking.Sales_v2;
    using PoVendorPortCommon.Models.Picking.SalesOut_v2;

    [Authorize]
    public class SalesOutController : Controller
    {
        public SalesOutController(IDataAccess da, ILogger logger)
        {
            DataAccess = da;
            Logger = logger;
        }

        public IDataAccess DataAccess { get; set; }
        public ILogger Logger { get; set; }

        // GET: SalesOut
        public async Task<ActionResult> Index(int? page, int? Size, string storeNo, int? sort)
        {

            try
            {

                if (Session["userRole"] == null)
                {
                    List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                    Session["userRole"] = bn;
                }

                List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];

                userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "SalesOut");

                Session["role_Item"] = role_Item;

                if (role_Item.typeGroup == 3)
                {
                    return RedirectToAction("ItemList");
                }
                else
                {
                    return RedirectToAction("StoreList");
                }

                if (page == null) page = 1;
                if (sort == null) sort = 0;

                List<saleOut_Item> its = await DataAccess.sp_Picking_get_SaleOut(User.Identity.GetUserId());

                Session["lsSalesOut"] = its;

                List<int> orderByStatus = new List<int> { 2, 1, 3, 4 };

                var listV = its.OrderBy(s => s.storeNo).OrderBy(s => orderByStatus.IndexOf(s.status_code));

                if (sort == 1)
                {
                    if (storeNo != null && storeNo.Length > 0)
                    {
                        listV = its.Where(s => s.storeNo.ToLower().Contains(storeNo.ToLower()) || s.storeName.ToLower().Contains(storeNo.ToLower()) || s.orderNo.ToLower().Contains(storeNo.ToLower())).OrderByDescending(s => s.storeNo).OrderBy(s => orderByStatus.IndexOf(s.status_code));
                    }
                    else
                    {
                        listV = its.OrderByDescending(s => s.storeNo).OrderBy(s => orderByStatus.IndexOf(s.status_code));
                    }
                }
                else
                {
                    if (storeNo != null && storeNo.Length > 0)
                    {
                        listV = its.Where(s => s.storeNo.ToLower().Contains(storeNo.ToLower()) || s.storeName.ToLower().Contains(storeNo.ToLower()) || s.orderNo.ToLower().Contains(storeNo.ToLower())).OrderBy(s => s.storeNo).OrderBy(s => orderByStatus.IndexOf(s.status_code));
                    }
                }

                int pageSize = (Size ?? 5);

                int pageNumber = (page ?? 1);

                int checkTotal = (int)(listV.Count() / pageSize) + 1;
                if (pageNumber > checkTotal) pageNumber = 1;

                ViewBag.pageSize = pageSize;
                ViewBag.storeNo = storeNo;
                ViewBag.sort = sort;

                return View(listV.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> Detail(string orderNo, int? page, int? Size, string item, int? sort)
        {
            try
            {


                if (orderNo == null || orderNo.Length == 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    //get purchase info

                    List<saleOut_Item> lsSalesOut = (List<saleOut_Item>)Session["lsSalesOut"];

                    var pc_info = lsSalesOut.First(s => s.orderNo == orderNo);

                    ViewBag.pc_info = pc_info;

                    //get purchase detail


                    if (page == null) page = 1;

                    if (sort == null) sort = 0;


                    //Session["onEdit_SalesOut"] = its;

                    List<salesOut_Detail> its = new List<salesOut_Detail>();

                    if (Session["onEdit_SalesOut"] != null && Session["orderNo_SalesOut"] != null && Session["orderNo_SalesOut"].ToString() == orderNo)
                    {
                        its = (List<salesOut_Detail>)Session["onEdit_SalesOut"];
                    }
                    else
                    {
                        //its = await DataAccess.sp_Picking_get_SaleInDetail(User.Identity.GetUserId(), orderNo);
                        its = await DataAccess.sp_Picking_get_SaleOutDetail(User.Identity.GetUserId(), orderNo);

                        Session["orderNo_SalesOut"] = orderNo;
                        Session["onEdit_SalesOut"] = its;
                    }

                    var listV = its.OrderBy(s => s.itemNo);


                    if (sort == 1)
                    {

                        if (item != null && item.Length > 0)
                        {
                            listV = its.Where(s => s.itemNo.Substring(0, item.Length) == item).OrderByDescending(s => s.itemNo);
                        }
                        else
                        {
                            listV = its.OrderByDescending(s => s.itemNo);
                        }
                    }
                    else
                    {
                        if (item != null && item.Length > 0)
                        {
                            listV = its.Where(s => s.itemNo.Substring(0, item.Length) == item).OrderBy(s => s.itemNo);
                        }
                    }


                    int pageSize = (Size ?? 5);


                    int pageNumber = (page ?? 1);

                    int checkTotal = (int)(listV.Count() / pageSize) + 1;
                    if (pageNumber > checkTotal) pageNumber = checkTotal;

                    ViewBag.pageSize = pageSize;
                    ViewBag.item = item;
                    ViewBag.orderNo = orderNo;
                    ViewBag.sort = sort;

                    Session["onEdit"] = listV;

                    return View(listV.ToPagedList(pageNumber, pageSize));
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult Packing(string orderNo)
        {
            try
            {

                if (orderNo == null || orderNo.Length == 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    //get purchase info

                    List<saleOut_Item> lsSalesOut = (List<saleOut_Item>)Session["lsSalesOut"];

                    var pc_info = lsSalesOut.FirstOrDefault(s => s.orderNo == orderNo);

                    ViewBag.pc_info = pc_info;

                    return View();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult ItemInput()
        {
            if (Session["userRole"] == null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public async Task<ActionResult> StoreList(int? page, int? Size, string dateFill, string storeNo, int? sort)
        {
            try
            {

                if (Session["userRole"] == null)
                {
                    List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                    Session["userRole"] = bn;
                }

                List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];

                userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "SalesOut");

                Session["role_Item"] = role_Item;


                if (page == null) page = 1;
                if (dateFill == null) dateFill = DateTime.Now.ToString("yyyy-MM-dd");
                if (storeNo == null) storeNo = "";
                if (sort == null) sort = 0;
                int pageSize = (Size ?? 5);
                int pageNumber = (page ?? 1);

                List<SalesOut_Header> its = await DataAccess.sp_Picking_get_SaleOut_v2(User.Identity.GetUserId(), dateFill);

                var ls_Store = its.OrderBy(s => s.storeNo);

                if (sort == 0)
                {
                    ls_Store = its.Where(s => s.storeNo.Substring(0, storeNo.Length) == storeNo).OrderBy(s => s.storeNo);
                }
                else
                {
                    ls_Store = its.Where(s => s.storeNo.Substring(0, storeNo.Length) == storeNo).OrderByDescending(s => s.storeNo);
                }


                //List<TO_ItemTotal> its = await DataAccess.sp_get_TO_ItemTotal(User.Identity.GetUserId(), "2019-10-22", "0", "");

                Session["ls_TODetail_header"] = its;

                int checkTotal = (int)(ls_Store.Count() / pageSize) + 1;
                if (pageNumber > checkTotal) pageNumber = 1;

                ViewBag.pageSize = pageSize;
                ViewBag.dateFill = dateFill;
                ViewBag.storeNo = storeNo;
                ViewBag.sort = sort;

                return View(ls_Store.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> SalesDetail(string dateFill, string storeNo)
        {
            try
            {
                if (dateFill.Length > 0 && storeNo.Length > 0)
                {

                    //get data header

                    List<SalesOut_Header> lsStore = (List<SalesOut_Header>)Session["lsStore"];


                    var bn = await DataAccess.sp_Picking_get_SalesOut_Detail_v2(User.Identity.GetUserId(), dateFill, storeNo);
                    //data for View
                    ViewBag.lsItem = bn;


                    //load role
                    userRole_Item role_Item = (userRole_Item)Session["role_Item"];

                    //Save button actived
                    var ls = bn.FirstOrDefault(s => s.realQuantity != s.orderQuantity || s.status >= role_Item.level_m - 3);
                    if (ls == null)
                    {
                        ViewBag.isSave = 0;
                    }
                    else
                    {
                        ViewBag.isSave = 1;
                    }


                    return View();
                }
                else
                {
                    return RedirectToAction("StoreList");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("StoreList");
            }
        }

        public async Task<ActionResult> ItemList(int? page, int? Size, string dateFill, string typeFill, string dataFill, string Router)
        {

            try
            {

                if (Session["userRole"] == null)
                {
                    List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                    Session["userRole"] = bn;
                }

                List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];

                userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "SalesOut");

                Session["role_Item"] = role_Item;


                var ls_r = await DataAccess.sp_Picking_get_RouterType();
                SelectList lsRouter = new SelectList(ls_r, "Router", "Router");

                ViewBag.lsRouter = lsRouter;


                if (page == null) page = 1;
                if (dateFill == null) dateFill = DateTime.Now.ToString("yyyy-MM-dd");
                if (typeFill == null) typeFill = "1";
                if (dataFill == null) dataFill = "";
                if (Router == null) Router = ls_r[0].Router;
                int pageSize = (Size ?? 5);
                int pageNumber = (page ?? 1);

                List<TO_ItemTotal> its=new List<TO_ItemTotal>();

                if (dataFill == null || dataFill.Length == 0)
                {
                    its = await DataAccess.sp_get_TO_ItemTotal(User.Identity.GetUserId(), dateFill, "0", "", Router);
                }
                else
                {
                    its = await DataAccess.sp_get_TO_ItemTotal(User.Identity.GetUserId(), dateFill, typeFill, dataFill, Router);
                }

                //List<TO_ItemTotal> its = await DataAccess.sp_get_TO_ItemTotal(User.Identity.GetUserId(), "2019-10-22", "0", "");

                //count total item
                decimal total = 0;
                foreach (var k in its)
                {
                    total += k.quantity;
                }

                ViewBag.total = (int)total;
                Session["ls_TOItemTotal"] = its;

                var listV = its.OrderBy(s => s.itemNo).OrderBy(s => s.is_Done);

                int checkTotal = (int)(listV.Count() / pageSize) + 1;
                if (pageNumber > checkTotal) pageNumber = 1;

                ViewBag.pageSize = pageSize;
                ViewBag.dateFill = dateFill;
                ViewBag.typeFill = typeFill;
                ViewBag.dataFill = dataFill;
                ViewBag.Router = Router;
                ViewBag.pageNumber = pageNumber;

                return View(listV.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> StoreItem(int? page, int? Size, string dateFill, string typeFill, string dataFill, string Router, string fillStore, string fillItem)
        {

            try
            {

                if (Session["userRole"] == null)
                {
                    List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                    Session["userRole"] = bn;
                }

                List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];

                userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "SalesOut");

                Session["role_Item"] = role_Item;

                var ls_r = await DataAccess.sp_Picking_get_RouterType();
                SelectList lsRouter = new SelectList(ls_r, "Router", "Router");

                ViewBag.lsRouter = lsRouter;

                //gen body

                if (page == null) page = 1;
                if (dateFill == null) dateFill = DateTime.Now.ToString("yyyy-MM-dd");
                if (typeFill == null) typeFill = "1";
                if (dataFill == null) dataFill = "";
                if (Router == null) Router = ls_r[0].Router;
                int pageSize = (Size ?? 5);
                int pageNumber = (page ?? 1);


                List<SalesOut_Line> its = new List<SalesOut_Line>();

                //new 20191111
                if (fillStore == null) fillStore = "0";
                if (fillItem == null) fillItem = "0";

                if (dataFill == null || dataFill.Length == 0)
                {
                    //its = await DataAccess.sp_Picking_get_TO_Detail_Line(User.Identity.GetUserId(), dateFill, "0", "", Router);
                }
                else
                {
                    its = await DataAccess.sp_Picking_get_TO_Detail_Line(User.Identity.GetUserId(), dateFill, typeFill, dataFill, Router);
                }

                Session["ls_TODetail"] = its;

                var cas = 0;

                var listV = its.OrderBy(s => s.status);

                if(Session["fillItem"]!=null && Session["fillItem"].ToString() != fillItem)
                {
                    cas = 1;
                    Session["colF"] = "1";
                }
                else if(Session["fillStore"] != null && Session["fillStore"].ToString() != fillStore)
                {
                    cas = 2;
                    Session["colF"] = "2";
                }
                else
                {
                    cas = 3;
                    //Session["colF"] = "0";
                }

                if (fillItem == "1") cas = 1;
                if (fillStore == "1") cas = 2;
                if (Session["colF"] != null && Session["colF"].ToString() == "1") cas = 1;
                if (Session["colF"] != null && Session["colF"].ToString() == "2") cas = 2;

                switch (cas)
                {
                    case 1:
                        if (fillItem == "1")
                        {
                            listV = its.OrderBy(s => s.status).ThenByDescending(s => s.itemNo).ThenBy(s=>s.storeLayout);
                        }
                        else
                        {
                            listV = its.OrderBy(s => s.status).ThenBy(s => s.itemNo).ThenBy(s => s.storeLayout);
                        }

                        fillStore = "0";
                        break;
                    case 2:
                        if (fillStore == "1")
                        {
                            listV = its.OrderBy(s => s.status).ThenByDescending(s => s.storeLayout).ThenBy(s=>s.itemNo);
                        }
                        else
                        {
                            listV = its.OrderBy(s => s.status).ThenBy(s => s.storeLayout).ThenBy(s => s.itemNo);
                        }

                        fillItem = "0";
                        break;
                    default :
                        listV = its.OrderBy(s => s.status).ThenBy(s => s.itemNo).ThenBy(s => s.storeLayout);
                        break;
                }

                //var listV = its.OrderBy(s => s.status).ThenBy(s => s.itemNo).ThenBy(s => s.storeLayout);

                //if (fillStore == "1")
                //{
                //    if (fillItem == "1")
                //    {
                //        listV = its.OrderBy(s => s.status).ThenByDescending(s => s.itemNo).ThenByDescending(s => s.storeLayout);
                //    }
                //    else
                //    {
                //        listV = its.OrderBy(s => s.status).ThenBy(s=>s.itemNo) .ThenByDescending(s => s.storeLayout);
                //    }

                //}
                //else
                //{
                //    if (fillItem == "1")
                //    {
                //        listV = its.OrderBy(s => s.status).ThenByDescending(s => s.itemNo).ThenBy(s=>s.storeLayout);
                //    }
                //    else
                //    {
                //        //listV = its.OrderBy(s => s.status);
                //    }
                //}

                //get acceptPacking

                var is_Accept = 1;

                foreach (var i in listV)
                {
                    if (i.status != role_Item.level_m - 4)
                    {
                        is_Accept = 0;
                    }
                }

                ViewBag.is_Accept = is_Accept;

                int checkTotal = (int)(listV.Count() / pageSize) + 1;
                if (pageNumber > checkTotal) pageNumber = 1;

                ViewBag.pageSize = pageSize;
                ViewBag.dateFill = dateFill;
                ViewBag.typeFill = typeFill;
                ViewBag.dataFill = dataFill;
                ViewBag.Router = Router;

                ViewBag.fillStore = fillStore;
                Session["fillStore"] = fillStore;

                ViewBag.fillItem = fillItem;
                Session["fillItem"] = fillItem;

                ViewBag.pageNumber = pageNumber;

                return View(listV.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> subPacking(string storeNo,string dateFill)
        {

            try
            {

                if (Session["userRole"] == null)
                {
                    List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                    Session["userRole"] = bn;
                }

                List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];

                userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "SalesOut");

                Session["role_Item"] = role_Item;


                //get SalesOut_Header
                List<SalesOut_Header> ls_TODetail = (List<SalesOut_Header>)Session["ls_TODetail_header"];

                var it_Header = ls_TODetail.First(s => s.storeNo == storeNo);
                if (it_Header == null || it_Header.is_Packing==1)
                {
                    return RedirectToAction("Index");
                }

                ViewBag.header_info = it_Header;

                //get list boxType

                var ls_Box = await DataAccess.sp_Picking_get_BoxType(User.Identity.GetUserId());

                if(dateFill==null) dateFill= DateTime.Now.ToString("yyyy-MM-dd");

                ViewBag.dateFill = dateFill;


                return View(ls_Box);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> AcceptPacking(string dateFill,string storeNo,string item, int? page, int? Size)
        {
            try
            {

                if (Session["userRole"] == null)
                {
                    List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                    Session["userRole"] = bn;
                }

                List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];

                userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "SalesOut");

                Session["role_Item"] = role_Item;


                if (page == null) page = 1;
                if (dateFill == null) dateFill = DateTime.Now.ToString("yyyy-MM-dd");
                if (item == null) item = "";
                int pageSize = (Size ?? 5);
                int pageNumber = (page ?? 1);

                //get header
                List<SalesOut_Header> its = await DataAccess.sp_Picking_get_SaleOut_v2(User.Identity.GetUserId(), dateFill);

                var dt_header = its.First(s => s.storeNo == storeNo);

                if (dt_header == null)
                {
                    return RedirectToAction("Index");
                }

                ViewBag.dt_header = dt_header;

                //genBody
                var its_body = await DataAccess.sp_Picking_get_SalesOut_Detail_v2(User.Identity.GetUserId(), dateFill, storeNo);

                var ls_body=its_body.OrderBy(s=>s.itemNo).OrderBy(s=>s.orderNo);

                if (item.Length >0 )
                {
                    ls_body = its_body.Where(s => s.itemNo.Contains(item)).OrderBy(s => s.orderNo);
                }

                Session["salesOut_Accept"] = its_body;

                var tt_min=its_body.Min(s => s.status);

                ViewBag.tt_min = tt_min;



                int checkTotal = (int)(ls_body.Count() / pageSize) + 1;
                if (pageNumber > checkTotal) pageNumber = 1;

                ViewBag.pageSize = pageSize;
                ViewBag.dateFill = dateFill;
                ViewBag.storeNo = storeNo;
                ViewBag.item = item;

                return View(ls_body.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> AcceptItemWait(string dateFill, string typeFill, string dataFill, int? page, int? Size)
        {
            try
            {

                if (Session["userRole"] == null)
                {
                    List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                    Session["userRole"] = bn;
                }

                List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];

                userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "SalesOut");

                Session["role_Item"] = role_Item;


                if (page == null) page = 1;
                if (dateFill == null) dateFill = DateTime.Now.ToString("yyyy-MM-dd");
                if (typeFill == null) typeFill = "1";
                if (dataFill == null) dataFill = "";
                int pageSize = (Size ?? 5);
                int pageNumber = (page ?? 1);

                //genBody

                List<ItemWait> its_body = new List<ItemWait>();

                if (dataFill==null || dataFill.Length == 0)
                {
                    its_body = await DataAccess.sp_Picking_get_AcceptItemWait(User.Identity.GetUserId(), dateFill, "0", "");
                }
                else
                {
                    its_body = await DataAccess.sp_Picking_get_AcceptItemWait(User.Identity.GetUserId(), dateFill, typeFill, dataFill);
                }

                var ls_body = its_body.OrderBy(s => s.itemNo).OrderBy(s => s.orderNo);

                Session["ls_ItemWait"] = its_body;


                int checkTotal = (int)(ls_body.Count() / pageSize) + 1;
                if (pageNumber > checkTotal) pageNumber = 1;

                ViewBag.pageSize = pageSize;
                ViewBag.dateFill = dateFill;
                ViewBag.typeFill = typeFill;
                ViewBag.dataFill = dataFill;

                return View(ls_body.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }


        #region using for destop
        [HttpPost]
        public async Task<ActionResult> getListOrder(string orderNo)
        {
            try
            {
                //List<saleOut_Item> its = await DataAccess.sp_Picking_get_SaleOut(User.Identity.GetUserId());
                //Session["lsSaleOut"] = its;

                List<saleOut_Item> its = (List<saleOut_Item>)Session["lsSaleOut"];

                if (orderNo != null && orderNo.Length > 0)
                {
                    var lis = its.Where(s => s.orderNo.ToLower().Equals(orderNo.ToLower()));

                    return Json(JsonConvert.SerializeObject(lis));
                }
                else
                {
                    return Json(JsonConvert.SerializeObject(its));
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getOrder_Header(string orderNo)
        {
            try
            {
                //List<saleOut_Item> its = (List<saleOut_Item>)Session["lsSaleOut"];
                List<saleOut_Item> its = await DataAccess.sp_Picking_get_SaleOut(User.Identity.GetUserId());

                var lis = its.FirstOrDefault(s => s.orderNo == orderNo);
                return Json(JsonConvert.SerializeObject(lis));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getOrder_Detail(string orderNo)
        {
            try
            {
                List<salesOut_Detail> its = await DataAccess.sp_Picking_get_SaleOutDetail(User.Identity.GetUserId(), orderNo);
                Session["onEdit_SalesOut"] = its;

                return Json(JsonConvert.SerializeObject(its));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getPacking(string storeNo, string dateFill)
        {
            try
            {
                var its = await DataAccess.sp_Picking_get_Packing_Line(User.Identity.GetUserId(), storeNo, dateFill);

                return Json(JsonConvert.SerializeObject(its));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getLsStore(string dateFill)
        {
            try
            {
                var bn = await DataAccess.sp_Picking_get_SaleOut_v2(User.Identity.GetUserId(), dateFill);
                Session["lsStore"] = bn;
                return Json(JsonConvert.SerializeObject(bn));
            }
            catch (Exception)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getSalesOutByStore(string dateFill, string storeNo)
        {
            try
            {
                var bn = await DataAccess.sp_Picking_get_SalesOut_Detail_v2(User.Identity.GetUserId(), dateFill, storeNo);

                return Json(JsonConvert.SerializeObject(bn));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }
        #endregion

        [HttpPost]
        public async Task<ActionResult> changedQuantity(string orderNo, string itemNo, int quantity)
        {
            try
            {
                List<salesOut_Detail> its = (List<salesOut_Detail>)Session["onEdit_SalesOut"];

                var found = its.FirstOrDefault(s => s.itemNo == itemNo);

                if (found.realQuantity != quantity)
                {
                    found.realQuantity = quantity;
                    found.isEdit = 1;

                    Session["onEdit_SalesOut"] = its;
                }

                return Json("Done");
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> rawSave()
        {
            try
            {
                List<salesOut_Detail> its = (List<salesOut_Detail>)Session["onEdit_SalesOut"];

                var dis = await DataAccess.sp_Picking_update_SalesOut(User.Identity.GetUserId(), its[0].orderNo, 1);
                if (dis == true)
                {
                    var bn = await DataAccess.sp_Picking_add_SalesOut(its, User.Identity.GetUserId(), 1);
                    return Json("Done");
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> AcceptOrder()
        {
            try
            {
                List<salesOut_Detail> its = (List<salesOut_Detail>)Session["onEdit_SalesOut"];

                var dis = await DataAccess.sp_Picking_update_SalesOut(User.Identity.GetUserId(), its[0].orderNo, 1);
                if (dis == true)
                {
                    var bn = await DataAccess.sp_Picking_add_SalesOut(its, User.Identity.GetUserId(), 2);
                    return Json("Done");
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> savePacking(List<packingLine_Item> its)
        {
            try
            {
                //get core vol
                var str_fill = its[0].orderNo;

                var ls_vol = await DataAccess.sp_Picking_get_VolumeByItem(User.Identity.GetUserId(), str_fill.Split('/')[1], str_fill.Split('/')[0]);

                decimal core_vol = 0;
                foreach(var i in ls_vol)
                {
                    core_vol += i.volume;
                }

                //cal vol by input
                decimal cal_vol = 0;
                foreach(var i in its)
                {
                    cal_vol += i.packTotal * i.coreVolume;
                }

                decimal diff = (Math.Abs(cal_vol - core_vol) / core_vol) / 100;

                if ( diff > 5)
                {
                    return Json(@"{""code"":""0"",""mess"":""lệch quá 5%""}");
                }


                var bn = await DataAccess.sp_Picking_add_PackingLine(its, User.Identity.GetUserId());

                if (bn == true)
                {
                    return Json(@"{""code"":""1"",""mess"":""Done""}");
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json(@"{""code"":""0"",""mess"":""Có lỗi sảy ra thử l""}");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getSaleOutByDate(string dateFill, string itemNo, string typeFill)
        {
            try
            {
                var bn = await DataAccess.sp_Picking_get_SalesOut_byDate(User.Identity.GetUserId(), dateFill, itemNo, typeFill);
                Session["SalesOut_byDate"] = bn;
                return Json(JsonConvert.SerializeObject(bn.OrderBy(s => s.orderNo).OrderBy(s => s.status)));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> addSalesOut_byDate(string orderNo, string itemNo, int realQuantity)
        {
            try
            {
                List<SalesOut_byDate> ls = (List<SalesOut_byDate>)Session["SalesOut_byDate"];

                var foud = ls.First(s => s.orderNo == orderNo && s.itemNo == itemNo);
                if (foud != null)
                {
                    var bn = await DataAccess.sp_Picking_add_SalesOut(
                        new List<salesOut_Detail>
                        {
                            new salesOut_Detail{orderNo=foud.orderNo, itemNo=foud.itemNo, orderQuantity=foud.orderQuantity, realQuantity=realQuantity}
                        }, User.Identity.GetUserId(), 1
                        );

                    if (bn == true)
                    {
                        return Json(bn.ToString());
                    }
                    else
                    {
                        return Json("");
                    }
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> acceptSalesOut_byDate()
        {
            try
            {
                List<SalesOut_byDate> ls = (List<SalesOut_byDate>)Session["SalesOut_byDate"];

                List<salesOut_Detail> its = new List<salesOut_Detail>();
                foreach (var i in ls)
                {
                    salesOut_Detail it = new salesOut_Detail
                    {
                        orderNo = i.orderNo,
                        itemNo = i.itemNo,
                        orderQuantity = i.orderQuantity,
                        realQuantity = i.realQuantity
                    };

                    its.Add(it);
                }

                var bn = await DataAccess.sp_Picking_add_SalesOut(its, User.Identity.GetUserId(), 2);
                return Json("Done");
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> save_TOItem(string dateFill, string itemNo, int is_Done)
        {
            try
            {
                List<TO_ItemTotal> ls_TOItemTotal = (List<TO_ItemTotal>)Session["ls_TOItemTotal"];
                var found = ls_TOItemTotal.First(s => s.itemNo == itemNo);
                if (found != null)
                {
                    found.is_Done = is_Done;
                    var bn = await DataAccess.sp_Picking_add_TO_ItemTotal_Line(User.Identity.GetUserId(), found);
                    return Json(bn.ToString());
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> save_TODetail(string itemNo, string storeNo,string orderNo , int typeStatus = 0)
        {
            try
            {
                List<SalesOut_Line> ls_TODetail = (List<SalesOut_Line>)Session["ls_TODetail"];

                var foud = ls_TODetail.First(s => s.storeNo == storeNo && s.itemNo == itemNo && s.orderNo== orderNo);

                if (foud != null)
                {
                    if (typeStatus == 0) foud.realQuantity = 0;
                    var bn = await DataAccess.sp_Picking_add_SalesOut(
                        new List<salesOut_Detail>
                        {
                            new salesOut_Detail{orderNo=foud.orderNo, itemNo=foud.itemNo, orderQuantity=foud.orderQuantity, realQuantity=foud.realQuantity}
                        }, User.Identity.GetUserId(), typeStatus + 1
                        );

                    if (bn == true)
                    {
                        return Json(bn.ToString());
                    }
                    else
                    {
                        return Json("");
                    }
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> getItemStoreLeast(string dateFill, string storeNo)
        {
            try
            {
                var bn = await DataAccess.sp_Picking_get_ItemForStore_least(User.Identity.GetUserId(), dateFill, storeNo);
                return Json(JsonConvert.SerializeObject(bn));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> acceptSL_Packing()
        {
            try
            {
                List<SalesOut_Line> salesOut_Accept = (List<SalesOut_Line>)Session["salesOut_Accept"];

                List<salesOut_Detail> its = new List<salesOut_Detail>();

                foreach(var i in salesOut_Accept)
                {
                    salesOut_Detail it = new salesOut_Detail
                    {
                        orderNo = i.orderNo,
                        itemNo=i.itemNo,
                        orderQuantity=i.orderQuantity,
                        realQuantity=i.realQuantity,
                        corePrice=i.corePrice
                    };

                    its.Add(it);
                }
                
                var bn = await DataAccess.sp_Picking_add_SalesOut(its, User.Identity.GetUserId(), 2);

                return Json("Done");
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> addSalesOut_ItemWait(string orderNo, string itemNo, int typeStatus)
        {
            try
            {
                List<ItemWait> ls = (List<ItemWait>)Session["ls_ItemWait"];

                var foud = ls.First(s => s.orderNo == orderNo && s.itemNo == itemNo);
                if (foud != null)
                {
                    var bn = await DataAccess.sp_Picking_add_SalesOut(
                        new List<salesOut_Detail>
                        {
                            new salesOut_Detail{orderNo=foud.orderNo, itemNo=foud.itemNo, orderQuantity=foud.quantity, realQuantity=0}
                        }, User.Identity.GetUserId(), typeStatus
                        );

                    if (bn == true)
                    {
                        return Json(bn.ToString());
                    }
                    else
                    {
                        return Json("");
                    }
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }
    }
}