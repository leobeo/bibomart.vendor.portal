﻿using Microsoft.AspNet.Identity;
using PoVendorPortCommon;
using PoVendorPortCommon.Models.Picking;
using PoVendorPortCommon.Models.Picking.Invoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace PoVendorPort.Controllers
{
    [Authorize]
    public class InvoiceController : Controller
    {
        public InvoiceController(IDataAccess da, ILogger logger)
        {
            DataAccess = da;
            Logger = logger;
        }

        public IDataAccess DataAccess { get; set; }
        public ILogger Logger { get; set; }

        // GET: Invoice
        public async Task<ActionResult> Index()
        {
            if (Session["userRole"] == null)
            {
                List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserId());
                Session["userRole"] = bn;
            }

            List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];


            userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "Invoice");

            Session["role_Item"] = role_Item;


            var ls_iv = await DataAccess.sp_Picking_get_Invoice_Item(User.Identity.GetUserId());
            ViewBag.ls_iv = ls_iv;
            return View();
        }

        public async Task<ActionResult> Detail(string invoiceNo)
        {
            try
            {
                if(invoiceNo !=null && invoiceNo.Length > 0)
                {
                    var ls_iv = await DataAccess.sp_Picking_get_Invoice_Item(User.Identity.GetUserId());

                    var invoiceHeader = ls_iv.FirstOrDefault(s => s.invoiceNo == invoiceNo);

                    ViewBag.invoiceHeader = invoiceHeader;


                    var ls_invoiceDetail = await DataAccess.sp_Picking_get_Invoice_ItemDetail(User.Identity.GetUserId(), invoiceNo);
                    ViewBag.ls_invoiceDetail = ls_invoiceDetail;
                    Session["onEdit"] = ls_invoiceDetail;

                    return View();
                }
                else
                {
                    return RedirectToAction("VendorList");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("VendorList");
            }
        }

        public async Task<ActionResult> VendorList()
        {
            var ls_vd = await DataAccess.sp_Picking_get_Invoice_lsVendor(User.Identity.GetUserId());
            ViewBag.ls_vd = ls_vd;
            return View();
        }

        public async Task<ActionResult> OrderList(string vendorNo)
        {
            if(vendorNo !=null && vendorNo.Length > 0)
            {
                try
                {
                    var lsOrder = await DataAccess.sp_Picking_get_Invoice_lsOrder(User.Identity.GetUserId(), vendorNo);
                    ViewBag.lsOrder = lsOrder;
                    return View();
                }
                catch (Exception ex)
                {
                    return RedirectToAction("VendorList");
                }
            }
            else
            {
                return RedirectToAction("VendorList");
            }
        }

        public async Task<ActionResult> getInvoiceHeader()
        {
            //sp_Picking_add_InvoiceHeader
            try
            {
                var bn = await DataAccess.sp_Picking_add_InvoiceHeader(User.Identity.GetUserId());
                return Json(bn);
            }
            catch (Exception)
            {
                return Json("");
            }
        }

        #region dataAction

        [HttpPost]
        public async Task<ActionResult> genHeader()
        {
            try
            {
                var bn = await DataAccess.sp_Picking_add_InvoiceHeader(User.Identity.GetUserId());
                return Json(bn);
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> genLine(List<invoice_Detail> it)
        {
            try
            {
                var bn = await DataAccess.sp_Picking_add_InvoiceLine(it, User.Identity.GetUserId(), 0);
                if (bn == true)
                {
                    return Json("Done");
                }
                else
                {
                    return Json("");
                }
                

            }catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> changedDiscount(string orderNo, string itemNo, decimal discount)
        {
            try
            {
                List<invoice_Detail> ls_invoiceDetail = (List<invoice_Detail>)Session["onEdit"];

                var found = ls_invoiceDetail.FirstOrDefault(s => s.orderNo == orderNo && s.itemNo == itemNo);
                if (found.discount != discount)
                {
                    found.discount = discount;
                    found.isEdit = 1;

                    Session["onEdit"] = ls_invoiceDetail;
                }

              return Json("Done");
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        public async Task<ActionResult> saveRaw(string invoiceNo)
        {
            try
            {
                List<invoice_Detail> ls_invoiceDetail = (List<invoice_Detail>)Session["onEdit"];

                var bn = await DataAccess.sp_Picking_add_InvoiceLine(ls_invoiceDetail, User.Identity.GetUserId(), 1);

                if (bn == true)
                {
                    return Json("Done");
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex )
            {
                return Json("");
            }
        }

        public async Task<ActionResult> saveAccept(string invoiceNo)
        {
            try
            {
                List<invoice_Detail> ls_invoiceDetail = (List<invoice_Detail>)Session["onEdit"];

                //var bn = await DataAccess.sp_Picking_add_InvoiceLine(ls_invoiceDetail, User.Identity.GetUserId(), 2);

                var bn = await DataAccess.sp_Picking_update_InvoiceHeader(User.Identity.GetUserId(), invoiceNo, 2);

                if (bn == true)
                {
                    return Json("Done");
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }
        #endregion
    }
}