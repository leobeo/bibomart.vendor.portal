﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoVendorPort.Controllers
{
    using PoVendorPortCommon;
    using Models;
    using System.Threading.Tasks;
    using System.Web.Caching;
    using Microsoft.AspNet.Identity;
    using PoVendorPortCommon.Models;
    using DataAccess;
    
    [Authorize]
    public class HomeController : Controller
    {
        public HomeController(IDataAccess da, ILogger logger)
        {
            DataAccess = da;
            Logger = logger;
        }
        const string ReadonlyPoMessage = "Đơn hàng đang ở trạng thái chỉ xem, bạn không thể sửa đổi được";
        public IDataAccess DataAccess { get; set; }
        public ILogger Logger { get; set; }
        public async Task<ActionResult> Index()
        {
            var ui = await DataAccess.GetUserInfo(User.Identity.GetUserId());
            if(ui.IsManager || ui.IsAdmin)
            {
                return RedirectToAction(nameof(ManageController.ControlPanel), "Manage");
            } 
            return await _indexForVendor();            
        }
        async Task<ActionResult> _indexForVendor()
        {
            var vm = new HomeViewModel();
            await vm.Load(DataAccess);
            if (!vm.LoggedInVendor.PurchaseOrders.Any()) return View("NoPo");
            return View(vm);
        }
        
        public async Task<ActionResult> ViewApprovedPurchaseOrdersReport()
        {
            var vm = new ApprovedPurchaseOrdersViewModel();
            vm.FromDate = vm.ToDate = DateTime.Now;
            var vendor = await DataAccess.GetVendorInfoAsync(User.Identity.GetUserId());            
            var pos = await DataAccess.LoadPurchaseOrdersByVendorId(vendor.No);
            vm.ApprovedPurchaseOrders = pos.Where(e => e.Status > 0).ToList();
            return View("ApprovedPurchaseOrdersReport", vm);
        }
        public async Task<ActionResult> LoadPurchaseItems(string poNo)
        {
            PurchaseOrder po = null;
            try
            {
                var vendor = await DataAccess.GetVendorInfoAsync(User.Identity.GetUserId());
                if (vendor == null || vendor.No == null) return new EmptyResult();
                var pos = await DataAccess.LoadPurchaseOrdersByVendorId(vendor.No);
                po = pos.FirstOrDefault(e => e.PoNo == poNo);
                //await Task.Delay(1000);
                if(po == null)
                {
                    Logger.Write(string.Format("{0} - Cound not find PO: {2} of the vendorNo: {1}", DateTime.Now, vendor.No, poNo));
                } else if((po.VendorNo ?? "").Trim() == "" || (po.PoNo ?? "").Trim() == "")
                {
                    Logger.Write(string.Format("{0} - Invalid fetched data for PO: {1}, the currentVendorNo: {2}, the associated vendorNo: {3}, poNo: {4}", DateTime.Now, poNo, vendor.No, po.VendorNo, po.PoNo));
                } else
                {
                    //load Note separately
                    po.Note = await DataAccess.GetPoNote(po.PoNo);
                }
            }
            catch
            {
                return PartialView("_LoadPurchaseItemsErrorPartial");
            }                            
            return PartialView("_PurchaseItemsListPartial", po);
        }
        /// <summary>
        /// This is for AJAX request only to refresh the newly updated data (purchase orders of the caller client have been changed)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> LoadPurchaseOrders()
        {
            if (!Request.IsAjaxRequest()) return new EmptyResult();
            try {
                var vi = await DataAccess.GetVendorInfoAsync(User.Identity.Name);
                var pos = await DataAccess.LoadPurchaseOrdersByVendorId(vi.No);
                vi.PurchaseOrders = pos;                               
                return PartialView("_PoListPartialView", vi);
            } catch(Exception ex)
            {
                Logger.Write(string.Format("{0} - AJAX LoadPurchaseOrders error: {1}, Authorized username: {2}", DateTime.Now, ex, User.Identity.Name));
                return null;
            }
        }
        [HttpPost]
        public async Task<ActionResult> UpdatePO(PurchaseOrder po)
        {            
            var msg = "";            
            var vendorInfo = await DataAccess.GetVendorInfoAsync(User.Identity.GetUserId());
            var isOfCurrentVendor = vendorInfo.No == po.VendorNo;
            if (isOfCurrentVendor && ModelState.IsValid)
            {
                try
                {
                    var isPoEditable = await DataAccess.CheckIfPoEditable(po.PoNo);
                    if (isPoEditable)
                    {
                        //await Task.Delay(3000);                
                        var ok = await DataAccess.UpdatePurchaseOrder(po);
                        if (ok)
                        {
                            //NOTE: This LastApprovedDate is updated by the db server (using SQL), not a value from the ASP.NET server (running this app).
                            //To actually refresh this date with the new saved data, we have to query back this value - this may cost some unnessary connection.
                            //So we just update the date here (it may be a bit different from the actually saved date).
                            //The newly saved date will be obtained only when the cache is expired.
                            po.LastApprovedDate = DateTime.Now;
                            po.Status = 1;
                            DataCache.UpdateCachedPo(po);
                            return Content("ok");
                        }
                    } else
                    {
                        msg = ReadonlyPoMessage;
                    }
                } catch
                {
                    msg = "Có lỗi lưu sửa đổi không mong muốn. Xin hãy thử lại sau";
                }
            } else //return a composite string message (like validation summary) to the client to parse and show.
            {      //Note that the ValidationMessage for each property is added in the result partial view (if returned).
                   //So unless the partial view is rendered, the user won't be able to see those messages. That's why we use a composite message here to 
                   //show in just one place.
                   //The problem is the partial view is not rendered on the client side, it just waits for success response 
                   //to continue going to the next page (for the next po).
                   //
                   //There are 2 approaches:
                   //- Explicitly add error status code to the response so that the OnSuccess won't be fired (instead only the OnComplete).
                   //  The client code then can handle the error with the response message updated to the element having id specified via UpdateTargetId
                   //- Let the response seem to be successfull normally, but the response is returned with a JSON containing about the error, in fact any data can be fine
                   //  The point is how to help the client know that there is something wrong right inside the OnSuccess callback to handle accordingly.
                if (!isOfCurrentVendor)
                {
                    if (string.IsNullOrEmpty(po.PoNo))
                    {
                        msg = "Không nhận được dữ liệu gửi lên, xin hãy thử tải lại trang (F5) và thực hiện lại.";
                    }
                    else
                    {
                        msg = "Đơn hàng duyệt không thuộc vendor hiện tại.";
                    }
                    Logger.Write(string.Format("{0} - PurchaseOrder tampered with mismatched VendorNo, current vendor no: {1}, submitted vendor no: {2}, submitted po no: {3}, full_JSON: {4}", DateTime.Now, vendorInfo.No, po.VendorNo, po.PoNo, Newtonsoft.Json.JsonConvert.SerializeObject(po)));
                }
                else
                {
                    //collect model's error
                    msg = string.Join(", ", new HashSet<string>(ModelState.Values.SelectMany(e => e.Errors)
                                                .Where(e => !string.IsNullOrEmpty(e.ErrorMessage))
                                                .Select(e => e.ErrorMessage)));
                }
            }
            return Content(msg);
        }
        [HttpPost]
        public async Task<ActionResult> UpdatePoNote(PurchaseOrder po)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var isPoEditable = await DataAccess.CheckIfPoEditable(po.PoNo);
                    if (isPoEditable)
                    {
                        await DataAccess.UpdatePoNote(po);
                        return new EmptyResult();
                    } else
                    {
                        return Content(ReadonlyPoMessage);
                    }
                } catch
                {
                    //Logger.Write($"{DateTime.Now} - {nameof(UpdatePoNote)}: Could not save PO Note, po: {po.PoNo}, note: {po.Note}, Exception: {ex}");
                    return Content("Có lỗi không mong muốn, xin thử lại sau.");
                }
            }
            return Content("Dữ liệu không hợp lệ");
        }
        public async Task<FileStreamResult> ExportPurchaseOrders()
        {
            var ui = await DataAccess.GetVendorInfoAsync(User.Identity.GetUserId());
            var pos = await DataAccess.LoadPurchaseOrdersByVendorId(ui.No);
            var s = Export.ExportPurchaseOrdersToExcel(pos, "Purchase Orders");
            var cd = new System.Net.Mime.ContentDisposition() { FileName = string.Format("PurchaseOrders_{0}.xlsx", ui.Name) };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(s, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml");
        }
        [HttpPost]
        public async Task<ActionResult> MarkPoAsSent(string poNo)
        {
            if (!Request.IsAjaxRequest()) return new EmptyResult();
            if (poNo == null) return Content("Lỗi truyền dữ liệu: Không nhận được mã đơn hàng");
            try
            {
                var vendor = await DataAccess.GetVendorInfoAsync(User.Identity.GetUserId());
                //this usually happens because the cache for the current user is cleared, such as by the last update by MarkPoAsSent
                if(vendor.PurchaseOrders == null)
                {
                    vendor.PurchaseOrders = await DataAccess.LoadPurchaseOrdersByVendorId(vendor.No);
                }
                var cachedPo = vendor.PurchaseOrders.FirstOrDefault(e => e.PoNo == poNo);
                if(cachedPo == null)
                {
                    return Content("Không tìm thấy mã đơn hàng, xin thử tải lại trang.");
                }
                await DataAccess.MarkPoAsSent(poNo);
                //update info to the cache
                cachedPo.Sended = 1;
                cachedPo.SendDate = DateTime.Now;                
            } catch//(Exception ex)
            {
                //Logger.Write(string.Format("{0} - Could not mark PO as sent, Exception: {1}", DateTime.Now, ex));
                return Content("Lỗi cập nhật dữ liệu, xin hãy thử lại hoặc liên hệ với bộ phận IT");
            }
            return new EmptyResult();
        }
        public async Task<ActionResult> CheckAccessTokenValiditiy(string accessToken)
        {
            var ok = await DataAccess.IsValidManagerToken(accessToken);
            return Content(ok ? "ok" : "");
        }
        public async Task<ActionResult> GetAccessToken(string username, string password)
        {
            var managerInfo = await DataAccess.GetManagerInfo(username, password);
            if (managerInfo == null) return Content("");
            managerInfo.AccessToken = Guid.NewGuid().ToString("N");
            HttpContext.Cache[managerInfo.AccessToken] = managerInfo;
            return Content(managerInfo.AccessToken);
        }
        //confirm that the po has been fullfilled and the goods has been delivered to store
        public async Task<ActionResult> ApprovePo(string accessToken, string pono)
        {
            var ok = await DataAccess.IsValidManagerToken(accessToken);
            if (ok)
            {
                await DataAccess.ApprovePo(pono);
                return Content("ok");
            }
            Response.StatusCode = (int) System.Net.HttpStatusCode.Unauthorized;
            return null;
        }

        public async Task<ActionResult> ViewPosReport()
        {
            return View("PosReport");
        }
        public async Task<ActionResult> GetPoReportItems(string vendorNo = null, int confirmStatus = -1, int sentStatus = -1, int receivedStatus = -1, DateTime? fromOrderDate = null, DateTime? toOrderDate = null, DateTime? fromConfirmDate = null, DateTime? toConfirmDate = null, string locationCode = null)
        {
            var ps = await DataAccess.GetPosWithFilter(vendorNo, confirmStatus, sentStatus, receivedStatus, fromOrderDate, toOrderDate, fromConfirmDate, toConfirmDate, locationCode);
            var nextPage = ps.NextPage;
            var items = nextPage ?? Enumerable.Empty<PurchaseItem>();
            ViewBag.PosRequestId = ps.RequestId;
            ViewBag.TotalCount = ps.Pis.Count();
            HttpContext.Cache[ps.RequestId] = ps;
            return PartialView("_PurchaseOrdersReportPartialView", items);
        }
        
        public async Task<ActionResult> GetMorePos(string requestId)
        {            
            var items = await DataAccess.GetMorePos(requestId);
            if(items == null)//reached the end, now just remove the cache
            {
                HttpContext.Cache.Remove(requestId);
            }
            return PartialView("_PurchaseOrdersReportPartialView", items ?? Enumerable.Empty<PurchaseItem>());
        }

        public async Task<ActionResult> ExportPosStatus(string requestId)
        {
            var ui = await DataAccess.GetVendorInfoAsync(User.Identity.GetUserId());
            var items = await DataAccess.GetPosStatusReport(requestId);
            if (items == null) return new EmptyResult();
            var reportItems = items.Select(e => new PoStatusReportItem() {
                OrderDate = e.PO.OrderDate,
                PoNo = e.PoNo,
                VendorName = e.PO.VendorNameVn,
                VendorNo = e.PO.VendorNo,
                ItemNo = e.ItemNo,
                ItemName = e.Description,
                LocationCode = e.LocationCode,
                Quantity = e.Quantity,
                QuantityToReceive = e.QuantityToReceive,
                ConfirmedPoStatus = e.PO.Status != 0 ? "Đã xác nhận" : "Chưa xác nhận",
                ExpectedReceiptTime = e.ExpectedReceiptDate,
                POSentStatus = e.PO.Sended > 0 ? "Đã gửi hàng" : "Chưa gửi hàng",
                POSentDate = e.PO.SendDate,
                Status = e.PO.Approved > 0 ? "Đơn hàng hoàn thiện" : e.PO.Sended > 0 ? "Đã gửi hàng chưa post" : "Chưa gửi hàng đi"
            });
            var s = Export.ExportPosStatusReport(reportItems, "POs status");
            var cd = new System.Net.Mime.ContentDisposition() { FileName = string.Format("POs status_{0}.xlsx", ui.Name) };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(s, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml");
        }
    }
}