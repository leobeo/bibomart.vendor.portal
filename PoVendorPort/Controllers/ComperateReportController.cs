﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoVendorPort.Controllers
{
    using PoVendorPortCommon;
    using Models;
    using System.Threading.Tasks;
    using System.Web.Caching;
    using Microsoft.AspNet.Identity;
    using PoVendorPortCommon.Models;
    using DataAccess;
    using PoVendorPortCommon.Models.Picking;
    using PagedList;
    using Newtonsoft.Json;

    [Authorize]
    public class ComperateReportController : Controller
    {
        public ComperateReportController(IDataAccess da, ILogger logger)
        {
            DataAccess = da;
            Logger = logger;
        }

        public IDataAccess DataAccess { get; set; }
        public ILogger Logger { get; set; }

        // GET: ComperateReport
        public async Task<ActionResult> Index(string vendorNo)
        {
            if (Session["userRole"] == null)
            {
                List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
                Session["userRole"] = bn;
            }

            List<userRole_Item> userRole_s = (List<userRole_Item>)Session["userRole"];


            userRole_Item role_Item = userRole_s.FirstOrDefault(s => s.moduleController == "ComperateReport");

            Session["role_Item"] = role_Item;

            if (role_Item.typeGroup == 2)
            {
                var ls_report = await DataAccess.sp_Picking_get_ReportItem(User.Identity.GetUserId());
                ViewBag.lsReport = ls_report;

                Session["lsReport"] = ls_report;

                return View();
            }
            else
            {
                if(vendorNo !=null && vendorNo.Length > 0)
                {
                    try
                    {
                        var ls_report = await DataAccess.sp_Picking_get_ReportItem(vendorNo);
                        ViewBag.lsReport = ls_report;

                        Session["lsReport"] = ls_report;

                        return View();
                    }
                    catch (Exception ex)
                    {
                        return RedirectToAction("Vendors");
                    }
                    
                }
                else
                {
                    return RedirectToAction("Vendors");
                }
            }

            
        }

        public async Task<ActionResult> Detail(string orderNo)
        {
            try
            {
                if (orderNo == null || orderNo.Length == 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    List<report_Detail> its = new List<report_Detail>();


                    its = await DataAccess.sp_Picking_get_ReportDetail(User.Identity.GetUserId(), orderNo);
                    Session["orderNo"] = orderNo;
                    Session["onEdit"] = its;

                    //get report info

                    List<report_Item> lsReport = (List<report_Item>)Session["lsReport"];

                    var rp_info = lsReport.FirstOrDefault(s => s.orderNo == orderNo);

                    ViewBag.rp_info = rp_info;

                    //get report detail
                    ViewBag.rp_detail = its;

                    return View();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> Vendors()
        {
            var bn = await DataAccess.sp_Picking_get_ReportVendor(User.Identity.GetUserId());
            ViewBag.ls_vd = bn;
            return View();
        }


        [HttpPost]
        public ActionResult changedDiscount(string orderNo, string itemNo, string discountP)
        {
            try
            {
                List<report_Detail> its = (List<report_Detail>)Session["onEdit"];

                var found = its.FirstOrDefault(s => s.itemNo == itemNo && s.orderNo == orderNo);

                if(found != null)
                {
                    decimal disc = decimal.Parse(discountP);

                    if (found.discountPercent != disc)
                    {
                        found.discountPercent = disc;
                        found.isEdit = 1;

                        Session["onEdit"] = its;
                    }

                    return Json("Done");
                }else
                {
                    return Json("");
                }

                
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> rawSave(string orderNo)
        {
            try
            {
                List<report_Detail> its = (List<report_Detail>)Session["onEdit"];

                if (Session["orderNo"].ToString() == orderNo)
                {
                    var dis = await DataAccess.sp_Picking_update_Report(User.Identity.GetUserId(), its[0].orderNo, 1);
                    if (dis == true)
                    {
                        var bn = await DataAccess.sp_Picking_add_ReportLine(its, User.Identity.GetUserId(), 1);
                        return Json("Done");
                    }
                    else
                    {
                        return Json("");
                    }
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<ActionResult> AcceptReport(string orderNo)
        {
            try
            {
                List<report_Detail> its = (List<report_Detail>)Session["onEdit"];

                if (Session["orderNo"].ToString() == orderNo)
                {
                    var dis = await DataAccess.sp_Picking_update_Report(User.Identity.GetUserId(), its[0].orderNo,1);
                    if (dis == true)
                    {
                        var bn = await DataAccess.sp_Picking_add_ReportLine(its, User.Identity.GetUserId(), 2);
                        return Json("Done");
                    }
                    else
                    {
                        return Json("");
                    }
                }
                else
                {
                    return Json("");
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

    }
}