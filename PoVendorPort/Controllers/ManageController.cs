﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PoVendorPort.Models;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace PoVendorPort.Controllers
{
    using PoVendorPortCommon;
    using PoVendorPortCommon.Models;
    using Filters;
    using PoVendorPortCommon.Models.Picking;

    [Authorize]
    //[CustomActionFilter]
    [ManagerRightRequired]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ManageController(IDataAccess da)
        {
            DataAccess = da;
        }        
        public IDataAccess DataAccess { get; set; }
        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        public async Task<ActionResult> ControlPanel()
        {            
            var vm = new ControlPanelViewModel();
            vm.LoggedInUser = await DataAccess.GetUserInfo(User.Identity.GetUserId());
            if (vm.LoggedInUser.IsAdmin)
            {
                var users = await DataAccess.GetAllManageableUsers();
                vm.ManagedUsers = users.ToList();
            }
            var vendors = await DataAccess.SearchVendorsManagedBy(vm.LoggedInUser.IsAdmin ? null : vm.LoggedInUser.UserName, "");
            vm.ManagedVendors = vendors.ToList();

            List<userRole_Item> bn = await DataAccess.sp_Picking_get_UserRole(User.Identity.GetUserName());
            Session["userRole"] = bn;

            return View(vm);
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var userId = User.Identity.GetUserId();
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await UserManager.GetLoginsAsync(userId),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId)
            };
            return View(model);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        #region main management       
        [AdminRightRequired] 
        public async Task<ActionResult> EditUser(string username)
        {
            ViewBag.Caption = "Sửa thông tin người dùng";
            var vm = new UserEditViewModel();
            vm.UserInfo = await DataAccess.GetUserInfo(username);
            var vendors = await DataAccess.SearchVendorsManagedBy(vm.UserInfo.IsAdmin ? null : username, "");
            vm.UserInfo.ManagedVendors = vendors.ToList();
            var mg = await DataAccess.GetManageGroups();
            vm.ManageGroups = mg.ToList();
            return View(vm);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AdminRightRequired]
        public async Task<ActionResult> SaveUser(UserEditViewModel vm)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var b = await DataAccess.UpdateUserInfo(vm.UserInfo, vm.Password);
                    return RedirectToAction(nameof(ControlPanel));
                }
                catch
                {
                    ModelState.AddModelError("", "Có lỗi cập nhật dữ liệu không mong muốn, xin thử lại sau");
                }
            }
            if (vm.IsAdding)
            {
                ViewBag.Caption = "Thêm tài khoản người dùng";
            } else
            {
                ViewBag.Caption = "Sửa thông tin người dùng";
            }
            return View("EditUser", vm);
        }
        [HttpPost]
        public async Task<ActionResult> GetFormattedVendorsText(string vendors)
        {
            vendors = HttpUtility.HtmlDecode(vendors).Replace("&nbsp;", " ");
            var formattedText = Regex.Replace(vendors, @"(?<=([>]|^)[^><]*)\w+(?=[^<]*([<]|$))", m => {
                var vi = DataAccess.GetVendorInfo(m.Value);
                if (vi == null)
                {
                    var t = DataAccess.GetUserInfo(m.Value);
                    t.Wait();
                    if (t.Result == null)
                    {
                        vi = DataAccess.GetVendorInfoFromPoTable(m.Value);
                        if (vi == null)
                        {
                            vi = new Vendor
                            {
                                No = m.Value,
                                State = VendorState.NoPoNoUser
                            };
                        }
                        else
                        {
                            vi.State = VendorState.PoNoUser;
                        }
                    } else
                    {
                        vi = new Vendor() { No = t.Result.UserName };
                        vi.State = VendorState.NoPoUser;
                    }
                }
                return $"<span data-vendorInfo='{Newtonsoft.Json.JsonConvert.SerializeObject(vi)}' class='vendor-tag {((vi.State == VendorState.NoPoNoUser || vi.State == VendorState.PoNoUser ? "no-user" : vi.State == VendorState.NoPoUser ? "no-po" : "") + (vi.IsManaged ? " is-managed" : "")).Trim()}' style='display:inline-block;position:relative;'>{vi.No}</span>";
            });                
            return Content(formattedText);
        }
        [AdminRightRequired]
        public async Task<ActionResult> AddUser()
        {
            var vm = new UserEditViewModel();
            ViewBag.Caption = "Thêm tài khoản người dùng";
            vm.UserInfo = new PoVendorPortCommon.Models.UserInfo();
            vm.IsAdding = true;
            var mg = await DataAccess.GetManageGroups();
            vm.ManageGroups = mg.ToList();
            return View("EditUser",vm);
        }        
        public async Task<ActionResult> EditVendor(string vendorNo)
        {
            var vm = new VendorEditViewModel();
            vm.Vendor = await DataAccess.GetVendorInfoAsync(vendorNo);
            return View(vm);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SaveVendor(VendorEditViewModel vm)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    
                } catch
                {

                }
            }
            return View("EditVendor", vm);
        }
        public async Task<ActionResult> AddVendors()
        {
            //ActiveTab = 1 here means the active tab should be the second tab (which is Quản lý nhà cung cấp).
            ViewBag.BackUrl = Url.Action(nameof(ControlPanel));
            ViewBag.BackLinkText = "Quay lại trang quản lý NCC";
            var vm = new VendorsAddingViewModel();
            return View(vm);
        }
        [HttpPost]
        public async Task<ActionResult> ResetVendorsPass(string vendors)
        {
            var codes = Regex.Matches(vendors, @"\w+").Cast<Match>().Select(m => m.Value).ToList();
            if (!codes.Any()) return Content("Không nhận được mã NCC nào!");
            try
            {
                await DataAccess.ResetVendorsPass(codes);
                return new EmptyResult();
            } 
            catch
            {
                return Content("Có lỗi không mong muốn, xin thử lại sau!");
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SaveVendors(VendorsAddingViewModel vm)
        {
            if (ModelState.IsValid)
            {
                try
                {                    
                    var pattern = vm.ParseNumbersHavingAtLeast5DigitsAsVendorNo ? @"\d{5,}" : @"\w+";
                    var vendorNos = Regex.Matches(vm.Vendors, pattern)
                                         .Cast<Match>().Select(e => e.Value).ToList();                    
                    if (vendorNos.Any()) {
                        var vendorCredentials = vendorNos.Select(e => new Tuple<string, string>(e, vm.UsePasswordWithDefaultPattern ? $"{e}@123456" : vm.Password)).ToList();
                        var existingCodes = await DataAccess.AddVendors(vendorCredentials, vm.ResetPasswordForExistingVendors);
                        vm.ExistingVendorCodes = existingCodes.ToList();
                        ViewBag.Success = "Lưu thành công!";
                    } else
                    {
                        ModelState.AddModelError("", "Bạn cần nhập ít nhất 1 mã NCC!");
                    }
                }
                catch
                {
                    ModelState.AddModelError("", "Có lỗi cập nhật không mong muốn, xin thử lại sau!");
                }
            }
            //ActiveTab = 1 here means the active tab should be the second tab (which is Quản lý nhà cung cấp).
            ViewBag.BackUrl = Url.Action(nameof(ControlPanel));
            ViewBag.BackLinkText = "Quay lại trang quản lý NCC";
            return View("AddVendors", vm);
        }
        //[ValidateAntiForgeryToken]
        [HttpPost]
        [AdminRightRequired]
        public async Task<ActionResult> AddDefaultVendorAccount(string vendorNo)
        {
            try
            {
                await DataAccess.AddDefaultVendors(new List<string> { vendorNo });
                var vendor = DataAccess.GetVendorInfoFromPoTable(vendorNo);
                if(vendor == null)
                {
                    //this means the newly created vendor account does not have any PO, so that also means it won't have any friendly (fullname) which can 
                    //only be obtained from po table.
                    //We can then just create a new default vendor instance, later in future if it has some purchase order, its name may be obtained.
                    vendor = new Vendor() {
                         No = vendorNo,
                         VietnameseName = vendorNo
                    };
                }
                else vendor.PoCount = await DataAccess.GetVendorPoCount(vendorNo);
                return PartialView("_VendorItemPartial", vendor);
            } catch(Exception ex)
            {                
                return new EmptyResult();
            }
        }
        public async Task<ActionResult> LoadPurchaseOrders(string vendorNo)
        {
            if (!Request.IsAjaxRequest()) return new EmptyResult();
            try
            {
                //await Task.Delay(3000);
                var vi = await DataAccess.GetVendorInfoAsync(vendorNo);                
                if (vi == null)
                {
                    vi = new Vendor() { No = vendorNo };
                    vi.PurchaseOrders = new List<PurchaseOrder>();
                }
                else
                {
                    var pos = await DataAccess.LoadPurchaseOrdersByVendorId(vi.No);
                    vi.PurchaseOrders = pos.ToList();
                }
                return PartialView("_DetailPoListPartial", vi);
            }
            catch (Exception ex)
            {
                //Logger.Write(string.Format("{0} - AJAX LoadPurchaseOrders error: {1}, Authorized username: {2}", DateTime.Now, ex, User.Identity.Name));
                return null;
            }
        }
        #endregion

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

#endregion
    }
}